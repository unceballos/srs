CREATE DATABASE  IF NOT EXISTS `srs` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci */;
USE `srs`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: srs
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentarios` (
  `co_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_comentario` text,
  `co_fechaHora` datetime NOT NULL,
  `co_idReporte` int(10) unsigned NOT NULL,
  `co_idUsuario` int(10) unsigned NOT NULL,
  PRIMARY KEY (`co_id`),
  KEY `fk_usuarioRol_idx` (`co_idReporte`),
  KEY `fk_coIdUsuarioComentario_idx` (`co_idUsuario`),
  CONSTRAINT `fk_idReporteComentario` FOREIGN KEY (`co_idReporte`) REFERENCES `reportes` (`re_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_idUsuarioComentario` FOREIGN KEY (`co_idUsuario`) REFERENCES `usuarios` (`us_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentarios`
--

LOCK TABLES `comentarios` WRITE;
/*!40000 ALTER TABLE `comentarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delegaciones`
--

DROP TABLE IF EXISTS `delegaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delegaciones` (
  `dl_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dl_delegacion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`dl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delegaciones`
--

LOCK TABLES `delegaciones` WRITE;
/*!40000 ALTER TABLE `delegaciones` DISABLE KEYS */;
INSERT INTO `delegaciones` VALUES (1,'Manzanillo'),(2,'Tecomán'),(3,'Colima'),(4,'Coquimatlán'),(5,'Villa de Álvarez'),(6,'Comala'),(7,'Campus Norte'),(8,'Suchitlán'),(9,'Cuauhtemoc'),(10,'Armería'),(11,'El Colomo'),(12,'Santiago'),(13,'Queseria');
/*!40000 ALTER TABLE `delegaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dependencias`
--

DROP TABLE IF EXISTS `dependencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependencias` (
  `de_id` int(11) unsigned NOT NULL,
  `de_dependencia` varchar(65) NOT NULL,
  `de_delegacion` int(10) unsigned NOT NULL,
  PRIMARY KEY (`de_id`),
  KEY `fk_delegacion_dependencias_idx` (`de_delegacion`),
  CONSTRAINT `fk_delegacion_dependencia` FOREIGN KEY (`de_delegacion`) REFERENCES `delegaciones` (`dl_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependencias`
--

LOCK TABLES `dependencias` WRITE;
/*!40000 ALTER TABLE `dependencias` DISABLE KEYS */;
INSERT INTO `dependencias` VALUES (1,'Biblioteca de Ciencias del Mar',1),(2,'Biblioteca de Comercio',1),(3,'Biblioteca Lic. Ernesto Camacho Quiñones',1),(4,'Biblioteca Comercio y Nutrición',1),(5,'Biblioteca de Ciencias Agropecuarias',2),(6,'Biblioteca de los bachilleratos 5, 6 y 20',2),(7,'Biblioteca de Ciencias Miguel de la Madrid Hurtado',3),(8,'Biblioteca de Ciencias Sociales',3),(9,'Biblioteca del Área de la salud',3),(10,'Biblioteca IUBA',3),(11,'Biblioteca Pinacoteca',3),(12,'Biblioteca Albarrada Bachilleratos 15 y 30',3),(13,'Biblioteca Cóbano',3),(14,'Biblioteca de Ciencias Aplicadas',4),(15,'Biblioteca de Humanidades',5),(16,'Biblioteca de los bachilleratos 4 y 16',5),(17,'Biblioteca del bachillerato 17 y 25',6),(18,'Biblioteca Nogueras',6),(19,'Biblioteca de Ciencias Políticas y Jurídicas',7),(20,'Biblioteca Bachillerato 32',8),(21,'Biblioteca de los bachilleratos 12 y 13',9),(22,'Biblioteca de bachillerato 7 y 21',10),(23,'Biblioteca de Bachillerato 14',11),(24,'Biblioteca del Bachillerato Santiago',12),(28,'DGTI',3),(29,'Biblioteca del Bachillerato 22',13),(30,'Procesos técnicos',3),(31,'Desarrollo de Colecciones',3),(32,'Contabilidad',3),(33,'Soporte técnico',3);
/*!40000 ALTER TABLE `dependencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `encargados`
--

DROP TABLE IF EXISTS `encargados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `encargados` (
  `en_numTrabajador` smallint(6) unsigned NOT NULL,
  `en_nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`en_numTrabajador`),
  UNIQUE KEY `en_Nombre_UNIQUE` (`en_nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `encargados`
--

LOCK TABLES `encargados` WRITE;
/*!40000 ALTER TABLE `encargados` DISABLE KEYS */;
/*!40000 ALTER TABLE `encargados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipos`
--

DROP TABLE IF EXISTS `equipos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipos` (
  `eq_patrimonio` varchar(45) NOT NULL,
  `eq_marca` varchar(45) NOT NULL,
  `eq_modelo` varchar(45) NOT NULL,
  `eq_serie` varchar(60) NOT NULL,
  `eq_sisOper` varchar(45) NOT NULL,
  `eq_office` varchar(20) NOT NULL,
  `eq_tipoEquipo` int(10) unsigned NOT NULL,
  `eq_encargado` smallint(6) unsigned NOT NULL,
  `eq_status` int(10) unsigned NOT NULL,
  `eq_motivoBaja` varchar(100) DEFAULT NULL,
  `eq_descripcion` text,
  `eq_folio` tinyint(3) DEFAULT NULL,
  `eq_factura` varchar(10) DEFAULT NULL,
  `eq_programaFederal` varchar(15) DEFAULT NULL,
  `eq_proveedor` int(10) unsigned NOT NULL,
  `eq_dependencia` int(10) unsigned NOT NULL,
  PRIMARY KEY (`eq_patrimonio`),
  KEY `fk_tipoEquipo_Equipos_idx` (`eq_tipoEquipo`),
  KEY `fk_tipoStatus_Equipos_idx` (`eq_status`),
  KEY `fk_encargadoEquipo_Equipos_idx` (`eq_encargado`),
  KEY `fk_proveedor_Equipos_idx` (`eq_proveedor`),
  KEY `fk_dependencia_Equipos_idx` (`eq_dependencia`),
  CONSTRAINT `fk_dependencia_Equipos` FOREIGN KEY (`eq_dependencia`) REFERENCES `dependencias` (`de_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_encargadoEquipo_Equipos` FOREIGN KEY (`eq_encargado`) REFERENCES `encargados` (`en_numTrabajador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_proveedor_Equipos` FOREIGN KEY (`eq_proveedor`) REFERENCES `proveedores` (`pr_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_statusEquipo_Equipos` FOREIGN KEY (`eq_status`) REFERENCES `status` (`st_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tipoEquipo_Equipos` FOREIGN KEY (`eq_tipoEquipo`) REFERENCES `tipoequipo` (`te_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipos`
--

LOCK TABLES `equipos` WRITE;
/*!40000 ALTER TABLE `equipos` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedores`
--

DROP TABLE IF EXISTS `proveedores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedores` (
  `pr_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pr_proveedor` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`pr_id`),
  UNIQUE KEY `pr_proveedor_UNIQUE` (`pr_proveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedores`
--

LOCK TABLES `proveedores` WRITE;
/*!40000 ALTER TABLE `proveedores` DISABLE KEYS */;
INSERT INTO `proveedores` VALUES (1,'Hp de México S.A. de C.V.');
/*!40000 ALTER TABLE `proveedores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reportes`
--

DROP TABLE IF EXISTS `reportes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reportes` (
  `re_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `re_trabajoSolicitado` text NOT NULL,
  `re_trabajoRealizado` text,
  `re_asunto` varchar(45) NOT NULL,
  `re_status` int(10) unsigned NOT NULL,
  `re_idPatrimonio` varchar(45) NOT NULL,
  `re_tipoServicio` int(10) unsigned NOT NULL,
  `re_reparador` int(10) unsigned DEFAULT NULL,
  `re_tipoEquipo` int(10) unsigned NOT NULL,
  `re_fecha` datetime NOT NULL,
  PRIMARY KEY (`re_id`),
  KEY `fk_tipoServicio_Reportes_idx` (`re_tipoServicio`),
  KEY `fk_status_Reportes_idx` (`re_status`),
  KEY `fk_reparador_Reportes_idx` (`re_reparador`),
  KEY `fk_tipoEquipo_Reportes_idx` (`re_tipoEquipo`),
  KEY `fk_patrimonio_Equipos_idx` (`re_idPatrimonio`),
  CONSTRAINT `fk_patrimonio_Equipos` FOREIGN KEY (`re_idPatrimonio`) REFERENCES `equipos` (`eq_patrimonio`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_reparadaro_Reportes` FOREIGN KEY (`re_reparador`) REFERENCES `usuarios` (`us_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_Reportes` FOREIGN KEY (`re_status`) REFERENCES `status` (`st_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tipoEquipo_Reportes` FOREIGN KEY (`re_tipoEquipo`) REFERENCES `tipoequipo` (`te_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tipoServicio_Reportes` FOREIGN KEY (`re_tipoServicio`) REFERENCES `tiposervicio` (`ti_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reportes`
--

LOCK TABLES `reportes` WRITE;
/*!40000 ALTER TABLE `reportes` DISABLE KEYS */;
/*!40000 ALTER TABLE `reportes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `ro_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ro_rol` varchar(40) NOT NULL,
  PRIMARY KEY (`ro_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin'),(2,'Tecnico'),(3,'Blibliotecario'),(4,'Directivo'),(5,'Bibliotecario Informático');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `st_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `st_status` varchar(20) NOT NULL,
  PRIMARY KEY (`st_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'Activo'),(2,'Inactivo'),(3,'En proceso'),(4,'Terminado'),(5,'Entregado'),(6,'Eliminado');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoequipo`
--

DROP TABLE IF EXISTS `tipoequipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoequipo` (
  `te_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `te_tipo` varchar(20) NOT NULL,
  PRIMARY KEY (`te_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoequipo`
--

LOCK TABLES `tipoequipo` WRITE;
/*!40000 ALTER TABLE `tipoequipo` DISABLE KEYS */;
INSERT INTO `tipoequipo` VALUES (1,'Impresora'),(2,'Miniprinter'),(3,'Lector de CB'),(4,'Computadora'),(5,'Pantalla'),(6,'Escaner'),(7,'Proyector'),(8,'Videocasetera'),(9,'Otro'),(10,'Mobiliario'),(11,'Alumbrado');
/*!40000 ALTER TABLE `tipoequipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiposervicio`
--

DROP TABLE IF EXISTS `tiposervicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiposervicio` (
  `ti_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ti_tipoServicio` varchar(20) NOT NULL,
  PRIMARY KEY (`ti_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiposervicio`
--

LOCK TABLES `tiposervicio` WRITE;
/*!40000 ALTER TABLE `tiposervicio` DISABLE KEYS */;
INSERT INTO `tiposervicio` VALUES (1,'Instalación'),(2,'Mantenimiento'),(3,'Reparacón'),(4,'Garantía'),(5,'Asesoría');
/*!40000 ALTER TABLE `tiposervicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `us_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `us_email` varchar(45) NOT NULL,
  `us_nombre` varchar(45) NOT NULL,
  `us_apellido` varchar(45) NOT NULL,
  `us_dependencia` int(10) unsigned DEFAULT NULL,
  `us_telefono` varchar(15) NOT NULL,
  `us_pass` varchar(20) NOT NULL,
  `us_status` int(10) unsigned NOT NULL,
  `us_rol` int(10) unsigned DEFAULT NULL,
  `us_numTrabajador` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`us_id`),
  UNIQUE KEY `us_email_UNIQUE` (`us_email`),
  KEY `fk_rol_Usuarios_idx` (`us_rol`),
  KEY `fk_status_Usuarios_idx` (`us_status`),
  KEY `fk_idDependencia_idx` (`us_dependencia`),
  CONSTRAINT `fk_idDependencia` FOREIGN KEY (`us_dependencia`) REFERENCES `dependencias` (`de_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_rol_Usuarios` FOREIGN KEY (`us_rol`) REFERENCES `roles` (`ro_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_Usuarios` FOREIGN KEY (`us_status`) REFERENCES `status` (`st_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (15,'soportedgti@ucol.mx','Mike','Flores',NULL,'3123102307','S0p0rt3-',1,1,'1444');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_reportes`
--

DROP TABLE IF EXISTS `usuarios_reportes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios_reportes` (
  `ur_idUsuario` int(10) unsigned NOT NULL,
  `ur_idReporte` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ur_idUsuario`,`ur_idReporte`),
  KEY `fk_idReporte_usReportes_idx` (`ur_idReporte`),
  CONSTRAINT `fk_idReporte_usReportes` FOREIGN KEY (`ur_idReporte`) REFERENCES `reportes` (`re_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_idUsuario_usReportes` FOREIGN KEY (`ur_idUsuario`) REFERENCES `usuarios` (`us_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_reportes`
--

LOCK TABLES `usuarios_reportes` WRITE;
/*!40000 ALTER TABLE `usuarios_reportes` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_reportes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `ver_bibliotecarios`
--

DROP TABLE IF EXISTS `ver_bibliotecarios`;
/*!50001 DROP VIEW IF EXISTS `ver_bibliotecarios`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ver_bibliotecarios` AS SELECT 
 1 AS `id`,
 1 AS `email`,
 1 AS `nombre`,
 1 AS `apellido`,
 1 AS `telefono`,
 1 AS `Status`,
 1 AS `pass`,
 1 AS `numTrabajador`,
 1 AS `rol`,
 1 AS `idRol`,
 1 AS `dependencia`,
 1 AS `idDependencia`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ver_inventario`
--

DROP TABLE IF EXISTS `ver_inventario`;
/*!50001 DROP VIEW IF EXISTS `ver_inventario`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ver_inventario` AS SELECT 
 1 AS `nameTipoEquipo`,
 1 AS `tipoEquipo`,
 1 AS `marca`,
 1 AS `modelo`,
 1 AS `serie`,
 1 AS `patrimonio`,
 1 AS `so`,
 1 AS `office`,
 1 AS `description`,
 1 AS `folio`,
 1 AS `factura`,
 1 AS `programaFederal`,
 1 AS `numTrabajador`,
 1 AS `status`,
 1 AS `idDependencia`,
 1 AS `dependencia`,
 1 AS `encargado`,
 1 AS `idEncargado`,
 1 AS `idProveedor`,
 1 AS `proveedor`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ver_reportes_completos`
--

DROP TABLE IF EXISTS `ver_reportes_completos`;
/*!50001 DROP VIEW IF EXISTS `ver_reportes_completos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ver_reportes_completos` AS SELECT 
 1 AS `id_solicitante`,
 1 AS `nombre_solicitante`,
 1 AS `apellido_solicitante`,
 1 AS `fecha_solicitud`,
 1 AS `tipo_equipo`,
 1 AS `id_servicio`,
 1 AS `marca_equipo`,
 1 AS `modelo_equipo`,
 1 AS `serie_equipo`,
 1 AS `patrimonio_equipo`,
 1 AS `trabajos_solicitado`,
 1 AS `trabajo_realizado`,
 1 AS `id_status`,
 1 AS `id_tipo_equipo`,
 1 AS `id_reporte`,
 1 AS `status`,
 1 AS `asunto_reporte`,
 1 AS `dependencia_solicitante`,
 1 AS `id_reparador`,
 1 AS `idDependencia`,
 1 AS `nombre_reparador`,
 1 AS `apellido_reparador`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ver_reportes_sin_reparador`
--

DROP TABLE IF EXISTS `ver_reportes_sin_reparador`;
/*!50001 DROP VIEW IF EXISTS `ver_reportes_sin_reparador`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ver_reportes_sin_reparador` AS SELECT 
 1 AS `nombre_solicitante`,
 1 AS `apellido_solicitante`,
 1 AS `id_solicitante`,
 1 AS `idDependencia`,
 1 AS `fecha_solicitud`,
 1 AS `tipo_equipo`,
 1 AS `marca_equipo`,
 1 AS `modelo_equipo`,
 1 AS `serie_equipo`,
 1 AS `patrimonio_equipo`,
 1 AS `trabajos_solicitado`,
 1 AS `trabajo_realizado`,
 1 AS `status`,
 1 AS `id_status`,
 1 AS `id_tipo_equipo`,
 1 AS `asunto_reporte`,
 1 AS `id_reparador`,
 1 AS `id_reporte`,
 1 AS `dependencia_solicitante`,
 1 AS `id_servicio`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `ver_tecnicos`
--

DROP TABLE IF EXISTS `ver_tecnicos`;
/*!50001 DROP VIEW IF EXISTS `ver_tecnicos`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `ver_tecnicos` AS SELECT 
 1 AS `id`,
 1 AS `email`,
 1 AS `nombre`,
 1 AS `apellido`,
 1 AS `telefono`,
 1 AS `numTrabajador`,
 1 AS `Status`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_comentarios_reportes`
--

DROP TABLE IF EXISTS `vw_comentarios_reportes`;
/*!50001 DROP VIEW IF EXISTS `vw_comentarios_reportes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_comentarios_reportes` AS SELECT 
 1 AS `comentario`,
 1 AS `fecha`,
 1 AS `idReporte`,
 1 AS `nombre`,
 1 AS `apellido`,
 1 AS `rol`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_dependencias`
--

DROP TABLE IF EXISTS `vw_dependencias`;
/*!50001 DROP VIEW IF EXISTS `vw_dependencias`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_dependencias` AS SELECT 
 1 AS `dependencia`,
 1 AS `idDependencia`,
 1 AS `delegacion`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_equipos_reportados`
--

DROP TABLE IF EXISTS `vw_equipos_reportados`;
/*!50001 DROP VIEW IF EXISTS `vw_equipos_reportados`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_equipos_reportados` AS SELECT 
 1 AS `equipo`,
 1 AS `fecha`,
 1 AS `totales_reportes_equipos`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_meses`
--

DROP TABLE IF EXISTS `vw_meses`;
/*!50001 DROP VIEW IF EXISTS `vw_meses`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_meses` AS SELECT 
 1 AS `MONTH`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_reportes_dependencias`
--

DROP TABLE IF EXISTS `vw_reportes_dependencias`;
/*!50001 DROP VIEW IF EXISTS `vw_reportes_dependencias`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_reportes_dependencias` AS SELECT 
 1 AS `nombre_dependencia`,
 1 AS `fecha`,
 1 AS `totales_reportes_dependencias`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_reportes_totales_mensuales`
--

DROP TABLE IF EXISTS `vw_reportes_totales_mensuales`;
/*!50001 DROP VIEW IF EXISTS `vw_reportes_totales_mensuales`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_reportes_totales_mensuales` AS SELECT 
 1 AS `num_mes`,
 1 AS `total_reportes_mes`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `vw_status_reportes`
--

DROP TABLE IF EXISTS `vw_status_reportes`;
/*!50001 DROP VIEW IF EXISTS `vw_status_reportes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `vw_status_reportes` AS SELECT 
 1 AS `status`,
 1 AS `fecha_status`,
 1 AS `totales_reportes_status`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'srs'
--

--
-- Dumping routines for database 'srs'
--

--
-- Final view structure for view `ver_bibliotecarios`
--

/*!50001 DROP VIEW IF EXISTS `ver_bibliotecarios`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ver_bibliotecarios` AS select `usuarios`.`us_id` AS `id`,`usuarios`.`us_email` AS `email`,`usuarios`.`us_nombre` AS `nombre`,`usuarios`.`us_apellido` AS `apellido`,`usuarios`.`us_telefono` AS `telefono`,`usuarios`.`us_status` AS `Status`,`usuarios`.`us_pass` AS `pass`,`usuarios`.`us_numTrabajador` AS `numTrabajador`,`roles`.`ro_rol` AS `rol`,`roles`.`ro_id` AS `idRol`,`dependencias`.`de_dependencia` AS `dependencia`,`dependencias`.`de_id` AS `idDependencia` from ((`usuarios` join `roles` on((`roles`.`ro_id` = `usuarios`.`us_rol`))) join `dependencias` on((`dependencias`.`de_id` = `usuarios`.`us_dependencia`))) where (((`usuarios`.`us_rol` = 3) or (`usuarios`.`us_rol` = 4) or (`usuarios`.`us_rol` = 5)) and (`usuarios`.`us_status` <> 6)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ver_inventario`
--

/*!50001 DROP VIEW IF EXISTS `ver_inventario`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ver_inventario` AS select `tipoequipo`.`te_tipo` AS `nameTipoEquipo`,`equipos`.`eq_tipoEquipo` AS `tipoEquipo`,`equipos`.`eq_marca` AS `marca`,`equipos`.`eq_modelo` AS `modelo`,`equipos`.`eq_serie` AS `serie`,`equipos`.`eq_patrimonio` AS `patrimonio`,`equipos`.`eq_sisOper` AS `so`,`equipos`.`eq_office` AS `office`,`equipos`.`eq_descripcion` AS `description`,`equipos`.`eq_folio` AS `folio`,`equipos`.`eq_factura` AS `factura`,`equipos`.`eq_programaFederal` AS `programaFederal`,`equipos`.`eq_encargado` AS `numTrabajador`,`equipos`.`eq_status` AS `status`,`equipos`.`eq_dependencia` AS `idDependencia`,`dependencias`.`de_dependencia` AS `dependencia`,`encargados`.`en_nombre` AS `encargado`,`encargados`.`en_numTrabajador` AS `idEncargado`,`proveedores`.`pr_id` AS `idProveedor`,`proveedores`.`pr_proveedor` AS `proveedor` from ((((`equipos` join `tipoequipo` on((`tipoequipo`.`te_id` = `equipos`.`eq_tipoEquipo`))) join `encargados` on((`encargados`.`en_numTrabajador` = `equipos`.`eq_encargado`))) join `proveedores` on((`proveedores`.`pr_id` = `equipos`.`eq_proveedor`))) join `dependencias` on((`dependencias`.`de_id` = `equipos`.`eq_dependencia`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ver_reportes_completos`
--

/*!50001 DROP VIEW IF EXISTS `ver_reportes_completos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ver_reportes_completos` AS select `ver_reportes_sin_reparador`.`id_solicitante` AS `id_solicitante`,`ver_reportes_sin_reparador`.`nombre_solicitante` AS `nombre_solicitante`,`ver_reportes_sin_reparador`.`apellido_solicitante` AS `apellido_solicitante`,`ver_reportes_sin_reparador`.`fecha_solicitud` AS `fecha_solicitud`,`ver_reportes_sin_reparador`.`tipo_equipo` AS `tipo_equipo`,`ver_reportes_sin_reparador`.`id_servicio` AS `id_servicio`,`ver_reportes_sin_reparador`.`marca_equipo` AS `marca_equipo`,`ver_reportes_sin_reparador`.`modelo_equipo` AS `modelo_equipo`,`ver_reportes_sin_reparador`.`serie_equipo` AS `serie_equipo`,`ver_reportes_sin_reparador`.`patrimonio_equipo` AS `patrimonio_equipo`,`ver_reportes_sin_reparador`.`trabajos_solicitado` AS `trabajos_solicitado`,`ver_reportes_sin_reparador`.`trabajo_realizado` AS `trabajo_realizado`,`ver_reportes_sin_reparador`.`id_status` AS `id_status`,`ver_reportes_sin_reparador`.`id_tipo_equipo` AS `id_tipo_equipo`,`ver_reportes_sin_reparador`.`id_reporte` AS `id_reporte`,`ver_reportes_sin_reparador`.`status` AS `status`,`ver_reportes_sin_reparador`.`asunto_reporte` AS `asunto_reporte`,`ver_reportes_sin_reparador`.`dependencia_solicitante` AS `dependencia_solicitante`,`ver_reportes_sin_reparador`.`id_reparador` AS `id_reparador`,`ver_reportes_sin_reparador`.`idDependencia` AS `idDependencia`,`reparador`.`us_nombre` AS `nombre_reparador`,`reparador`.`us_apellido` AS `apellido_reparador` from (`ver_reportes_sin_reparador` left join `usuarios` `reparador` on((`reparador`.`us_id` = `ver_reportes_sin_reparador`.`id_reparador`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ver_reportes_sin_reparador`
--

/*!50001 DROP VIEW IF EXISTS `ver_reportes_sin_reparador`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ver_reportes_sin_reparador` AS select `solicitante`.`us_nombre` AS `nombre_solicitante`,`solicitante`.`us_apellido` AS `apellido_solicitante`,`solicitante`.`us_id` AS `id_solicitante`,`solicitante`.`us_dependencia` AS `idDependencia`,`reportes`.`re_fecha` AS `fecha_solicitud`,`tipoequipo`.`te_tipo` AS `tipo_equipo`,`equipos`.`eq_marca` AS `marca_equipo`,`equipos`.`eq_modelo` AS `modelo_equipo`,`equipos`.`eq_serie` AS `serie_equipo`,`equipos`.`eq_patrimonio` AS `patrimonio_equipo`,`reportes`.`re_trabajoSolicitado` AS `trabajos_solicitado`,`reportes`.`re_trabajoRealizado` AS `trabajo_realizado`,`status`.`st_status` AS `status`,`status`.`st_id` AS `id_status`,`tipoequipo`.`te_id` AS `id_tipo_equipo`,`reportes`.`re_asunto` AS `asunto_reporte`,`reportes`.`re_reparador` AS `id_reparador`,`reportes`.`re_id` AS `id_reporte`,`dependencias`.`de_dependencia` AS `dependencia_solicitante`,`tiposervicio`.`ti_id` AS `id_servicio` from (((((((`reportes` join `equipos` on((`reportes`.`re_idPatrimonio` = `equipos`.`eq_patrimonio`))) join `tipoequipo` on((`tipoequipo`.`te_id` = `reportes`.`re_tipoEquipo`))) join `tiposervicio` on((`tiposervicio`.`ti_id` = `reportes`.`re_tipoServicio`))) join `status` on((`status`.`st_id` = `reportes`.`re_status`))) join `usuarios_reportes` on((`usuarios_reportes`.`ur_idReporte` = `reportes`.`re_id`))) join `usuarios` `solicitante` on((`solicitante`.`us_id` = `usuarios_reportes`.`ur_idUsuario`))) left join `dependencias` on(((`dependencias`.`de_id` = `solicitante`.`us_dependencia`) or (`solicitante`.`us_dependencia` = NULL)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `ver_tecnicos`
--

/*!50001 DROP VIEW IF EXISTS `ver_tecnicos`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `ver_tecnicos` AS select `usuarios`.`us_id` AS `id`,`usuarios`.`us_email` AS `email`,`usuarios`.`us_nombre` AS `nombre`,`usuarios`.`us_apellido` AS `apellido`,`usuarios`.`us_telefono` AS `telefono`,`usuarios`.`us_numTrabajador` AS `numTrabajador`,`usuarios`.`us_status` AS `Status` from `usuarios` where ((`usuarios`.`us_rol` = 2) and (`usuarios`.`us_status` <> 6)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_comentarios_reportes`
--

/*!50001 DROP VIEW IF EXISTS `vw_comentarios_reportes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_comentarios_reportes` AS select `comentarios`.`co_comentario` AS `comentario`,`comentarios`.`co_fechaHora` AS `fecha`,`comentarios`.`co_idReporte` AS `idReporte`,`usuarios`.`us_nombre` AS `nombre`,`usuarios`.`us_apellido` AS `apellido`,`usuarios`.`us_rol` AS `rol` from ((`comentarios` join `reportes` on((`comentarios`.`co_idReporte` = `reportes`.`re_id`))) join `usuarios` on((`comentarios`.`co_idUsuario` = `usuarios`.`us_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_dependencias`
--

/*!50001 DROP VIEW IF EXISTS `vw_dependencias`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_dependencias` AS select `dependencias`.`de_dependencia` AS `dependencia`,`dependencias`.`de_id` AS `idDependencia`,`delegaciones`.`dl_delegacion` AS `delegacion` from (`dependencias` join `delegaciones` on((`dependencias`.`de_delegacion` = `delegaciones`.`dl_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_equipos_reportados`
--

/*!50001 DROP VIEW IF EXISTS `vw_equipos_reportados`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_equipos_reportados` AS select `tipoequipo`.`te_tipo` AS `equipo`,`reportes`.`re_fecha` AS `fecha`,count(0) AS `totales_reportes_equipos` from ((`reportes` join `equipos` on((`reportes`.`re_idPatrimonio` = `equipos`.`eq_patrimonio`))) join `tipoequipo` on((`tipoequipo`.`te_id` = `equipos`.`eq_tipoEquipo`))) group by `reportes`.`re_tipoEquipo` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_meses`
--

/*!50001 DROP VIEW IF EXISTS `vw_meses`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_meses` AS select 'enero' AS `MONTH` union select 'febrero' AS `MONTH` union select 'marzo' AS `MONTH` union select 'abril' AS `MONTH` union select 'mayo' AS `MONTH` union select 'junio' AS `MONTH` union select 'julio' AS `MONTH` union select 'agosto' AS `MONTH` union select 'septiembre' AS `MONTH` union select 'octubre' AS `MONTH` union select 'noviembre' AS `MONTH` union select 'diciembre' AS `MONTH` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_reportes_dependencias`
--

/*!50001 DROP VIEW IF EXISTS `vw_reportes_dependencias`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_reportes_dependencias` AS select `dependencias`.`de_dependencia` AS `nombre_dependencia`,`reportes`.`re_fecha` AS `fecha`,count(`reportes`.`re_id`) AS `totales_reportes_dependencias` from (((`dependencias` join `usuarios` on((`usuarios`.`us_dependencia` = `dependencias`.`de_id`))) join `usuarios_reportes` on((`usuarios_reportes`.`ur_idUsuario` = `usuarios`.`us_id`))) join `reportes` on((`reportes`.`re_id` = `usuarios_reportes`.`ur_idReporte`))) group by `dependencias`.`de_dependencia` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_reportes_totales_mensuales`
--

/*!50001 DROP VIEW IF EXISTS `vw_reportes_totales_mensuales`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_reportes_totales_mensuales` AS select `meses`.`MONTH` AS `num_mes`,count(`reportes`.`re_fecha`) AS `total_reportes_mes` from (`vw_meses` `meses` left join `reportes` on((monthname(`reportes`.`re_fecha`) = `meses`.`MONTH`))) group by `meses`.`MONTH`,month(`reportes`.`re_fecha`) order by `meses`.`MONTH` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `vw_status_reportes`
--

/*!50001 DROP VIEW IF EXISTS `vw_status_reportes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`srs`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `vw_status_reportes` AS select `status`.`st_status` AS `status`,`reportes`.`re_fecha` AS `fecha_status`,count(0) AS `totales_reportes_status` from (`reportes` join `status` on((`status`.`st_id` = `reportes`.`re_status`))) group by `reportes`.`re_status` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-17 13:24:08
