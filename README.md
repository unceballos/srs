# Sistema de reparación de SIABUC

Una plataforma para administrar y manejar reportes de reparación de manera eficiente.

## Getting Started

Para montar este proyecto y verlo en acción, generalmente seguimos las siguientes instrucciones:

* Hacer un 'git clone' al proyecto.
* Importar la base de datos (srs/BD.sql) al MySQL que estén usando.
* Tener un servidor de PHP y la base de datos operando.
* Crear un usuario que tenga todos los privilegios para manejar la base de datos, para crear el usuario  se pueden usar las siguientes líneas de código, sólo hace falta insertarlas en el shell de mysql:

```mysql
create user 'srs'@'localhost' identified by '152421';
grant all privileges on srs.* to 'srs'@'localhost';
flush privileges;
```

(En caso de que se desee cambiar esta configuración, el archivo que genera la conexión a la base de datos puede ser encontrado en ** srs/proyecto/controllers/controladorConexionMySQL.php **). 

* Eso es todo lo que hay que hacer. De aquí en delante sólo queda acceder al sistema y probar que todo funcione correctamente.


Como nota: Si quieren probar el sistema, deberán tener acceso a la cuenta de administrador, en estos momentos la cuenta por defecto es "maycolima@gmail.com" como correo y "admin" como contraseña. Ya, lo sabemos. No es lo más seguro del universo; pero es sólo un placeholder.


## Prerequisites

Antes de correr el proyecto, es necesario tener instalado una instancia de MySQL y un servidor para ejecutar código PHP.


## Built With

* [PHP](http://php.net/docs.php) - La mayoría del back
* [Materialize](http://materializecss.com/) - El framework de diseño utilizado
* [Ajax](http://api.jquery.com/jquery.ajax/) - Usado para relizar llamadas del lado del cliente
* [MySQL](https://www.mysql.com/) - Usado como base de datos para guardar los registros consultados


## Authors

* **Fernando Ceballos** - *Front-end / UI Design / PM* - [LinkedIn](https://www.linkedin.com/in/fernanceballos/)
* **Gustavo Urbinas** - *Most of the data base stuff / Some of the back-end* - [LinkedIn](https://www.linkedin.com/in/gustavo-urbina/)
* **Ubaldo Torres** - *Some of the data base stuff / Most of the back-end (JS, JQuery, PHP)* - [LinkedIn](https://www.linkedin.com/in/ubaldo-torres-ju%C3%A1rez-792827149/)

Siéntanse libres de ver [commits](https://bitbucket.org/unceballos/srs/commits/all) para ver las contribuciones que fueron hechas al proyecto.

## License

This project was meant to be licensed under the MIT License - see the [LICENSE.md](LICENSE.md) - but it doesn't really matter since we're pretty sure the UdeC is going to take over anyways. I mean, that's what we signed up for, right?

## Acknowledgments

* Hat tip to anyone who's code was used
* Thanks to Google for letting me know how design works
* And thanks to SIABUC for letting us work in such freedom
