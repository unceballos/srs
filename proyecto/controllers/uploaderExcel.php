<?php
require 'Classes/PHPExcel/IOFactory.php';
include 'controladorConexionMySQL.php';

$salida = "";
$listaErrores = "";
$insertados = 0;
$duplicados = 0;
$erroresExtras = 0;
$target_path = "../uploadsExcel/";

date_default_timezone_set('America/Mexico_City');
$timedate = date('Y-m-d H:i:s');
$timedate = str_replace(':', '', $timedate);
$timedate = str_replace('-', '', $timedate);
$target_path = $target_path . $timedate . ".csv";



if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {

    $objSQL = new conectionSQL();
    $conn = $objSQL->startConection();

    if ($conn->connect_error) {
        die("Hubo un error al conectarse a la base de datos, Error=" . $conn->getError());
    }

    $nombreArchivo = $target_path;
    $archivo = PHPExcel_IOFactory::load($nombreArchivo);
    $archivo->setActiveSheetIndex(0);
    $numRows = $archivo->setActiveSheetIndex(0)->getHighestDataRow();
    $time = (($numRows-1)/1000)*60; //Numero de registros-1 entre mil para sacar los minutos y por 60 para sacar los segundos 
    ini_set('max_execution_time', $time);

    for ($i = 2; $i <= $numRows; $i++) {
        $equipo = $archivo->getActiveSheet()->getCell('A' . $i)->getCalculatedValue();
        $equipo = mb_strtolower($equipo, 'UTF-8');
        //$equipo = strtoupper($equipo)
        //$equipo = trim($equipo);
        switch (strtoupper($equipo)) {
            case "IMPRESORA":
                $equipo = 1;
                break;
            case "MINIPRINTER":
                $equipo = 2;
                break;
            case "LECTOR DE CB":
                $equipo = 3;
                break;
            case "COMPUTADORA":
                $equipo = 4;
                break;
            case "PANTALLA":
                $equipo = 5;
                break;
            case "ESCANER":
                $equipo = 6;
                break;
            case "PROYECTOS":
                $equipo = 7;
                break;
            case "VIDEOCASETERA":
                $equipo = 8;
                break;
            case "OTRO":
                $equipo = 9;
                break;
            case "MOBILIARIO":
                $equipo = 10;
                break;
            case "ALUMBRADO":
                $equipo = 11;
                break;
        }

        $marca = $archivo->getActiveSheet()->getCell('B' . $i)->getCalculatedValue();
        $modelo = $archivo->getActiveSheet()->getCell('C' . $i)->getCalculatedValue();
        $serie = $archivo->getActiveSheet()->getCell('D' . $i)->getCalculatedValue();
        $patrimonio = $archivo->getActiveSheet()->getCell('E' . $i)->getCalculatedValue();
        $so = $archivo->getActiveSheet()->getCell('F' . $i)->getCalculatedValue();
        if (empty($so) || $so == "0") {
            $so = "NA";
        }
        $office = $archivo->getActiveSheet()->getCell('G' . $i)->getCalculatedValue();
        if (empty($office) || $office == "0") {
            $office = "NA";
        }
        $nombreEncargado = $archivo->getActiveSheet()->getCell('H' . $i)->getCalculatedValue();
        $numeroTrabajador = $archivo->getActiveSheet()->getCell('I' . $i)->getCalculatedValue();
        $descripcion = $archivo->getActiveSheet()->getCell('J' . $i)->getCalculatedValue();
        $folio = $archivo->getActiveSheet()->getCell('K' . $i)->getCalculatedValue();
        if (empty($folio) || $folio == "") {
            $folio = 0;
        }
        $factura = $archivo->getActiveSheet()->getCell('L' . $i)->getCalculatedValue();
        $programa = $archivo->getActiveSheet()->getCell('M' . $i)->getCalculatedValue();
        $proveedor = $archivo->getActiveSheet()->getCell('N' . $i)->getCalculatedValue();

        // DEPENDENCIA
        $dependecia = $archivo->getActiveSheet()->getCell('O' . $i)->getCalculatedValue();   
        
        // Agregar dependencia si existe el nombre

        if ($dependecia != "") {
            $select = "SELECT de_id FROM dependencias WHERE de_dependencia ='".$dependecia."'";
            $result = $objSQL->select($select);

            if (mysqli_num_rows($result) >= 1) {
                $row = $result->fetch_assoc();
                $dependecia = $row['de_id'];
            } else {
                // $insert = "INSERT INTO proveedores (pr_proveedor) VALUES ('" . $dependecia . "')";
                // $objSQL->insert($insert);
                // $proveedor = $objSQL->obtenerUltimoId();
            }
        }


        
        

        if ($proveedor != "" || $nombreEncargado != "" || $numeroTrabajador != "") {
            $select = "SELECT pr_id FROM proveedores WHERE pr_proveedor = '" . $proveedor . "'";
            $result = $objSQL->select($select);

            if (mysqli_num_rows($result) >= 1) {
                $row = $result->fetch_assoc();
                $proveedor = $row['pr_id'];
            } else {
                $insert = "INSERT INTO proveedores (pr_proveedor) VALUES ('" . $proveedor . "')";
                $objSQL->insert($insert);
                $proveedor = $objSQL->obtenerUltimoId();
            }

            $select = "SELECT en_numTrabajador FROM encargados WHERE en_nombre = '" . $nombreEncargado . "'";
            $result = $objSQL->select($select);
            if (mysqli_num_rows($result) >= 1) {
                $row = $result->fetch_assoc();
                $numeroTrabajador = $row['en_numTrabajador'];
            } else {
                $insert = "INSERT INTO encargados (en_numTrabajador,en_nombre) VALUES (" . $numeroTrabajador . ",'" . $nombreEncargado . "')";
                $objSQL->insert($insert);
            }

            $insertNew = "INSERT INTO equipos (eq_patrimonio, eq_marca, eq_modelo, eq_serie, eq_sisOper, eq_office, eq_tipoEquipo, eq_encargado, eq_status, eq_descripcion, eq_folio, eq_factura, eq_programaFederal,eq_proveedor, eq_dependencia) VALUES ('$patrimonio','$marca','$modelo','$serie','$so','$office',$equipo,$numeroTrabajador,1,'$descripcion',$folio,'$factura','$programa',$proveedor, $dependecia)";
            if ($conn->query($insertNew)) {
                $insertados++;
            } else {
                if ($conn->errno == 1062) {
                    $duplicados++;
                } else {
                    $listaErrores .= "<p class=\"blue-grey-text text-darken-3\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;En la línea " . $i . ": Un dato presenta un formato incorrecto.</p>";
                    $erroresExtras++;
                }
            }
        } else {
            $listaErrores .= "<p class=\"blue-grey-text text-darken-3\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;En la línea " . $i . ": Un dato presenta un formato incorrecto.</p>";
            $erroresExtras++;
        }
    } //END FOR

    if ($insertados == 0) {

        $salida .= "<p class=\"red-text text-darken-4\"><b>No se encontró ningún equipo a agregar<br>";
    } else {
        $salida = "<p class=\"teal-text text-darken-3\"><b>¡Se agregaron " . $insertados . " equipos exitosamente!</b></p>";
    }

    if ($duplicados != 0) {
        $salida .= "<p class=\"cyan-text text-darken-3\"><b>Se omitieron " . $duplicados . " equipos porque ya estaban registrados en la base de datos.</b></p>";
    }

    if ($erroresExtras != 0) {
        $salida .= "<p class=\"red-text text-darken-4\"><b>Hubo algún problema al tratar de registrar " . $erroresExtras . " equipos, los errores se muestran a continuación:<br>";
        $salida .= $listaErrores;

    }
    unlink($target_path);
} else {
    $salida = "Error al subir el archivo.";
}
echo $salida;
