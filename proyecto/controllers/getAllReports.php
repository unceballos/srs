<?php
	session_start();
	include("controladorConexionMySQL.php");
	$conn=new conectionSQL();
	$conn->startConection();

	$itemsPorPag = 5;	
	$totalPaginas = 1;	
	
	$pagina=0;
	$cad="";

	if(isset($_POST['page'])){
		$pagina=$_POST['page'];
	}else{
		$pagina=1;
	}

	$inicioResultado=($pagina-1)*$itemsPorPag;

 	if( isset($_POST['fechaInicio']) && isset($_POST['fechaFinal']) && isset($_POST['status']) && isset($_POST['idDependencia']) ){  //Filtro de fechas y estatus en los roles de Directivos y bibliotecarios.
		$sql="SELECT * FROM ver_reportes_completos WHERE (fecha_solicitud BETWEEN '".$_POST['fechaInicio']." 00:00:00' AND '".$_POST['fechaFinal']." 23:59:59') AND (id_status=".$_POST['status'].") AND idDependencia = ".$_POST['idDependencia']." ORDER BY fecha_solicitud ASC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM ver_reportes_completos WHERE (fecha_solicitud BETWEEN '".$_POST['fechaInicio']." 00:00:00' AND '".$_POST['fechaFinal']." 23:59:59') AND (id_status=".$_POST['status'].") AND idDependencia = ".$_POST['idDependencia']." ORDER BY fecha_solicitud ASC";
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

	}else if(isset($_POST['fechaInicio']) && isset($_POST['fechaFinal']) && isset($_POST['idDependencia']) ){ //Filtro de fechas en los roles de Directivos y bibliotecarios.
		$sql="SELECT * FROM ver_reportes_completos WHERE (fecha_solicitud BETWEEN '".$_POST['fechaInicio']." 00:00:00' AND '".$_POST['fechaFinal']." 23:59:59') AND idDependencia = ".$_POST['idDependencia']." ORDER BY fecha_solicitud ASC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM ver_reportes_completos WHERE (fecha_solicitud BETWEEN '".$_POST['fechaInicio']." 00:00:00' AND '".$_POST['fechaFinal']." 23:59:59') AND idDependencia = ".$_POST['idDependencia']." ORDER BY fecha_solicitud ASC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

	}else if(isset($_POST['fechaInicio']) && isset($_POST['fechaFinal']) && isset($_POST['status'])){ //Filtro de fechas y estatus en el rol de técnico
		$sql="SELECT * FROM ver_reportes_completos WHERE fecha_solicitud BETWEEN '".$_POST['fechaInicio']." 00:00:00' AND '".$_POST['fechaFinal']." 23:59:59' AND id_status=".$_POST['status']." ORDER BY fecha_solicitud ASC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM ver_reportes_completos WHERE fecha_solicitud BETWEEN '".$_POST['fechaInicio']." 00:00:00' AND '".$_POST['fechaFinal']." 23:59:59' AND id_status=".$_POST['status'];
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

	}else if(isset($_POST['status']) && isset($_POST['idDependencia'])){ //Filtro de status en los roles de Directivos y bibliotecarios.
		$sql="SELECT * FROM ver_reportes_completos WHERE idDependencia=".$_POST['idDependencia']." AND id_status=".$_POST['status']." ORDER BY fecha_solicitud DESC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM ver_reportes_completos WHERE idDependencia=".$_POST['idDependencia']." AND id_status=".$_POST['status']." ORDER BY fecha_solicitud DESC";
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

	}else if(isset($_POST['idDependencia']) && isset($_POST['asunto'])){ //Filtro por asunto en los roles de Directivos y bibliotecarios.
		$sql="SELECT * FROM ver_reportes_completos WHERE idDependencia=".$_POST['idDependencia']." AND asunto_reporte LIKE '%".$_POST['asunto']."%' ORDER BY fecha_solicitud DESC LIMIT ".$inicioResultado.",".$itemsPorPag;

	}else if(isset($_POST['patrimonio'])){ //Filtro por id de equipo en los detalles de un equipo por el rol de técnico
		$sql="SELECT * FROM ver_reportes_completos WHERE patrimonio_equipo='".$_POST['patrimonio']."' ORDER BY fecha_solicitud DESC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM ver_reportes_completos WHERE patrimonio_equipo='".$_POST['patrimonio']."'";
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

	}else if(isset($_POST['asunto'])){ //Filtro por asunto en el rol de técnico.
		$sql="SELECT * FROM ver_reportes_completos WHERE asunto_reporte LIKE '%".$_POST['asunto']."%' ORDER BY fecha_solicitud DESC LIMIT ".$inicioResultado.",".$itemsPorPag;

	}else if(isset($_POST['status'])){ //Filtro de estatus en el rol de técnico.
		$sql="SELECT * FROM ver_reportes_completos WHERE id_status=".$_POST['status']." ORDER BY fecha_solicitud DESC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM ver_reportes_completos WHERE id_status=".$_POST['status'];
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

	}else if(isset($_POST['fechaInicio']) && isset($_POST['fechaFinal'])) {//Filtro de fechas de todos los reportes en el rol de técnico

		$sql="SELECT * FROM ver_reportes_completos WHERE fecha_solicitud BETWEEN '".$_POST['fechaInicio']." 00:00:00' AND '".$_POST['fechaFinal']." 23:59:59' ORDER BY fecha_solicitud ASC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM ver_reportes_completos WHERE fecha_solicitud BETWEEN '".$_POST['fechaInicio']." 00:00:00' AND '".$_POST['fechaFinal']." 23:59:59'";
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

	}else if(isset($_POST['idDependencia'])){ //Obtiene todos los reportes por dependencia en los roles de Directivos y bibliotecarios.

		$sql="SELECT * FROM ver_reportes_completos WHERE idDependencia=".$_POST['idDependencia']." ORDER BY fecha_solicitud DESC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM ver_reportes_completos WHERE idDependencia=".$_POST['idDependencia'];
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

	}else{ //Obtiene todos los reportes por dependencia en el rol de técnico.
		$sql="SELECT * FROM ver_reportes_completos ORDER BY fecha_solicitud DESC LIMIT ".$inicioResultado.",".$itemsPorPag; 
		$consultaTotal="SELECT count(*) AS total FROM ver_reportes_completos";
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);
	}

	$result=$conn->select($sql);
	$outp = array();
	$outp = $result->fetch_all(MYSQLI_ASSOC);

	if(!isset($_POST['asunto'])){
		$arrayName = array(
			'totalPaginas' => $totalPaginas,
			'paginaAcual' => $pagina,
		);
		array_push($outp,$arrayName);
	}
	$conn->closeConection();
  	echo json_encode($outp);
  	
?>