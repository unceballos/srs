<?php
	include("controladorConexionMySQL.php");
	$connect = new conectionSQL();
	$connect -> startConection();

	date_default_timezone_set("America/Mexico_City");
	setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');

	$formato="Y-m-d H:i:s";

	if($_POST['fechaInicio']=="" || $_POST['fechaFin']==""){
		echo "dateNull";
	}else{
		$fechaInicio = $_POST['fechaInicio']." 00:00:00";
		$fechaFin =$_POST['fechaFin']." 11:59:59";

		$fechaInicioExplode = explode('-',$_POST['fechaInicio']);
		$fechaFinExplode = explode('-',$_POST['fechaFin']);

		$mesInicio = $fechaInicioExplode[1];
		$mesFin = $fechaFinExplode[1];
		$anioInicio = $fechaInicioExplode[0];
		$anioFin = $fechaFinExplode[0];

		$outp = array(); //Array que será convertido a JSON para sacarlo a vista.

		/*
		** Total de reportes totales historico.
		*/
		$sql="SELECT count(*) as total_reportes FROM reportes";
		$resultado=$connect->select($sql);
		array_push($outp,$resultado->fetch_all(MYSQLI_ASSOC));

		/*
		** Intervalo de meses.
		*/
		$sql="SELECT meses.aMonth aMonth, COUNT(re_fecha) total FROM (
	  		SELECT DATE_FORMAT(m1, '%m-%Y') aMonth
	  		FROM (    					
	   			SELECT
	    		('".$fechaInicio."' - INTERVAL DAYOFMONTH('".$fechaInicio."')-1 DAY) +INTERVAL m MONTH as m1 FROM (
					SELECT @rownum:=@rownum+1 m FROM
						(SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t1,
						(SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t2,
						(SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t3,
						(SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4) t4,
						(SELECT @rownum:=-1) t0 ) d1
					) d2 
	  				WHERE m1 <= '".$fechaFin."'
					ORDER BY m1) meses
			LEFT JOIN reportes ON DATE_FORMAT(re_fecha, '%m-%Y') = meses.aMonth
			WHERE re_fecha BETWEEN '".$fechaInicio."' AND '".$fechaFin."'
			OR re_fecha IS NULL
			GROUP BY meses.aMonth";
		
		$resultado = $connect->select($sql);
		array_push($outp,$resultado->fetch_all(MYSQLI_ASSOC));			

		/*
		** Estado de los reportes
		*/
		$sql="SELECT * FROM vw_status_reportes WHERE fecha_status BETWEEN '".$fechaInicio."' AND '".$fechaFin."'" ;
		$resultado=$connect->select($sql);
		array_push($outp,$resultado->fetch_all(MYSQLI_ASSOC));

		/*
		** Equipos reportados
		*/
		$sql="SELECT * FROM vw_equipos_reportados WHERE fecha BETWEEN '".$fechaInicio."' AND '".$fechaFin."'";
		$resultado=$connect->select($sql);
		array_push($outp,$resultado->fetch_all(MYSQLI_ASSOC));

		/*
		** Equipos reportados por dependencias.
		*/
		$sql="SELECT * FROM vw_reportes_dependencias WHERE fecha BETWEEN '".$fechaInicio."' AND '".$fechaFin."'" ;
		$resultado=$connect->select($sql);
		array_push($outp,$resultado->fetch_all(MYSQLI_ASSOC));

		/*
		** Salida de datos
		*/
		$connect->closeConection();
	  	echo json_encode($outp);
	}	
?>