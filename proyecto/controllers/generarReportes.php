<?php

  //Had to change this path to point to IOFactory.php.
  //Do not change the contents of the PHPExcel-1.8 folder at all.
  include('Classes/PHPExcel/IOFactory.php');

  //Use whatever path to an Excel file you need.
  $inputFileName = 'C:/test/test.csv';

  try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
  } catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . 
        $e->getMessage());
  }

  $sheet = $objPHPExcel->getSheet(0);
  $highestRow = $sheet->getHighestRow();
  $highestColumn = $sheet->getHighestColumn();

  
  for ($row = 2; $row <= $highestRow; $row++) {
	$cad="";
    $cad .= $objPHPExcel->getActiveSheet()->getCell('A'.$row)->getCalculatedValue()." ";
    $cad .= $objPHPExcel->getActiveSheet()->getCell('B'.$row)->getCalculatedValue()." ";
	$cad .= $objPHPExcel->getActiveSheet()->getCell('C'.$row)->getCalculatedValue()." ";
	$cad .= $objPHPExcel->getActiveSheet()->getCell('D'.$row)->getCalculatedValue()." ";
	$cad .= $objPHPExcel->getActiveSheet()->getCell('E'.$row)->getCalculatedValue()." ";
	$cad .= $objPHPExcel->getActiveSheet()->getCell('F'.$row)->getCalculatedValue()." ";
	$cad .= $objPHPExcel->getActiveSheet()->getCell('G'.$row)->getCalculatedValue()." ";
	$cad .= $objPHPExcel->getActiveSheet()->getCell('H'.$row)->getCalculatedValue()." ";


    //Prints out data in each row.
    //Replace this with whatever you want to do with the data.
    echo $cad ."<br>";
  }
?>