<?php
    require 'controladorConexionMySQL.php';
    require 'Classes/PHPExcel/IOFactory.php';

    $conn=new conectionSQL();
    $conn->startConection();

    date_default_timezone_set('America/Mexico_City');
    $filename=date('Y-m-d H:i:s');
    $filename=str_replace(':','',$filename);
    $filename=str_replace('-','',$filename);

    $sql = "SELECT * FROM srs.ver_inventario";
    $result=$conn->select($sql);
    if(mysqli_num_rows($result)>0){        
     
        // Instantiate a new PHPExcel object
        $objPHPExcel = new PHPExcel(); 
        // Set the active Excel worksheet to sheet 0
        $objPHPExcel->setActiveSheetIndex(0); 
        // Initialise the Excel row number
        $rowCount = 2;
        // Add descriptions to fields
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Tipo');            
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Marca');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Modelo');            
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Serie'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Patrimonio');            
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'SO'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Office');            
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Encargado'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Num trabajador');            
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Descripcion'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Folio');            
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Factura'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Programa Federal');            
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Proveedor');  
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Proveedor'); 
        // Iterate through each result from the SQL query in turn

        // We fetch each database result row into $row in turn
        while($row = mysqli_fetch_array($result)){ 

            // Set cell An to the "name" column from the database (assuming you have a column called name)
            //    where n is the Excel row number (ie cell A1 in the first row)
            $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $row['nameTipoEquipo']);            
            $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $row['marca']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $row['modelo']);            
            $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $row['serie']); 
            $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $row['patrimonio']);            
            $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $row['so']); 
            $objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $row['office']);            
            $objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $row['encargado']); 
            $objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $row['numTrabajador']);            
            $objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $row['description']); 
            $objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $row['folio']);            
            $objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $row['factura']); 
            $objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $row['programaFederal']);            
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $row['proveedor']); 
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $row['proveedor']); 
            $objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $row['idDependencia']); 
            // Increment the Excel row counter
            $rowCount++; 
        } 

        // Headers for the explorer.
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition:attachment; filename=$filename.xlsx");
        header('Cache-Control: max-age=0'); 
        header('Content-Transfer-Encoding: binary');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
        ob_end_clean();
        $objWriter->save('php://output');
        exit;

    }
?>