<?php 
	
	header('Content-Type: text/html; charset=UTF-8');
	include ("controladorConexionMySql.php");
	$conn=new conectionSQL();
	$conn->startConection();

	$itemsPorPag = 10;	
	$totalPaginas = 1;	
	$pagina=0;
	
	if(isset($_POST['page'])){
		$pagina=$_POST['page'];
	}else{
		$pagina=1;
	}

	$inicioResultado=($pagina-1)*$itemsPorPag;
	if(isset($_POST['idDependencia'])&&isset($_POST['busqueda'])){
		$sql="SELECT * FROM srs.ver_bibliotecarios WHERE (nombre LIKE '%".$_POST['busqueda']."%' OR apellido LIKE '%".$_POST['busqueda']."%') AND idDependencia=".$_POST['idDependencia']." ORDER BY apellido ASC LIMIT ".$inicioResultado.",".$itemsPorPag;

	}else if(isset($_POST['idDependencia'])){
		$sql="SELECT * FROM srs.ver_bibliotecarios WHERE idDependencia=".$_POST['idDependencia']." AND (idRol=3 OR idrol=5) ORDER BY id DESC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM srs.ver_bibliotecarios WHERE idDependencia=".$_POST['idDependencia']." AND (idRol=3 OR idrol=5)";
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

	}else if(isset($_POST['busqueda'])){
		$sql="SELECT * FROM srs.ver_bibliotecarios WHERE nombre LIKE '%".$_POST['busqueda']."%' OR apellido LIKE '%".$_POST['busqueda']."%' ORDER BY apellido ASC LIMIT ".$inicioResultado.",".$itemsPorPag;
	}else{
		$sql="SELECT * FROM srs.ver_bibliotecarios ORDER BY id DESC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$consultaTotal="SELECT count(*) AS total FROM srs.ver_bibliotecarios";
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);
	}
	
	$result = $conn->select($sql);
	$outp = array();
	$outp = $result->fetch_all(MYSQLI_ASSOC);

	if(!isset($_POST['busqueda'])){
		$arrayName = array(
		'totalPaginas' => $totalPaginas,
		'paginaAcual' => $pagina,
		);
		array_push($outp,$arrayName);
	}	
	
	echo json_encode($outp);
	$conn->closeConection();
	
?>
