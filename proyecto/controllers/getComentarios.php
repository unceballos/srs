<?php
	session_start();
	include("controladorConexionMySQL.php");
	$conn=new conectionSQL();
	$conn->startConection();

	$itemsPorPag = 3;	
	$totalPaginas = 1;	
	
	$pagina=0;
	$cad="";

	if(isset($_POST['page'])){
		$pagina=$_POST['page'];
	}else{
		$pagina=1;
	}

	$inicioResultado=($pagina-1)*$itemsPorPag;

	if(!isset($_POST['idReporte'])){
		echo "dataNull";
	}else{
		$sql="SELECT * FROM vw_comentarios_reportes WHERE idReporte=".$_POST['idReporte']." ORDER BY fecha DESC LIMIT ".$inicioResultado.",".$itemsPorPag;
		$result=$conn->select($sql);
		$outp = $result->fetch_all(MYSQLI_ASSOC);

		$consultaTotal="SELECT count(*) AS total FROM vw_comentarios_reportes WHERE idReporte=".$_POST['idReporte']." ORDER BY fecha ASC";
		$result=$conn->select($consultaTotal);
		$row=$result->fetch_assoc();
		$totalReportes=$row['total'];
		$totalPaginas=ceil($totalReportes/$itemsPorPag);

		$arrayName = array(
			'totalPaginas' => $totalPaginas,
			'paginaAcual' => $pagina,
		);
		array_push($outp,$arrayName);
  		echo json_encode($outp);
	}


	$conn->closeConection();
?>