<?php
	include ("controladorConexionMySQL.php");
	
	$id=$_POST['id'];
	$resultado = array();
	$conn=new conectionSQL();
	$conn->startConection();
	
	if($id!=""){

		$sql="SELECT * FROM ver_reportes_completos WHERE id_reporte=".$id;
		$result=$conn->select($sql);
		if($result->num_rows>0){
			$row=$result->fetch_assoc();
			$resultado = array(
        		'asuntoReporte' => $row['asunto_reporte'],
        		'fechaReporte' => $row['fecha_solicitud'],
        		'marca' =>$row['marca_equipo'],
        		'modelo'=>$row['modelo_equipo'],
        		'serie'=>$row['serie_equipo'],
        		'patrimonio'=>$row['patrimonio_equipo'],
        		'estado'=>$row['id_status'],
        		'servicio'=>$row['id_servicio'],
        		'solicitante'=>$row['nombre_solicitante']." ".$row['apellido_solicitante'],
        		'dependencia'=>$row['dependencia_solicitante'],
        		'reparador'=>$row['id_reparador'],
        		'trabajoSolicitado'=>$row['trabajos_solicitado'],
        		'trabajoRealizado'=>$row['trabajo_realizado'],
        		'imgTipoEquipo'=>$row['id_tipo_equipo']
        	);
			
			$myJSON = json_encode($resultado);
			echo $myJSON;			
		}else{
			echo "null";
		}
	}else{
		echo "noData";
	}
	$conn->closeConection();

?>