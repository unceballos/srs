	<?php
		session_start();
		if (empty($_SESSION['txtEmail'])) {
    		header('Location: login.php');
    		die();
    	}
    	if($_SESSION['txtRol']==1||$_SESSION['txtRol']==2){

    	}else{
    		header('Location: orders.php');
    		die();
    	}
	?>
	
	<html>
	<head>
		<title>Reportes</title>
		<link rel="shortcut icon" href="img/icon.ico">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
		rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">
		<link rel="stylesheet" type="text/css" href="css/biblioteca.css">
		<link rel="stylesheet" type="text/css" href="css/radios.css">
		<link rel="stylesheet" type="text/css" href="materialize/nouislider/nouislider.min.css">
	</head>
	<body class="blue-grey lighten-5" onload="getAllReports()">

		<!-- Navbar and Header -->
		<nav class="nav-extended cyan darken-3" style="margin-bottom: 4%">
			<div class="nav-background nabground">
				<div class="ea k"></div>
			</div>
			<div class="nav-wrapper db">
				<!-- LOGO -->
				<a href="#" data-activates="mobile" class="button-collapse"><i
				class="white-text material-icons">menu</i></a>
				<ul class="bt hide-on-med-and-down">
					<li><a class="dropdown-button white-text" href="#!" data-activates="dropdown1"><?php echo $_SESSION['txtEmail'];?><i class="material-icons right">arrow_drop_down</i></a></li>
				</ul>

				<div class="nav-header de">

					<div class="row">
						<div class="col s4 offset-s4 center-align">
							<img src="img/screwdriver.png" style="width: 20%; margin-bottom: -6%">
						</div>
					</div>
					<h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">Técnico</h3>
					<h1>REPORTES</h1>
				</div>
			</div>

			<!-- Dropdown Structure -->
			<ul id='dropdown1' class='dropdown-content ddd'>
				<li><a href="controllers/logout.php">Cerrar sesión</a></li>
			</ul>

			<div class="categories-wrapper row cyan darken-4">
			<div class="center-align">
				<ul>
					<li class="col s4 m2 offset-m3 k"><a href="TecnicoOrders.php" class="white-text">REPORTES</a></li>
					<li class="col s4 m2"><a href="bibliotecaHacer.php" class="white-text">HACER REPORTE</a></li>
					<li class="col s4 m2"><a href="TecnicoInventario.php" class="white-text">INVENTARIO</a></li>
				</ul>
			</div>
		</div>
		</nav>

		<!-- SIDE NAV -->
		<ul id="mobile" class="side-nav">
			<li><div class="userView">
				<div class="background">
					<img src="img/library.jpeg">
				</div>

				<a href="#!user"><img style="width:25%;"
					src="img/school.png"></a>

					<a href="#!name"><span class="white-text name">Técnico</span></a>

					<!-- Email of the user in here -->
					<a href="#!email"><span class="white-text email"><?php echo $_SESSION['txtEmail'];?></span></a>
				</div></li>

				<li><a class="subheader">Actividades</a></li>
				<li><a  class="waves-effect"href="bibliotecaVer.php" ><i
					class="material-icons">assignment</i>Ver mis reportes</a></li>
					<li><a class="waves-effect" href="bibliotecaHacer.php" ><i
						class="material-icons">mode_edit</i>Hacer un reporte</a></li>
						<li><a class="waves-effect" href="controllers/logout.php"> <i
							class="material-icons">perm_identity</i>Salir de mi cuenta</a></li>
						</ul>

						<!-- CONTENIDO DE LA PÁGINA -->
						<div class="row container">
						<div class="col s12 m12">
								<form id="busquedaReporte">
									<div class="input-field">
										<input id="buscarReportes" type="search" placeholder="Buscar en reportes por lo escrito en 'asunto'" onkeyup="buscarReporte()" autocomplete="off">
										<label class="label-icon" for="buscarReportes"><i class="material-icons">search</i></label>
										<i class="material-icons valign-wrapper" onclick="limpiarBusquedaReporte()">close</i>
									</div>
								</form>
						</div>
							
						</div>
						<div class="row container">
							<div class="col s12 m3">
								<div class="card grey lighten-5">
									<div class="card-content grey-text white">
										<span class="card-title"></span>
										<h5><b>Filtrar reportes <i class="material-icons">assignment</i></b></h5><br>
										

										<form id="filtrarReportes">
											<p><b>Estado</b></p>
											<p>
												<input class="with-gap" name="filtro" type="radio" id="todos" value="0" checked/>
												<label for="todos">Todos</label>
											</p>
											<p>
												<input class="with-gap" name="filtro" type="radio" id="pendientes" value="3"/>
												<label for="pendientes">En proceso</label>
											</p>
											<p>
												<input class="with-gap" name="filtro" type="radio" id="terminados" value="4"/>
												<label for="terminados">Terminados</label>
											</p>
											<p>
												<input class="with-gap" name="filtro" type="radio" id="entregados" value="5"/>
												<label for="entregados">Entregados</label>
											</p><br>

											<p><b>Fecha</b></p>

											<label for="fechaInicio" class="active">Rango de inicio</label>
											<input id="fechaInicio" type="text" class="datepicker" aria-haspopup="true" aria-expanded="true" aria-readonly="false">
											
											<label for="fechaFinal" class="active">Rango final</label>
											<input id="fechaFinal" type="text" class="datepicker" aria-haspopup="true" aria-expanded="true" aria-readonly="false">
											<button type="button" id="buttonFiltro" class="waves-effect waves-light btn wide">Filtrar</button>

										</form>


									</div>
								</div>
							</div>
							<div class="col s12 m9">

								
								<!-- REPORTES -->

								

								<div id="showRepots">

								</div>
								<br>
								<ul class="pagination center-align" id="paginationReports">
									
								</ul>
								<br>
	</body>
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/materialize.js"></script>
	<script src="js/scripts.js" /></script>
	<script>
		$(document).ready(function() {
		$('.modal').modal();});
		$('.dropdown-button').dropdown({belowOrigin: true});
		$(document).ready(function() {$('select').material_select();});
		$(".button-collapse").sideNav();
		$('.datepicker').pickadate({
		    selectMonths: true, // Creates a dropdown to control month
		    selectYears: 5, // Creates a dropdown of 15 years to control year,
		    today: 'Hoy',
		    clear: 'Limpiar',
		    close: 'Ok',
		    max: new Date(),
		    firstDay: true,
		    closeOnSelect: false,
		    format: 'yyyy-mm-dd' // Close upon selecting a date,
		  });
	</script>
	<script>
		$('#busquedaReporte').submit(function(e) {
			e.preventDefault();
		});
	</script>
	<script>
		$(document).ready(function() {
			$('#buttonFiltro').click(function() {
			var radioValue=$('#filtrarReportes').find( "input[type=radio][name='filtro']:checked" ).val();
			var fechaInicio=$('#fechaInicio').val();
			var fechaFinal=$('#fechaFinal').val();
			if(fechaInicio!="" && fechaFinal!=""){
				filtroCompleto(1,);
				$('#fechaInicio').val("");
				$('#fechaFinal').val("");
				var $input = $('#fechaInicio').pickadate();
				var picker = $input.pickadate('picker');				
				picker.set({
				  max: new Date(),
				  select: null
				});
				$input = $('#fechaFinal').pickadate();
				picker = $input.pickadate('picker');				
				picker.set({
				  max: new Date(),
				  select: null
				});
		
			}else if(radioValue!="0"){
				filtrarReportes(1);
			}else{
				getAllReports(1);			
			}    
		});
		});

		/*
		** Control de paginación
		*/
		$("#paginationReports").on("click","a.pages", function(){
			var radioValue=$('#filtrarReportes').find( "input[type=radio][name='filtro']:checked" ).val();
			var busqueda=$('#buscarReportes').val();
			if($('#fechaInicio').val()!="" && $('#fechaFinal').val()!=""){
				filtroCompleto($(this).attr('value'));
			}
			else if(radioValue!=0){
				filtrarReportes($(this).attr('value'));				
			}else{	
				getAllReports($(this).attr('value'));
			}
			$("html, body").delay(50).animate({
	    		scrollTop: $('#buscarReportes').offset().top 
			}, 200);
		});	


	</script>
</html>
