	<?php
		session_start();
		if (empty($_SESSION['txtEmail'])) {
    		header('Location: login.php');
    		die();
    	}
    	if($_SESSION['txtRol']!=2){
			header('Location: orders.php');
    		die();
    	}
	?>
	<html>
		<head>
		<title>Inventario</title>
		<link rel="shortcut icon" href="img/icon.ico">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
		rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">
		<link rel="stylesheet" type="text/css" href="css/biblioteca.css">
		<link rel="stylesheet" type="text/css" href="css/radios.css">
	</head>
	<body class="blue-grey lighten-5" onload="getInventario(1)">

		<!-- Navbar and Header -->
		<nav class="nav-extended cyan darken-3" style="margin-bottom: 4%">
			<div class="nav-background nabground">
				<div class="ea k"></div>
			</div>
			<div class="nav-wrapper db">
			    <!-- LOGO -->
				<a
					href="#" data-activates="mobile" class="button-collapse"><i
					class="white-text material-icons">menu</i></a>
				<ul class="bt hide-on-med-and-down">
					      <li><a class="dropdown-button white-text" href="#!" data-activates="dropdown1"><?php echo $_SESSION['txtEmail']?><i class="material-icons right">arrow_drop_down</i></a></li>
				</ul>

				<div class="nav-header de">


					<div class="row">
					<div class="col s4 offset-s4 center-align">
					<img src="img/screwdriver.png" style="width: 20%; margin-bottom: -6%">
				</div>
					</div>
						<h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">Técnico</h3>
						<h1>INVENTARIO</h1>
				</div>
			</div>

			<!-- Dropdown Structure -->
			<ul id='dropdown1' class='dropdown-content ddd'>
				<li><a href="controllers/logout.php">Cerrar sesión</a></li>
			</ul>

			<!-- Pestañitas (hide-on-med-and-down)-->
			<div class="categories-wrapper row cyan darken-4">
				<!--categories-db col s4 offset-s4center-align">-->
				<div class="center-align">
					<ul>
						<li class="col s4 m2 offset-m3"><a href="TecnicoOrders.php" class="white-text">REPORTES</a></li>
						<li class="col s4 m2"><a href="bibliotecaHacer.php" class="white-text">HACER REPORTE</a></li>
						<li class="col s4 m2 K"><a href="TecnicoInventario.php" class="white-text">INVENTARIO</a></li>

					</ul>
				</div>
			</div>
		</nav>

		<!-- SIDE NAV -->
		<ul id="mobile" class="side-nav">
			<li><div class="userView">
					<div class="background">
						<img src="img/library.jpeg">
					</div>

					<a href="#!user"><img style="width:25%;"
						src="img/school.png"></a>

					<a href="#!name"><span class="white-text name">Técnico</span></a>

					<!-- Email of the user in here -->
					<a href="#!email"><span class="white-text email"><?php echo $_SESSION['txtEmail']?></span></a>
				</div></li>

			<li><a class="subheader">Actividades</a></li>
			<li><a  class="waves-effect"href="bibliotecaVer.php" ><i
					class="material-icons">assignment</i>Ver mis reportes</a></li>
			<li><a class="waves-effect" href="bibliotecaHacer.php" ><i
					class="material-icons">mode_edit</i>Hacer un reporte</a></li>
			<li><a class="waves-effect" href="controllers/logout.php"> <i
					class="material-icons">perm_identity</i>Salir de mi cuenta</a></li>
		</ul>

		<!-- CONTENIDO DE LA PÁGINA -->
		<div class="row container">
			<div class="col s12 m12">
				<form id="busquedaEquipo">
			        <div class="input-field">
			          <input style="height:45px;" id="buscarInventario" placeholder="Buscar por patrimonio" type="search" onkeyup="buscarEquipo()" autocomplete="off">
			          <label class="label-icon" for="buscarInventario"><i class="material-icons">search</i></label>
			          <i class="material-icons valign-wrapper" onclick="limpiarBusquedaEquipo()">close</i>
			        </div>
	    		</form><br>

							<div class="row ">
								<div class="col s12 m12 ">
									<table class="bordered highlight centered">
        								<thead>
								        	<tr>
								            	<th>Tipo</th>
								            	<th>Marca</th>
								            	<th>Modelo</th>
								            	<th>Serie</th>
								            	<th>Patrimonio</th>
												<th>Dependencia</th>
								            	<th>S.O.</th>
								            	<th>Office</th>
								            	<th>Custodio</th>
								            	<th>Acción</th>
								          	</tr>
								        </thead>

								        <tbody id="getInventario">
								        </tbody>
								    </table>
								    <br>
								    <ul class="pagination center-align">										
									</ul>
								</div>
							</div>

				<!-- FIN DEL CONTENEDOR -->

			<!-- DISPARADOR DEL MODAL -->
			<div class="fixed-action-btn">
			   <a href="#modalCrear" class="btn-floating btn-large cyan darken-2 tooltipped" data-position="left" data-delay="1200" data-tooltip="Agregar equipo"><i class="material-icons">add</i></a>
			  </div>
	<!-- Modal Structure -->

  <div id="modalCrear" class="modal modal-fixed-footer">
  <form id="agregarEquipo">
    <div class="modal-content">
      <h4>Agregar nuevo equipo</h4>
      <p>Ingrese los datos del nuevo equipo y haga clic en "Aceptar" para agregarlo al inventario</p>

      
		<!-- DISPOSITIVOS VAN AQUI -->
		<!-- IMPRESORA -->
		<div class="row">
			<p class="grey-text  text-darken-3" style="margin-left: 20px"><b>Equipo</b></p>
			<div>
				<div class="col s6 m2 offset-m1">
					<label>
						<input type="radio" name="equipo" value="1" checked />
						<img src="img/1.png">
						<p class="center-align">Impresora</p>
					</label>
				</div>

				<!-- MINIPRINTER -->
				<div class="col s6 m2">
					<label>
						<input type="radio" name="equipo" value="2" />
						<img src="img/2.png">
						<p class="center-align">Miniprinter</p>
					</label>
				</div>

				<!-- LECTOR DE CODIGO DE BARRAS -->
				<div class="col s6 m2">
					<label>
						<input type="radio" name="equipo" value="3" />
						<img src="img/3.png">
						<p class="center-align">Lector de CB</p>
					</label>
				</div>

				<!-- PC -->
				<div class="col s6 m2">
					<label>
						<input type="radio" name="equipo" value="4" id="equipo" />
						<img src="img/4.png">
						<p class="center-align">Computadora</p>
					</label>
				</div>

				<!-- PANTALLA -->
				<div class="col s6 m2">
					<label>
						<input type="radio" name="equipo" value="5" id="pantalla" />
						<img src="img/5.png">
						<p class="center-align">Pantalla</p>
					</label>
				</div>

				<!-- ESCANER -->
				<div class="col s6 m2 ">
					<label>
						<input type="radio" name="equipo" value="6" id="escaner" />
						<img src="img/6.png">
						<p class="center-align">Escáner</p>
					</label>
				</div>

				<!-- PROYECTOR -->
				<div class="col s6 m2">
					<label>
						<input type="radio" name="equipo" value="7" id="proyector" />
						<img src="img/7.png">
						<p class="center-align">Proyector</p>
					</label>
				</div>

				<!-- VIDEO CASETERA -->
				<div class="col s6 m2">
					<label>
						<input type="radio" name="equipo" value="8" id="videocas" />
						<img src="img/8.png">
						<p class="center-align">Videocasetera</p>
					</label>
				</div>

				<!-- Mobiliario -->
				<div class="col s6 m2">
					<label>
						<input type="radio" name="equipo" value="10" id="mobiliario" />
						<img src="img/10.png">
						<p class="center-align">Mobiliario</p>
					</label>
				</div>

				<!-- Alumbrado -->
				<div class="col s6 m2">
					<label>
						<input type="radio" name="equipo" value="11" id="alumbrado" />
						<img src="img/11.png">
						<p class="center-align">Alumbrado</p>
					</label>
				</div>

				<!-- OTRO -->
				<div class="col s6 m2">
					<label>
						<input type="radio" name="equipo" value="9" id="otro" />
						<img src="img/9.png">
						<p class="center-align">Otro</p>
					</label>
				</div>
			</div>
		</div>
		<br>
		<p class="grey-text  text-darken-3" style="margin-left: 20px"><b>Datos del equipo</b></p>
		
		<div class="row">
			<div class="col s12 col m10 offset-m1 left-align" style="margin-top: 20px;">
				<div class="row">

					<div class="input-field col s3">
						<input id="patrimonio" type="text" class="validate counter" data-length="45" maxlength="45" name="txtPatrimonio">
						<label for="patrimonio">Patrimonio</label>
					</div>

					<div class="input-field col s3">
						<input id="folio" type="text" class="validate counter" data-length="3" maxlength="3" name="txtFolio">
						<label for="folio">Folio</label>
					</div>

					<div class="input-field col s3">
						<input id="factura" type="text" class="validate counter" data-length="10" maxlength="10" name="txtFactura">
						<label for="factura">Factura</label>
					</div>

					<div class="input-field col s3">
						<input id="programaFederal" type="text" class="validate counter" data-length="15" maxlength="15" name="txtProgramaFederal">
						<label for="programaFederal">Programa federal</label>
					</div>
					
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col s12 col m10 offset-m1 left-align" style="margin-top: 20px;">
				<div class="row">

					<div class="input-field col s3">
						<input id="marca" type="text" class="validate counter" data-length="45" maxlength="45" name="txtMarca">
						<label for="marca">Marca</label>
					</div>

					<div class="input-field col s3">
						<input id="modelo" type="text" class="validate counter" data-length="45" maxlength="45" name="txtModelo">
						<label for="modelo">Modelo</label>
					</div>

					<div class="input-field col s3">
						<input id="serie" type="text" class="validate counter" data-length="60" maxlength="60" name="txtSerie">
						<label for="serie">Serie</label>
					</div>

					<div class="input-field col s3">
						<input id="proveedor" type="text" class="validate autocomplete counter" maxlength="100" name="txtProveedor">
						<label for="proveedor">Proveedor</label>
					</div>
					
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col s12 col m10 offset-m1 left-align" style="margin-top: 20px; ">
				<div class="row">
					<div class="input-field col s3">
						<input id="so" type="text" class="validate counter" data-length="45" maxlength="45" name="txtSO">
						<label for="so">S.O.</label>
					</div>

					<div class="input-field col s3">
						<input id="office" type="text" class="validate counter" data-length="20" maxlength="20" name="txtOffice">
						<label for="office">Office</label>
					</div>

					<div class="input-field col s3">
						<input id="nombreUsuario" type="text" class="validate counter autocomplete" maxlength="100" name="nombreUsuario">
						<label for="nombreUsuario">Encargado</label>						
					</div>

					<div class="input-field col s3">
						<input id="numTrabajador" type="text" class="validate counter" maxlength="4" name="txtNumTrabajador">
						<label for="numTrabajador">N° de trabajador</label>						
					</div>

					<div class="input-field col s12">
						<select name="dependencia" id="selectDependencia"></select>
			    		<label>Dependencia</label>
			  		</div>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col s12 col m10 offset-m1 left-align" style="margin-top: 20px; ">
				<div class="row">
					<div class="input-field col s12">
						<textarea id="description" class="materialize-textarea" name="txtDescription"></textarea>
          				<label for="description">Descripción / Ubicación</label>
					</div>
				</div>				
			</div>
		</div>
	</div>
	</form>
	<div class="modal-footer">
    	<button style="margin-right:15px; margin-right:15px;" type="button" class="modal-action blue waves-effect waves-green btn-flat white-text" onclick="agregarEquipo()">Aceptar</button>
    	<button style="margin-right:15px; margin-right:15px;" type="button" class="modal-action modal-close waves-effect waves-red btn-flat" onclick="clearAddEquipo()">Cancelar</button>
    	<!-- Trigger del modal para subir archivos excel -->
		<a style="margin-right:70px;" class="waves-effect waves-light btn blue lighten-2 modal-trigger left-align" href="#modalExcel">O use un archivo de Excel</a>
    </div>



 
</div>

</div>
</div>
	</body>
	 <!-- Modal Structure -->
  <div id="modalExcel" class="modal bottom-sheet">
    <div class="modal-content">
      <h4>Agregar a inventario mediante archivo de excel</h4>
      <div class="row">
		<p class="col m8">Haga clic en el botón de "SELECCIONAR" para seleccionar el archivo con los registros. Una vez seleccionado el archivo, haga clic en "ENVIAR" para confirmar.</p>
		<form class="col m4" method="get" action="uploadsExcel/plantilla.xlsx">
			<button class="waves-effect waves-light btn down" type="submit">Descargar archivo de ejemplo</button>
			<button type="button" style="margin-top: 10px" class="waves-effect waves-light btn" onclick="exportarInventario()" >Exportar inventario</button>

		</form>
	  </div>
	 
	  <!--Aqui empieza lo de subir al inventario mediante un excel. -->
      <form id="subirExcel" class="row" enctype="multipart/form-data" method="POST" style="	height: 46px;">
    <div class="file-field input-field">

      <div class="col s2 waves-effect waves-light btn">
        <span>SELECCIONAR</span>
        <input type="file" name="uploadedfile" accept=".csv, .xls, .xlsx">
      </div>

      <div class="col s3 file-path-wrapper">
        <input class="file-path validate" type="text" placeholder="Haga clic en el botón de 'SELECCIONAR'">
      </div>
      
    </div>
    <div class="col s2 file-path-wrapper">
    	<button class="btn fitbtn waves-effect waves-light light-blue" onclick="uploadFile()">ENVIAR  <i class="material-icons right">send</i></button>   
	</div>
	<div id="imgLoading" class"col s1 left-align" hidden>
		<img  src="img/loading.gif" class="loadingImg">
	</div>

  </form>

  <div id="showErrores">
  	
  </div>
    </div>
  </div>


	<script src="js/jquery-2.1.4.min.js" />"></script>
	<script src="js/materialize.min.js" />"></script>
	<script src="js/scripts.js" /></script>
	<script>
		$(document).ready(function() {
			$('.dropdown-button').dropdown({belowOrigin: true});
			$('#so').attr('disabled', true);
			$('#office').attr('disabled', true);
			$('select').material_select();
			$('.modal').modal();
			$(".button-collapse").sideNav();
			/*$('#nombreUsuario').on('keyup', function(){
				if($(this).val()==""){
					$('#numTrabajador').val("").removeClass('valid');;
					Materialize.updateTextFields();
				}
			});*/

			$('#numTrabajador').on('keyup', function(e){
				var code = e.keyCode || e.which;
				if (code != '9') {
					if($(this).val()!=""){
						getEncargado($(this).val());
					}
				}
				
			});
			$('#nombreUsuario').on('change', function(){
				if($(this).val()==""){
					$('#numTrabajador').val("");
					Materialize.updateTextFields();
				}

			});

			$('#numTrabajador').on('change', function(){
				if($(this).val()==""){
					$('#nombreUsuario').val("");
					Materialize.updateTextFields();
				}

			});

			$("#numTrabajador").keypress(function (e) { //Solo numeros
				if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					return false;
				}
			});
		});

		$(".pagination").on("click","a.pages", function(){
			getInventario($(this).attr('value'));
			$("html, body").delay(50).animate({
				scrollTop: $('#buscarInventario').offset().top 
			}, 200);

		});	

		$('input[type="radio"]').on("click", function(){
			if(!$('#equipo').is(':checked')){	

				$('#so').attr('disabled', true).val("").removeClass('valid');
				$('#office').attr('disabled', true).val("").removeClass('valid');
				Materialize.updateTextFields();

			}else{
				$('#so').removeAttr('disabled');
				$('#office').removeAttr('disabled');

			}
		});	

		$('#busquedaEquipo').submit(function(e) {
			e.preventDefault();
		});

		/*$('#proveedor').on('keyup',function(){
			
		});*/
	</script>
</html>

	</script>
</html>
