
<html>
<head>
<title>Registrarse</title>
<link rel="shortcut icon" href="img/icon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"	rel="stylesheet">
<link href="materialize/css/materialize.min.css" rel="stylesheet">
<link href="css/signup.css" rel="stylesheet">
<link href="css/navbar.css" rel="stylesheet">

</head>

<body onload="llenarBibliotecas()">

	    <!-- NAV -->
    <nav class="transparent z-depth-0">
        <div class="nav-wrapper db   ">
            <a href="./" class="brand-logo teal-text ">
                <i class="material-icons">camera</i>SRS</a>

            <a href="/" data-activates="mobile" class="button-collapse grey-text">
                <i class="material-icons">menu</i>
            </a>
            <ul class="right hide-on-med-and-down">
                        <a href="login.php" class="transparent waves-effect teal-text hovery">Iniciar sesión</a>
            </ul>
        </div>
    </nav>

	<div class="de">
          <h4 class="grey-text text-darken-2" style="margin:1%;">Crear una cuenta de directivo</h4>
          <div class="grey-text text-darken-2 ge">Accede a los servicios de ésta plataforma de manera gratuita. Sólo registre sus datos en éste formulario.</div>
      </div>


	<div class="container row valign" style="margin-top: 4%;">
		<div class="valign-wrapper col s12 m4 center-align">
			<img class="responsive valign" src="img/5.png"
				style="display: block; margin-left: auto; margin-right: auto; width: 65%;">
		</div>

		<div class="col s12 m8">
			<form id="formSignup" class="col s12">
			
				<div class="row">
					<div class="input-field col s12 m6">
						<input id="first_name" type="text" class="validate" name="txtFName"> <label
							for="first_name">Nombre</label>
					</div>
					<div class="input-field col s12 m6">
						<input id="last_name" type="text" class="validate" name="txtLName"> <label 
							for="last_name">Apellido</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="numTrabajador" type="text" class="validate" maxlength="4" name="txtNumTrabajador" > <label
							for="numTrabajador">N° de trabajador</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="tel" type="tel" class="validate" name="txtPhone" > <label
							for="tel">Teléfono</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="email" class="validate" name="txtEmail" > <label
							for="email">Correo</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<select name="dependencia" id="selectDependencia"></select>
			    		<label>Dependencia</label>
			  		</div>
			  	</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="password" type="password" class="validate" name="txtPass"> <label
							for="password">Contraseña</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="chkpassword" type="password" class="validate" name="txtCheckPass"> <label
							for="chkpassword">Repetir contraseña</label>
					</div>
				</div>


				<div class="row">
					<div class="col s12 center-align">
						<button id="btn" class="btn waves-effect waves-light" type="button" name="action" onclick="signupDirectivo()">Registrarse</button> <!--cambie el tipo de submit a button. En onclick hablo a javascript y mando parametros en la funcion de javascript-->
						
					</div>
				</div>
			</form>
	</div>
	</div>
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/scripts.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
    	$('select').material_select();
  		});
	</script>
</body>
</html>
