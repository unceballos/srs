<?php
   session_start();
   if (empty($_SESSION['txtEmail'])) {
     		header('Location: login.php');
     		die();
     	}
     	if($_SESSION['txtRol']!=4){
     		if($_SESSION['txtRol']==2){
     			header('Location: tecnicoOrders.php');
     		}else if($_SESSION['txtRol']==3){
     			header('Location: bibliotecaVer.php');
     		}else if($_SESSION['txtRol']==1){
     			header('Location: administradorBiblioteca.php');
     		}
   }
   ?>
<html>
   <head>
      <title>Bibliotecas</title>
      <link rel="shortcut icon" href="img/icon.ico">
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
      <link rel="stylesheet" type="text/css" href="css/navbar.css">
      <link rel="stylesheet" type="text/css" href="css/biblioteca.css">
      <link rel="stylesheet" type="text/css" href="css/radios.css">
   </head>
   <body class="blue-grey lighten-5" onload="getBibliotecariosByDependencia(1,<?php echo $_SESSION['idDependencia'];?>)">
      <!-- Navbar and Header -->
      <nav class="nav-extended cyan darken-3" style="margin-bottom: 4%">
         <div class="nav-background nabground">
            <div class="ea k"></div>
         </div>
         <div class="nav-wrapper db">
            <!-- LOGO -->
            <a href="" data-activates="mobile" class="button-collapse"><i class="white-text material-icons">menu</i></a>
            <ul class="bt hide-on-med-and-down">
               <li><a class="dropdown-button white-text" href="#!" data-activates="dropdown1"><?php echo $_SESSION['txtEmail'];?><i class="material-icons right">arrow_drop_down</i></a></li>
            </ul>
            <div class="nav-header de">
               <div class="row">
                  <div class="col s4 offset-s4 center-align">
                     <img src="img/school.png" style="width: 20%; margin-bottom: -6%">
                  </div>
               </div>
               <h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">ENCARGADO</h3>
               <h1>BIBLIOTECAS</h1>
            </div>
         </div>
         <!-- Dropdown Structure -->
         <ul id='dropdown1' class='dropdown-content ddd'>
            <li><a href="controllers/logout.php">Cerrar sesión</a></li>
         </ul>
         <div class="categories-wrapper row cyan darken-4">
            <div class="center-align">
               <ul>
                  <li class="col s6 m2 offset-m3"><a href="directivoVer.php" class="white-text">VER MIS REPORTES</a></li>
                  <li class="col s6 m2"><a href="bibliotecaHacer.php" class="white-text">HACER REPORTE</a></li>
                  <li class="col s6 m2 k"><a href="directivoBiblioteca.php" class="white-text">BIBLIOTECARIOS</a></li>
               </ul>
            </div>
         </div>
      </nav>
      <!-- SIDE NAV -->
      <ul id="mobile" class="side-nav">
         <li>
            <div class="userView">
               <div class="background">
                  <img src="img/library.jpeg">
               </div>
               <a href="#!user"><img style="width:25%;" src="img/school.png"></a>
               <a href="#!name"><span class="white-text name">Técnico</span></a>
               <!-- Email of the user in here -->
               <a href="#!email"><span class="white-text email"><?php echo $_SESSION['txtEmail'];?></span></a>
            </div>
         </li>
         <li><a class="subheader">Actividades</a></li>
         <li><a class="waves-effect" href="bibliotecaVer.php" ><i class="material-icons">assignment</i>Ver mis reportes</a></li>
         <li><a class="waves-effect" href="bibliotecaHacer.php" ><i class="material-icons">mode_edit</i>Hacer un reporte</a></li>
         <li><a class="waves-effect" href="controllers/logout.php"> <i class="material-icons">perm_identity</i>Salir de mi cuenta</a></li>
      </ul>
      <!-- CONTENIDO DE LA PÁGINA -->
      <div class="row container">
         <div class="col s12 m12">
            <div class="row">
               <div class="col s12 m12 center-align">
                  <img src="img/lib1.png" width="100px"><br>
                  <p class="grey-text text-darken-1 ">Aquí se mostrarán los usuarios que usted haya asignado para ayudar a su biblioteca. </p>
                  <p class="grey-text text-darken-1 "> Para agregar un nuevo bibliotecario, haga clic en el botón ubicado en la parte inferior derecha de la pantalla</p>
               </div>
            </div>
            <form id="busquedaBibliotecario">
               <div class="input-field">
                  <input style="height:45px;" id="buscarBibliotecario" placeholder="Buscar por nombre o apellido" type="search" autocomplete="off" required>
                  <label class="label-icon" for="buscarBibliotecario"><i class="material-icons">search</i></label>
                  <i class="material-icons valign-wrapper">close</i>
               </div>
            </form>
            <br>
            <div class="row"
            <div class="card horizontal col s9">
               <div class="card-content" style="margin:auto;">
                  <div class="row ">
                     <div class="col s12 m12 ">
                        <table class="bordered highlight centered">
                           <thead>
                              <tr>
                                 <th>Rol</th>
                                 <th>Nombre</th>
                                 <th>Apellido</th>
                                 <th>Correo</th>
                                 <th>Teléfono</th>
                                 <th>Clave - Biblioteca</th>
                                 <th>Aprobado</th>
                                 <th>Editar</th>
                              </tr>
                           </thead>
                           <tbody id="getResult">
                           </tbody>
                        </table>
                        <br>
                        <ul class="pagination center-align"></ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- FIN DEL CONTENEDOR -->
      <!-- DISPARADOR DEL MODAL -->
      <div class="fixed-action-btn">
         <a href="#modalCrearBib" class="btn-floating btn-large cyan darken-2 tooltipped" data-position="left" data-delay="1200" data-tooltip="Agregar equipo"><i class="material-icons">add</i></a>
      </div>
      <!-- Modal Structure -->
      <div id="modalCrearBib" class="modal modal-fixed-footer">
         <div class="modal-content">
            <h4>Agregar nuevo personal a biblioteca</h4>
            <p>Ingrese los datos del nuevo bibliotecario y haga clic en "Aceptar" para agregarlo al sistema</p>
            <p class="grey-text  text-darken-3"><b>Datos del bibliotecario</b></p>
            <form id="agregarBibliotecario">
               <div style="margin-top: 20px; ">
                  <div class="row">
                     <div class="input-field col s4">
                        <input id="correo" type="text" name="txtCorreo" autocomplete="off">
                        <label for="correo" class="active">Correo</label>
                     </div>
                     <div class="input-field col s4">
                        <input id="telefono" type="text" name="txtTelefono" autocomplete="off">
                        <label for="telefono" class="active">Teléfono</label>
                     </div>
                     <div class="input-field col s4">
                        <input id="pass" type="password" name="txtPass" autocomplete="off">
                        <label for="pass" class="active">Contraseña</label>
                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s4">
                        <input id="nombre" type="text" name="txtNombre" autocomplete="off">
                        <label for="nombre" class="active">Nombre</label>
                     </div>
                     <div class="input-field col s5">
                        <input id="apellido" type="text" name="txtApellido" autocomplete="off">
                        <label for="apellido" class="active">Apellido</label>
                     </div>
                     <div class="input-field col s3">
                        <input id="numTrabajador" type="text" name="txtNumTrabajador" maxlength="4" autocomplete="off">
                        <label for="pass" class="active"># de trabajador</label>
                     </div>
                  </div>
                  <input type="hidden" name="dependencia" value="<?php echo $_SESSION['idDependencia']?>">
                  <div class="row">
                     <div class="input-field col s12">
                        <select name="rol">
                           <option value="3">Bibliotecario</option>
                           <option value="5">Bibliotecario Informático</option>
                        </select>
                        <label>Rol</label>
                     </div>
                  </div>
               </div>
            </form>
            <br>
         </div>
         <div class="modal-footer">
            <button onclick="agregarBibliotecarioByDirectivo()" type="button" style="margin-right:15px; margin-right:15px;" class="modal-action blue waves-effect waves-green btn-flat white-text">Aceptar</button>
            <button onclick="clearAddBibliotecario()" type="button" style="margin-right:15px; margin-right:15px;" class="modal-action modal-close waves-effect waves-red btn-flat ">Cancelar</a>
         </div>
      </div>
   </body>
   <script src="js/jquery-2.1.4.min.js" />"></script>
   <script src="js/materialize.min.js" />"></script>
   <script src="js/scripts.js" />"></script>
   <script>
      $(document).ready(function() {
      
      
      		$('#busquedaBibliotecario').submit(function(e) {
      		e.preventDefault();
      	});
      
      	llenarBibliotecas();
      $('.modal').modal();});
      $('.dropdown-button').dropdown({belowOrigin: true});
      $(document).ready(function() {$('select').material_select();});
      $(".button-collapse").sideNav();
      $(".pagination").on("click","a.pages", function(){
      			getBibliotecariosByDependencia($(this).attr('value'),<?php echo $_SESSION['idDependencia']?>);
      			$("html, body").delay(50).animate({
            		scrollTop: $('div.container').offset().top 
        		}, 200);
      
      });		
      $('#busquedaBibliotecario').on("click","i.valign-wrapper", function(){
      			$('#buscarBibliotecario').val('');
      			getBibliotecariosByDependencia(1,<?php echo $_SESSION['idDependencia'];?>)
      });
      
      $('#busquedaBibliotecario').on("keyup","#buscarBibliotecario", function(){
      			if($("#buscarBibliotecario").val()==""){
      		getBibliotecariosByDependencia(1,<?php echo $_SESSION['idDependencia'];?>)
      			}else{
      		buscarBibliotecarioByDirectivo(<?php echo $_SESSION['idDependencia'];?>);
      			}
      });			
   </script>
</html>