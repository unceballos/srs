<?php
	session_start();
	if (empty($_SESSION['txtEmail'])) {
    	header('Location: login.php');
    	die();
    }
    if($_SESSION['txtRol']!=1){
		header('Location: orders.php');
    	die();
    }
    $email=$_SESSION['txtEmail'];
    $id=$_GET['id'];
?>
<html>
<head>
	<title>Editar usuario</title>
	<link rel="shortcut icon" href="img/icon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/navbar.css">
	<link rel="stylesheet" type="text/css" href="css/biblioteca.css">
	<link rel="stylesheet" type="text/css" href="css/radios.css">
</head>
<body class="blue-grey lighten-5" onload="getTecnico()">
	<!-- Navbar and Header -->
	<nav class="nav-extended cyan darken-3" style="margin-bottom: 4%">
		<div class="nav-background nabground">
			<div class="ea k"></div>
		</div>
		<div class="nav-wrapper db">
			<!-- LOGO -->
			<a href="#" data-activates="mobile" class="button-collapse"><i class="white-text material-icons">menu</i></a>
			<ul class="bt hide-on-med-and-down">
				<li>
					<a class="dropdown-button white-text" href="#!" data-activates="dropdown1"><?php echo $_SESSION['txtEmail'];?><i class="material-icons right">arrow_drop_down</i></a>
				</li>
			</ul>
			<div class="nav-header de">


				<div class="row">
					<div class="col s4 offset-s4 center-align">
						<img src="img/helmet.png" style="width: 20%; margin-bottom: -6%">
					</div>
				</div>
				<h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">Administrador</h3>
				<h1 id="h1Email"></h1>
			</div>
		</div>

		<!-- Dropdown Structure -->
		<ul id='dropdown1' class='dropdown-content ddd'>
			<li><a href="controllers/logout.php">Cerrar sesión</a></li>
		</ul>
		
		<!-- Pestañitas (hide-on-med-and-down)-->
		<div class="categories-wrapper row cyan darken-4">
			<div class="center-align">
				<ul>
					<li class="col s4 m2 offset-m3 k"><a href="AdministradorTecnicos.php" class="white-text">TÉCNICOS</a></li>
					<li class="col s4 m2"><a href="AdministradorBiblioteca.php" class="white-text">BIBLIOTECAS</a></li>
					<li class="col s4 m2"><a href="AdministradorEstadisticas.php" class="white-text">ESTADÍSTICAS</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<!-- SIDE NAV -->
	<ul id="mobile" class="side-nav">
		<li>
			<div class="userView">
				<div class="background">
					<img src="img/library.jpeg">
				</div>
				<a href="#!user"><img style="width:25%;" src="img/school.png"></a>
				<a href="#!name"><span class="white-text name">Técnico</span></a>
				<!-- Email of the user in here -->
				<a href="#!email"><span class="white-text email"><?php echo $_SESSION['txtEmail'];?></span></a>
			</div>
		</li>
		<li><a class="subheader">Actividades</a></li>
		<li><a  class="waves-effect"href="bibliotecaVer.php" ><i class="material-icons">assignment</i>Ver mis reportes</a></li>
		<li><a class="waves-effect" href="bibliotecaHacer.php" ><i class="material-icons">mode_edit</i>Hacer un reporte</a></li>
		<li><a class="waves-effect" href="controllers/logout.php"> <i class="material-icons">perm_identity</i>Salir de mi cuenta</a></li>
	</ul>

	<!-- CONTENIDO DE LA PÁGINA -->
	<div class="row container">
		<div class="col s12 m12">		
			<div class="card horizontal">
				<div class="card-stacked">		          
					<div class="card-content">
						<form id="showTecnico">
						<input type="hidden" name="hdIdTecnico" value="<?php echo $id; ?>">
							<div class="row">
								<h5 class="grey-text text-darken-3 center-align" id="h5Nombre"></h5>
								<p class="grey-text center-align" id="pTipo">Técnico</p>
								<br>
								<div class="col s12 m12 center-align">
									<img class="circle">
								</div>

								<div class="row"><div class="col s12 m8 offset-m2 left-align" style="margin-top: 20px; ">
									<div class="row">
										<div class="row">
											<div class="col s12 m6">
											<div class="input-field col m5 ">
													<input id="correo" name="txtCorreo" type="text" autocomplete="off">
													<label for="correo" class="active">Correo</label>
												</div>
												<div class="input-field col m4">
													<input id="telefono" name="txtTelefono" type="text" autocomplete="off">
													<label for="telefono" class="active">Teléfono</label>
												</div>
												<div class="input-field col m3">
													<input id="numTrabajador" name="txtNumTrabajador" type="text" autocomplete="off">
													<label for="numTrabajador" class="active">N° trabajador</label>
												</div>
											</div>

											<div class="col s12 m6 row">
												<div class="input-field col m4">
													<input id="pass" name="txtPass" type="password" autocomplete="off">
													<label for="pass" class="active">Contraseña</label>
												</div>
												<div class="input-field col m4">
													<input id="nombre" type="text" name="txtNombre" autocomplete="off">
													<label for="nombre" class="active">Nombre</label>
												</div>
												<div class="input-field col m4">
													<input id="apellido" type="text" name="txtApellido" autocomplete="off">
													<label for="apellido" class="active">Apellido</label>
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div><br>

							<div class="card-action center-align row">
								<div class="col s2">
									<a href="#modalBorrarUSR" class="btn-flat black-text tooltipped" data-position="bottom" data-delay="1300" data-tooltip="Borrar usuario" data-tooltip-id="efaf4648-391d-2a52-9c89-6d7c8a90c5ce"><i class="material-icons">delete</i></a>
								</div>
								<div class="col s7" style="margin-left: 6%">
									<button onclick="getTecnico()" type="button" class="btn-flat black-text tooltipped" data-position="bottom" data-delay="1300" data-tooltip="Descartar los cambios hechos y restablecer los datos de este usuario a su estado original" data-tooltip-id="efaf4648-391d-2a52-9c89-6d7c8a90c5ce"><i class="material-icons right">clear_all</i>Descartar</button>
									<button onclick="updateTecnico()" type="button" class="blue darken-1 waves-effect waves-light btn  tooltipped" data-position="bottom" data-delay="1300" data-tooltip="Enviar los cambios hechos a éste usuario" data-tooltip-id="d9896dd0-750b-32e8-3efd-adb03ecc8611"><i class="material-icons right">done</i>Modificar</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- FIN DEL CONTENEDOR -->

  	<!-- Modal Structure -->
  	<div id="modalBorrarUSR" class="modal">
    	<div class="modal-content">
      		<h4>Borrar un técnico</h4>
      		<p>Haca clic en el botón de 'Sí' para confirmar la eliminación de éste usuario, o en 'Cancelar' para anular la operación.</p>
      		<div class="row">
      			<div class="col s8 offset-s2 center-align">
      				<img src="img/barrier.png" width="140px">
      				<p>¿Está seguro de que desea eliminar a este usuario? Si se elimina el usuario ya no tendrá más acceso</p>
      			</div>
      		</div>
    	</div>
    	<div class="modal-footer">
    		<button onclick="deleteTecnico()" type="button" style="margin-right:15px; margin-right:15px;" class="modal-action blue modal-close waves-effect waves-green btn-flat white-text">Sí</button>
    		<a href="#!" style="margin-right:15px; margin-right:15px;" class="modal-action modal-close waves-effect waves-red btn-flat ">Cancelar</a>
    	</div>
  	</div>

</body>
<script src="js/jquery-2.1.4.min.js" />"></script>
<script src="js/materialize.min.js" />"></script>
<script src="js/scripts.js" />"></script>
<script>
	$(document).ready(function() {
	$('.modal').modal();
	$('select').material_select();
	});
	$('.dropdown-button').dropdown({belowOrigin: true});
	$(".button-collapse").sideNav();
</script>
</html>