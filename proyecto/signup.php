
<html>
<head>
<title>Registrarse</title>
<link rel="shortcut icon" href="img/icon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"	rel="stylesheet">
<link href="materialize/css/materialize.min.css" rel="stylesheet">
<link href="css/signup.css" rel="stylesheet">
<link href="css/navbar.css" rel="stylesheet">

</head>

<body>

	<nav class="nav-extended green darken-1">
		<div class="nav-background">
			<div class="ea k imgbg" ></div>
		</div>
		<div class="nav-wrapper db">
			<a href="#" class="brand-logo"><i class="material-icons">camera</i>SRS</a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li><a href="login.php"  class="transparent btn waves-effect waves-light rounded" style="">Iniciar sesión</a></li>
			</ul>
		</div>

		<div class="nav-header de">
          <h1>Crear una cuenta</h1>
          <div class="ge">Accede a los servicios de ésta plataforma de manera gratuita. Sólo registre sus datos en éste formulario.</div>
      </div>
	</nav>


	<div class="container row valign" style="margin-top: 4%;">
		<div class="valign-wrapper col s12 m4 center-align">
			<img class="responsive valign" src="img/monitor.png"
				style="display: block; margin-left: auto; margin-right: auto; width: 65%;">
		</div>

		<div class="col s12 m8">
			<form id="formSignup" class="col s12">
			
				<div class="row">
					<div class="input-field col s12 m6">
						<input id="first_name" type="text" class="validate" name="txtFName"> <label
							for="first_name">Nombre</label>
					</div>
					<div class="input-field col s12 m6">
						<input id="last_name" type="text" class="validate" name="txtLName"> <label 
							for="last_name">Apellido</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="tel" type="tel" class="validate" name="txtPhone" > <label
							for="tel">Teléfono</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="email" class="validate" name="txtEmail" > <label
							for="email">Correo</label>
					</div>
				</div>
				
				<div class="row">
					<div class="input-field col s12">
						<input id="dependencia" type="text" class="validate" name="txtDependencia" > <label
							for="dependencia">Dependencia</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="password" type="password" class="validate" name="txtPass"> <label
							for="password">Contraseña</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="password" type="password" class="validate" name="txtCheckPass"> <label
							for="password">Repetir contraseña</label>
					</div>
				</div>

				<div class="row">
					<div class="col s12">
						<p>
							<input type="checkbox" id="test5" name="chkBox"/> <label for="test5">Acepto 
								las condiciones de uso</label> <!-- falta validar el checkbox-->
						</p>
					</div>
				</div>
				<div class="row">
					<div class="col s12 center-align">
						<button id="btn" class="btn waves-effect waves-light rounded light-green" type="button" name="action" onclick="signup()">Registrarse</button> <!--cambie el tipo de submit a button. En onclick hablo a javascript y mando parametros en la funcion de javascript-->
						
					</div>
				</div>
			</form>
	</div>
	</div>
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/scripts.js"></script>
</body>
</html>
