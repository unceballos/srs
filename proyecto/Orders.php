<?php
	session_start();
	if (empty($_SESSION['txtEmail'])) {
    	header('Location: login.php');
    	die();
    }
?>
<html>
<head>
	<title></title>
	<link rel="shortcut icon" href="img/icon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/navbar.css">
	<link rel="stylesheet" type="text/css" href="css/biblioteca.css">
	<link rel="stylesheet" type="text/css" href="css/radios.css">
</head>

<body class="blue-grey lighten-5" onload="getOrder()">

	<!-- Navbar and Header -->
	<nav class="nav-extended cyan darken-3" style="margin-bottom: 4%">
		<div class="nav-background nabground">
			<div class="ea k"></div>
		</div>
		<div class="nav-wrapper db">
		<!-- LOGO -->
		<a href="#" data-activates="mobile" class="button-collapse"><i class="white-text material-icons">menu</i></a>
			
		<ul class="bt hide-on-med-and-down">
			<li><a class="dropdown-button white-text" href="#!" data-activates="dropdown1"><?php echo $_SESSION['txtEmail'];?><i class="material-icons right">arrow_drop_down</i></a></li>
		</ul>

		<div class="nav-header de">
			<div class="row">
				<div class="col s4 offset-s4 center-align">
					<img src="img/screwdriver.png" style="width: 20%; margin-bottom: -6%">
				</div>
			</div>
			<h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">Tecnico</h3>
			<h1 id="h1IdReporte">ORDEN #564456</h1>
		</div>
	</div>

	<!-- Dropdown Structure -->
	<ul id='dropdown1' class='dropdown-content ddd'>
		<li><a href="controllers/logout.php">Cerrar sesión</a></li>
	</ul>

	<!-- Pestañitas (hide-on-med-and-down)-->
	<div class=" categories-wrapper row cyan darken-4">
		<div class="center-align">
			<ul>
				<li class="col s6 m3 offset-m3 K"><a href="TecnicoOrders.php" class="white-text">REPORTES</a></li>
				<li class="col s6 m3"><a href="TecnicoInventario.php" class="white-text">INVENTARIO</a></li>
			</ul>
		</div>
	</div>
</nav>

	<!-- SIDE NAV -->
	<ul id="mobile" class="side-nav">
		<li>
			<div class="userView">
				<div class="background">
					<img src="img/library.jpeg">
				</div>
				<a href="#!user"><img style="width:25%;" src="img/school.png"></a>
				<a href="#!name"><span class="white-text name">Técnico</span></a>

				<!-- Email of the user in here -->
				<a href="#!email"><span class="white-text email"><?php echo $_SESSION['txtEmail'];?></span></a>
			</div>
		</li>
		<li><a class="subheader">Actividades</a></li>
		<li><a class="waves-effect" href="bibliotecaVer.php"><i class="material-icons">assignment</i>Ver mis reportes</a></li>
		<li><a class="waves-effect" href="bibliotecaHacer.php"><i class="material-icons">mode_edit</i>Hacer un reporte</a></li>
		<li><a class="waves-effect" href="controllers/logout.php"><i class="material-icons">perm_identity</i>Salir de mi cuenta</a></li>
	</ul>

	<!-- CONTENIDO DE LA PÁGINA -->
	<div class="row container">
		<div class="col s12 m12">
			<div class="card horizontal">
				<div class="card-stacked">
					<div class="card-content">
						<form id="showOrder">
						<input type="hidden" name="hdIdOrder" value="<?php echo $_GET['id']?>">
							<div class="row">
								<h5 class="grey-text text-darken-3 center-align" id="asuntoReporte"></h5>
								<p class="grey-text center-align" id="fechaReporte"></p>
								<br>
								<div class="col s12 m12 center-align">
									<img id="imgTipoEquipo" width="200px">
								</div>
								<div class="row">
									<div class="col s12 m8 offset-m2 left-align" style="margin-top: 20px; ">
										<div class="row">
											<div class="input-field col s2">
												<input id="marca" type="text" class="validate" name="txtMarca" disabled="">
												<label for="marca">Marca</label>
											</div>
											<div class="input-field col s3">
												<input id="modelo" type="text" class="validate" name="txtModelo" disabled="">
												<label for="modelo">Modelo</label>
											</div>
											<div class="input-field col s3">
												<input id="serie" type="text" class="validate" name="txtSerie" disabled="">
												<label for="serie">Serie</label>
											</div>
											<div class="input-field col s4">
												<input id="patrimonio" type="text" class="validate" name="txtPatrimonio" disabled="">
												<label for="patrimonio">Patrimonio</label>
											</div>
										</div>
									</div>
								</div>

								<!-- ESTADO -->
								<div class="row" style="margin: 20px">
									<p class="grey-text text-darken-3 left-align"><b>Estado</b></p>
									<div class="col s12">
										<div class="row" style="margin: 20px">
											<input class="with-gap col s2" name="estado" type="radio" id="3s" value="3" />
											<label for="3s">En proceso</label>
											<input class="with-gap col s2" name="estado" type="radio" id="4s" value="4" />
											<label for="4s">Terminado</label>
											<input class="with-gap col s2" name="estado" type="radio" id="5s"  value="5" />
											<label for="5s">Entregado</label>
										</div>
									</div>
								</div>

								<!-- TIPO DE SERVICIO -->
								<div class="row" style="margin: 20px">
									<p class="grey-text text-darken-3 left-align"><b>Tipo de servicio</b></p>
									<div class="col s12">
										<div class="row" style="margin: 20px">
											<input class="with-gap col s2" name="servicio" type="radio" id="1t" value="1"/>
											<label for="1t">Instalación</label>
											<input class="with-gap col s2" name="servicio" type="radio" id="2t" value="2"/>
											<label for="2t">Mantenimiento</label>
											<input class="with-gap col s2" name="servicio" type="radio" id="3t"  value="3"/>
											<label for="3t">Reparación</label>
											<input class="with-gap col s2" name="servicio" type="radio" id="4t" value="4"/>
											<label for="4t">Garantía</label>
											<input class="with-gap col s2" name="servicio" type="radio" id="5t" value="5"/>
											<label for="5t">Asesoría</label>
										</div>
									</div>
								</div>

								<div class="row" style="margin: 20px">
									<p class="grey-text text-darken-3 left-align"><b>Personas y lugares</b></p>
									<div class="col s12">
										<div class="row" style="margin: 20px">
											<!-- SOLICITANTE -->
											<div class="input-field">
												<input id="solicitante" type="text" class="validate" name="txtSolicitante" disabled="">
												<label for="solicitante">Solicitante</label>
											</div>
											
											<!-- Dependencia -->
											<div class="input-field">
												<input id="dependencia" type="text" class="validate" name="txtDependencia" disabled>
												<label for="dependencia">Dependencia</label>
											</div>
																
											<!-- ARREGLADO POR -->
											
											<div class="input-field">
											<select id="reparador" name="txtReparador">
							
											</select>										
											<label for="reparador">Arreglado por</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row" style="margin: 20px">
									<p class="grey-text text-darken-3 left-align"><b>Detalles del reporte</b></p>
									<div class="col s12">
										<div class="row">
											<div class="input-field">
												<i class="material-icons prefix valign-wrapper">error</i>
												<textarea id="trabajoSolicitado" class="materialize-textarea" disabled="true" name="txtTrabajoSolicitado"></textarea>
												<label for="trabajoSolicitado">Trabajo solicitado</label>
											</div>
										</div>
										<div class="row">
											<div class="input-field">
												<i class="material-icons prefix valign-wrapper">done</i>
												<textarea id="trabajoRealizado" class="materialize-textarea" name="txtTrabajoRealizado"></textarea>
												<label for="trabajoRealizado">Trabajo realizado</label>
											</div>
										</div>
										<div class="row">
											<p class="grey-text text-darken-3 left-align"><b>Comentarios</b></p>
											<div class="card-stacked">
												<div class="card-action blue-grey lighten-5" id="contComentarios<?php echo $_GET['id']?>">
												
												</div>

												<ul style="margin-top: 2%" class="pagination center-align" id="pagComments<?php echo $_GET['id']?>"></ul>
												<div class="container">
													<br>
													<div id="reporte<?php echo $_GET['id']?>" class="row">														
										        		<input name="txtComentario" class="col m9" type="text" placeholder="Escriba un comentario y presione el botón" class="grey-text text-darken-2 rounded">
										        			<div class="col m1"></div>
										        		<button class="btn waves-effect waves-light col m2" style="margin-top: 1%" type="button" name="action" onclick="comentarTecnico(<?php echo $_GET['id']?>)">
										        			<i class="material-icons">send</i>
										        		</button>			
										        	</div>
										        </div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-action center-align">
								<a onclick="getOrder()" class="btn-flat black-text tooltipped" data-position="bottom" data-delay="1300" data-tooltip="Descartar los cambios hechos al reporte"><i class="material-icons right">delete</i>Descartar</a>
								<button type="button" onclick="updateReporte()" class="blue darken-1 waves-effect waves-light btn  tooltipped" data-position="bottom" data-delay="1300" data-tooltip="Modificar el reporte"><i class="material-icons right">done</i>Modificar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="js/jquery-2.1.4.min.js" />"></script>
<script src="js/materialize.min.js" />"></script>
<script src="js/scripts.js"></script>
<script>
	$(document).ready(function() {
	$('.modal').modal();
	$('select').material_select();
	});
	$('.dropdown-button').dropdown({belowOrigin: true});
	$(".button-collapse").sideNav();
</script>
</html>