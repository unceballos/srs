<?php
	session_start();
	if (empty($_SESSION['txtEmail'])) {
    	header('Location: login.php');
    	die();
    }
    if($_SESSION['txtRol']!=2){
		header('Location: orders.php');
    	die();
    }
    $email=$_SESSION['txtEmail'];
    $patrimonio=$_GET['patrimonio'];
?>
<html>
<head>
	<title>MX-3012394</title>
	<link rel="shortcut icon" href="img/icon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/navbar.css">
	<link rel="stylesheet" type="text/css" href="css/biblioteca.css">
	<link rel="stylesheet" type="text/css" href="css/radios.css">
</head>
<body class="blue-grey lighten-5" onload="getEquipo()">

	<!-- Navbar and Header -->
	<nav class="nav-extended cyan darken-3" style="margin-bottom: 4%">
		<div class="nav-background nabground">
			<div class="ea k"></div>
		</div>
		<div class="nav-wrapper db">
			<!-- LOGO -->
			<a href="#" data-activates="mobile" class="button-collapse">
			<i class="white-text material-icons">menu</i></a>
			<ul class="bt hide-on-med-and-down">
				<li><a class="dropdown-button white-text" href="#!" data-activates="dropdown1"><?php echo $email?><i class="material-icons right">arrow_drop_down</i></a></li>
			</ul>

			<div class="nav-header de">
				<div class="row">
					<div class="col s4 offset-s4 center-align">
						<img src="img/screwdriver.png" style="width: 20%; margin-bottom: -6%">
					</div>
				</div>
				<h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">Tecnico</h3>
				<h1>Patrimonio # <?php echo $patrimonio ?></h1>
			</div>
		</div>

		<!-- Dropdown Structure -->
		<ul id='dropdown1' class='dropdown-content ddd'>
			<li><a href="controllers/logout.php">Cerrar sesión</a></li>
		</ul>

		<!-- Pestañitas (hide-on-med-and-down)-->
		<div class=" categories-wrapper row cyan darken-4">
			<!--categories-db col s4 offset-s4center-align">-->
			<div class="center-align">
				<ul>
					<li class="col s4 m2 offset-m3"><a href="TecnicoOrders.php" class="white-text">REPORTES</a></li>
					<li class="col s4 m2"><a href="bibliotecaHacer.php" class="white-text">HACER REPORTE</a></li>
					<li class="col s4 m2 K"><a href="TecnicoInventario.php" class="white-text">INVENTARIO</a></li>

				</ul>
			</div>
		</div>
	</nav>

	<!-- SIDE NAV -->
	<ul id="mobile" class="side-nav">
		<li><div class="userView">
			<div class="background">
				<img src="img/library.jpeg">
			</div>

			<a href="#!user"><img style="width:25%;"
				src="img/school.png"></a>

				<a href="#!name"><span class="white-text name">Técnico</span></a>

				<!-- Email of the user in here -->
				<a href="#!email"><span class="white-text email"><?php echo $email?></span></a>
			</div></li>

			<li><a class="subheader">Actividades</a></li>
			<li><a  class="waves-effect"href="bibliotecaVer.php" ><i
				class="material-icons">assignment</i>Ver mis reportes</a></li>
				<li><a class="waves-effect" href="bibliotecaHacer.php" ><i
					class="material-icons">mode_edit</i>Hacer un reporte</a></li>
					<li><a class="waves-effect" href="controllers/logout.php"> <i
						class="material-icons">perm_identity</i>Salir de mi cuenta</a></li>
					</ul>

					<!-- CONTENIDO DE LA PÁGINA -->
					<div class="row container">
						<div class="col s12 m12">

							<!-- CONTENT OF THE PAGE -->

							<!-- Comienzo del formulario -->
							<form id="showEquipo">
								<input type="hidden" name="oldPatrimonio" id="oldPatrimonio" value="<?php echo $patrimonio;?>">
								<input type="hidden" name="hdEncargado" id="idEncargado" value="">

							<div class="card horizontal">
								<div class="card-stacked">
									<div class="card-content">
										<p class="grey-text center-align">Reportes encontrados de este equipo: <b id="contador"></b></p><br>
										<div class="row"><div class="col s12 m12 center-align">
											<img width="200px" id="imgTipoEquipo">
										</div></div>

											<ul class="collapsible" data-collapsible="accordion">
												<li>
													<div class="collapsible-header center-align">Mostrar detalles del equipo</div>
													<div class="collapsible-body"> 
													<div class="row">
														<br>

		
		<div class="row">
			<div class="col s12 m8 offset-m2 left-align" style="margin-top: 20px; ">
				<div class="row">

					<div class="input-field col s3">
						<input id="patrimonio" type="text" name="txtPatrimonio" class="counter" data-length="45" maxlength="45">
						<label for="patrimonio">Patrimonio</label>
					</div>

					<div class="input-field col s3">
						<input id="folio" type="text" name="txtFolio" class="counter" data-length="3" maxlength="3">
						<label for="folio">Folio</label>
					</div>

					<div class="input-field col s3">
						<input id="factura" type="text" name="txtFactura" class="counter" data-length="10" maxlength="10">
						<label for="factura">Factura</label>
					</div>

					<div class="input-field col s3">
						<input id="programaFederal" type="text" name="txtProgramaFederal" class="counter" data-length="15" maxlength="15">
						<label for="programaFederal">Programa federal</label>
					</div>
					
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col s12 m8 offset-m2 left-align" style="margin-top: 20px; ">
				<div class="row">

					<div class="input-field col s3">
						<input id="marca" type="text" name="txtMarca" class="counter" data-length="45" maxlength="45">
						<label for="marca">Marca</label>
					</div>

					<div class="input-field col s3">
						<input id="modelo" type="text" name="txtModelo" class="counter" data-length="45" maxlength="45">
						<label for="modelo">Modelo</label>
					</div>

					<div class="input-field col s3">
						<input id="serie" type="text" name="txtSerie" class="counter" data-length="60" maxlength="60">
						<label for="serie">Serie</label>
					</div>

					<div class="input-field col s3">
						<input id="proveedor" type="text" name="txtProveedor" class="counter autocomplete" data-length="100" maxlength="100">
						<label for="proveedor">Proveedor</label>
					</div>
					
				</div>

			</div>
		</div>

		<div class="row">
			<div class="col s12 m8 offset-m2 left-align" style="margin-top: 20px; ">
				<div class="row">
					<div class="input-field col s3">
						<input id="so" type="text" name="txtSO" class="counter" data-length="45" maxlength="45">
						<label for="so">S.O.</label>
					</div>
					<div class="input-field col s3">
						<input id="office" type="text" name="txtOffice" class="counter" data-length="20" maxlength="20">
						<label for="office">Office</label>
					</div>
					
					<div class="input-field col s4">
						<input id="nombreUsuario" type="text" name="nombreUsuario" class="counter autocomplete" data-length="100" maxlength="100">
						<label for="nombreUsuario">Nombre del encargado</label>						
					</div>

					<div class="input-field col s2">
						<input id="numTrabajador" type="text" name="txtNumTrabajador" class="counter" data-length="4" maxlength="4">
						<label for="numTrabajador">N° de trabajador</label>						
					</div>

					<div class="input-field col s12">
						<select name="dependencia" id="selectDependencia"></select>
			    		<label>Dependencia</label>
			  		</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12 m8 offset-m2 left-align" style="margin-top: 20px; ">
				<div class="row">
					<div class="input-field col s12">
						<textarea id="description" class="materialize-textarea" name="txtDescription"></textarea>
          				<label for="description">Descripción / Ubicación</label>
					</div>					
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12 center-align" style="margin-top: 20px;">
				<div class="col s2 offset-s1">
									<a href="#modalBorrarEQU" class="btn-flat black-text tooltipped" data-position="bottom" data-delay="1300" data-tooltip="Borrar equipo"><i class="material-icons">delete</i></a>
								</div>
								<div class="col s7" style="margin-left: 6%">
									<button onclick="window.location='TecnicoInventario.php';" type="button" class="btn-flat black-text tooltipped" data-position="bottom" data-delay="1300" data-tooltip="Descartar los cambios hechos y restablecer los datos de este equipo a su estado original"><i class="material-icons right">clear_all</i>Descartar</button>
									<button onclick="updateEquipo()" type="button" class="blue darken-1 waves-effect waves-light btn  tooltipped" data-position="bottom" data-delay="1300" data-tooltip="Enviar los cambios hechos a éste equipo"><i class="material-icons right">done</i>Modificar</button>
								</div>
							</div>
			
						</div>
					</form>


				</div>
</div>
</li>
</ul>

	</div>	

</div></div> 


		<!-- COMIENZO DEL REPORTE 1-->
		<div id="showRepots">
			
		</div>
		<br>
		<ul class="pagination center-align">
			
		</ul>
		<!-- FIN DEL REPORTE -->

		<!-- Modal Structure -->
  	<div id="modalBorrarEQU" class="modal">
    	<div class="modal-content">
      		<h4>Dar de baja un equipo</h4>
      		<p>Haca clic en el botón de 'Sí' para confirmar, o en 'Cancelar' para anular la operación. <sub> (Es obligatorio especificar la razón por la que el equipo será dado de baja)</sub></p>

      		<div class="row">
      			<div class="col s8 offset-s2 center-align">
      				<img src="img/barrier.png" width="140px">
      				<p>¿Por qué razón desea dar de baja el equipo?</p>
      				<textarea class="materialize-textarea validate" id="razon"></textarea>
      				<label for="razon">Razon</label>
      			</div>
      		</div>
    	</div>
    	<div class="modal-footer">
    		<a onclick="deleteEquipo()" style="margin-right:15px; margin-right:15px;" class="modal-action blue waves-effect waves-green btn-flat white-text">Dar de baja</a>
    		<a style="margin-right:15px; margin-right:15px;" class="modal-action modal-close waves-effect waves-red btn-flat cancelar">Cancelar</a>
    	</div>
  	</div>


															</div>
														</div>

														</body>
	<script src="js/jquery-2.1.4.min.js" />"></script>
	<script src="js/materialize.min.js" />"></script>
	<script src="js/scripts.js" />"></script>
	<script>
		$(document).ready(function() {
			$('.modal').modal();
			$('select').material_select();
			$('#numTrabajador').on('keyup', function(e){
				var code = e.keyCode || e.which;
				if (code != '9') {
					if($(this).val()!=""){
						getEncargado($(this).val());
					}
				}
				
			});
			$('#nombreUsuario').on('change', function(){
				if($(this).val()==""){
					$('#numTrabajador').val("");
					Materialize.updateTextFields();
				}

			});

			$('#numTrabajador').on('change', function(){
				if($(this).val()==""){
					$('#nombreUsuario').val("");
					Materialize.updateTextFields();
				}

			});

			$("#numTrabajador").keypress(function (e) { //Solo numeros
				if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
					return false;
				}
			});
		});

		
		$('.dropdown-button').dropdown({belowOrigin: true});
		$(".button-collapse").sideNav();

		$( "a.cancelar" ).on( "click", function() {
  			$('#razon').val("");
  			$("#razon").removeClass("materialize-textarea validate valid");
  			$('#razon').addClass("materialize-textarea validate");
  			$("label[for='razon']").removeAttr('class');
		});		
	</script>
</html>
