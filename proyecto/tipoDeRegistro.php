<html>
<head>
<title>Registrarse</title>
<link rel="shortcut icon" href="img/icon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"	rel="stylesheet">
<link href="materialize/css/materialize.min.css" rel="stylesheet">
<link href="css/signup.css" rel="stylesheet">
<link href="css/navbar.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/tipoDeRegistro.css">

</head>
<body class="blue-grey lighten-5">

    <!-- NAV -->
    <nav class="transparent z-depth-0">
        <div class="nav-wrapper db   ">
            <a href="./" class="brand-logo teal-text ">
                <i class="material-icons">camera</i>SRS</a>

            <a href="/" data-activates="mobile" class="button-collapse grey-text">
                <i class="material-icons">menu</i>
            </a>
            <ul class="right hide-on-med-and-down">
                        <a href="login.php" class="transparent waves-effect teal-text hovery">Iniciar sesión</a>
            </ul>
        </div>
    </nav>

	<div class="de">
          <h4 class="grey-text text-darken-2" style="margin:1%;">Elija su tipo de cuenta</h4>
          <div class="grey-text text-darken-2 ge">Para acceder a la plataforma necesita una cuenta, pero antes de proceder a introducir sus datos, ¿qué tipo de cuenta necesita?.</div>
      </div>

	<!-- Contenido de la pagina -->
	<br><br>
	<div class="container row">
		<div class="col m12 row center-align">
			<div class="col s12 m5 white" style="padding: 30px">
				<!-- TECNICO --><a href="signupTecnico.php">
					<label class="hoverEffect">
				<input type="radio" name="equipo" value="impresora" id="tecnico" />
					<img style="width:70%" src="img/screwdriver.png">
					<p class="center-align" style="font-size: 130%">Si desea usar esta plataforma para atender reportes y trabajar como técnico, ésta es su opción.</p>
				</label>
				</a>
				
				<a href="signupTecnico.php" class="btn">Técnico</a>
			</div>

			<div class="col m2"></div>
		            
			<div class="col s12 m5 white " style="padding: 30px">
				<!-- DIRECTIVO -->
		        <label class="hoverEffect">
		            <input type="radio" name="equipo" value="impresora" id="directivo" />
		            <img src="img/school.png" style="width:70%">
		            <p class="center-align" style="font-size: 130%">Si usted está encargado de una biblioteca regístrese aquí para mandar sus reportes de manera electrónica.</p>
		        </label>
		        <a href="signupDirectivo.php" class="btn">Directivo de biblioteca</a>
			</div>       
      </div>
	</div>

	</div>
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/scripts.js"></script>
	<script src="js/onclicEvents.js"></script>
</body>
</html>
