function getAlldata(page){ 
	
	$.ajax({
		url:"controllers/getAllReports.php",
		method:"POST",
		data:{page,page},
		success: function(data){
			$('#showRepots').html(data);
		}
	});
}

function getReportsBibliotecario(page, idBibliotecario){ 
	
	$.ajax({
		url:"controllers/getAllReports.php",
		method:"POST",
		data:{page:page, idBibliotecario:idBibliotecario},
		success: function(data){
			$('#showRepots').html(data);
		}
	});
}

function clickTecnico(par){
	var page=$(par).attr("id");
	getAlldata(page);
}

function clickBibliotecario(par, id){
	var page=$(par).attr("id");
	getReportsBibliotecario(page, id);
}