function login(){

	var $form = $( '#formLogin' );
	var email=$form.find( "input[name='txtEmail']" ).val();
	var pass=$form.find( "input[name='txtPass']" ).val();

	$.post('controllers/login.php', {txtEmail:email, txtPass:pass},
	function(data){
		if(data=="success"){
			window.location = "Orders.html";
		}else{
			Materialize.toast(data, 4000,'rounded');
		}
	});
}

function signup(){
	var $form = $( '#formSignup' );
	var fname=$form.find( "input[name='txtFName']" ).val();
	var lname=$form.find( "input[name='txtLName']" ).val();
	var phone=$form.find( "input[name='txtPhone']" ).val();
	var email=$form.find( "input[name='txtEmail']" ).val();
	var pass=$form.find( "input[name='txtPass']" ).val();
	var checkPass=$form.find( "input[name='txtCheckPass']" ).val();

	$.post('controllers/register.php', {txtFName:fname, txtLName:lname, txtPhone:phone, txtEmail:email, txtPass:pass, txtCheckPass:checkPass},
	function(data){
		if(data=="Usuario registrado. Espere autorización del administrador."){
			Materialize.toast(data, 4000);
			setTimeout(function () {
       			window.location = "login.html";
    		}, 4500);
				
		}else{
			Materialize.toast(data, 4000);
		}
	});
}