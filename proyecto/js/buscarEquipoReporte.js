function buscarEquipoReporte(){

   
    var idPatrimonio=$('#patrimonio').val();
    $("#serie").val("");
    $("#marca").val("");
    $("#modelo").val("");
 
    $.post( 'controllers/buscarEquipo.php', {idPatrimonio:idPatrimonio},
    function(data) {
        if(data=="null"||data=="noData"){
                
            $("#serie").val("");
            $("#marca").val("");
            $("#modelo").val("");
            for(var i=1;i<=9;i++){
                $("#"+i).removeAttr('checked');
            }
            
            
            Materialize.updateTextFields();

        }else{

            var obj = jQuery.parseJSON(data);
            $.each(obj, function(i, item) {
                if (i=="tipoEquipo") {

                    $("#"+item).prop('checked', 'checked');
                }else{
                    $("#"+i).val(item);  
                }
                         
            });
            Materialize.updateTextFields();
        }
    });
}
