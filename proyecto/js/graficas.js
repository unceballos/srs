/*
**  Número random para los colores de background.
*/
function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min) ) + min;
}

/*
**  Se crean los charts para que no se vea vacio.
*/
function cargando(){
    var canvasTotal = $('#grafTotal');
    var canvasStatus = $('#grafStatus');
    var canvasEquipos = $('#grafEquipos');
    var canvasBibliotecas = $('#grafBibliotecas');

    var grafTotal = new Chart(canvasTotal, {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: 'Reportes',
            data: [],
            backgroundColor: [
                'rgba(54, 162, 235, 0.5)',
            ],
            pointRadius:6,
            pointHitRadius:10
            }]
        },
    });

    var grafStatus = new Chart(canvasStatus, {
        type: 'doughnut',
        data: {
            labels: ['Status'],
            datasets: [{
                data: [1],
                backgroundColor: [
                    'rgba(255, 125, 0, 0.5)'
                ],
            }]
        },
    });


    var grafEquipos = new Chart(canvasEquipos, {
        type: 'doughnut',
        data: {
            labels: ['Equipos'],
            datasets: [{
                data: [1],
                backgroundColor: [
                    'rgba(255, 125, 0, 0.5)'
                ],
            }]
        },
    }); 


    var grafBibliotecas = new Chart(canvasBibliotecas, {
    type: 'horizontalBar',
    data: {
        labels: ['Bibliotecas'],
            datasets: [{
                label: "Reportes",
                data: [1],
                backgroundColor: ['rgba(54, 162, 235, 0.2)'],
                borderColor: ['rgba(54, 162, 235, 1)'],
                borderWidth: 1
            }]
        },
    });
}
/*
**  Elimina las graficas anteriores al pedir nuevas estadisticas.
*/
function resetAllCanvas(){
    $('#grafTotal').remove();
    $('#grafTotalContainer').prepend('<canvas id="grafTotal" width="400" height="150"></canvas>');

    $('#grafStatus').remove();
    $('#grafStatusContainer').append('<canvas id="grafStatus" width="200" height="150"></canvas>');
    

    $('#grafEquipos').remove();
    $('#grafEquiposContainer').append('<canvas id="grafEquipos" width="200" height="150"></canvas>');
    

    $('#grafBibliotecas').remove();
    $('#grafBibliotecasContainer').append('<canvas id="grafBibliotecas" width="400" height="500"></canvas>');
    

}

/*
**  Obtiene las estadisticas para mostrarlas posteriormente
*/
function obtenerDatos(){
    

    var $form = $('#rangoFechas');
    var fechaInicio=$form.find("input[id='fechaInicio']").val();
    var fechaFin=$form.find("input[id='fechaFin']").val();
    
    var meses = new Array();
    var total = new Array();
    var totales_status = new Array();
    var status = new Array();
    var equipos = new Array();
    var totales_equipos = new Array();
    var dependencias = new Array();
    var datosDependencias = new Array();

    $.post('controllers/getEstadisticas.php', {fechaInicio:fechaInicio, fechaFin:fechaFin},
    function(data){
        if(data=="dateNull"){
            Materialize.toast("Por favor, seleccione rango de fechas.",4000,'rounded');
        }else{
            resetAllCanvas();
            var obj = jQuery.parseJSON(data);
            var cantidadObjetosJSON = Object.keys(obj).length;
            $.each(obj, function(i, item) {
                $.each(obj[i], function(j, item2){
                    if(item2.total_reportes){
                        $('#total_reportes').html("("+item2.total_reportes+" en total)");
                    }
                    if(item2.aMonth){
                        meses.push(item2.aMonth);
                    }
                    if(item2.total){
                        total.push(item2.total);
                    }
                    if(item2.status){
                        status.push(item2.status);
                    }
                    if(item2.totales_reportes_status){
                        totales_status.push(item2.totales_reportes_status);
                    }
                    if(item2.equipo){
                        equipos.push(item2.equipo);
                    }
                    if(item2.totales_reportes_equipos){
                        totales_equipos.push(item2.totales_reportes_equipos);
                    }
                    if(item2.nombre_dependencia){
                        dependencias.push(item2.nombre_dependencia);
                    }
                    if(item2.totales_reportes_dependencias){
                        datosDependencias.push(item2.totales_reportes_dependencias);
                    }
                });                   
                        
                                    
            });
            graficaPorMes(meses,total);
            graficaPorStatus(status,totales_status);
            graficaPorEquipos(equipos,totales_equipos);  
            graficaPorDependencia(dependencias,datosDependencias); 

        }
         
    });
}


//Grafica del total
function graficaPorMes(meses,total){
    var canvasTotal = $('#grafTotal');
    var grafTotal = new Chart(canvasTotal, {
    type: 'line',
    data: {
        labels: meses,
        datasets: [{
            label: 'Reportes',
            data: total,
            backgroundColor: [
                'rgba(54, 162, 235, 0.5)',
            ],
            pointRadius:6,
            pointHitRadius:10
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
   
}

 

//Grafica del status
function graficaPorStatus(status,totales_status){
    var canvasStatus = $('#grafStatus');
    var grafStatus = new Chart(canvasStatus, {
        type: 'doughnut',
        data: {
            labels: status,
            datasets: [{
                data: totales_status,
                backgroundColor: [
                    'rgba(186, 255, 0, 0.5)',
                    'rgba(255, 125, 0, 0.5)',
                    'rgba(0, 206, 255, 0.5)'
                ],
            }]
        },
    });
}

//Grafica de los equipos
function graficaPorEquipos (equipo,totales_equipos){
    
    var background=new Array();
    var colores = [
        'rgb(100, 127, 191, 0.5)',
        'rgba(63, 63, 191, 0.51)',
        'rgba(186, 255, 0, 0.5)',
        'rgba(191, 63, 63, 0.51)',
        'rgba(127, 63, 191, 0.51)',
        'rgba(63, 191, 191, 0.51)',
        'rgba(30, 188, 109, 0.51)',
        'rgba(188, 109, 30, 0.51)',
        'rgba(79, 170, 231, 0.51)'
        ];
    

    for(var i=0; i < equipo.length; i++){
        background.push(colores[i]);
    }

    var canvasEquipos = $('#grafEquipos');
    var grafEquipos = new Chart(canvasEquipos, {
        type: 'doughnut',
        data: {
            labels: equipo,
            datasets: [{
                data: totales_equipos,
                backgroundColor: background,
            }]
        },
    });  
}

//Grafica de las bibliotecas
function graficaPorDependencia(dependencias,datosDependencias) {
    var bgcolor=[
        'rgba(255, 99, 132, 0.2)',
        'rgba(54, 162, 235, 0.2)',
        'rgba(255, 206, 86, 0.2)',
        'rgba(75, 192, 192, 0.2)',
        'rgba(153, 102, 255, 0.2)',
        'rgba(255, 159, 64, 0.2)'
        ];
    var bordes=[
        'rgba(255,99,132,1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)',
        'rgba(75, 192, 192, 1)',
        'rgba(153, 102, 255, 1)',
        'rgba(255, 159, 64, 1)'
        ];
    
    var background = new Array();
    var border = new Array();
    
    for (var i = 0; i < dependencias.length; i++) {
        var random=getRndInteger(1,6);
        background.push(bgcolor[random]);
        border.push(bordes[random]);
    }

    var canvasBibliotecas = $('#grafBibliotecas');
    var grafBibliotecas = new Chart(canvasBibliotecas, {
    type: 'horizontalBar',
    data: {
        labels: dependencias,
            datasets: [{
                label: "Reportes",
                data: datosDependencias,
                backgroundColor: background,
                borderColor: border,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
}

