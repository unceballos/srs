// --- Funciones generales ---
function pagination(json) {
  var pages = "";
  var cantidadObjetosJSON = Object.keys(json).length;
  $.each(json, function (i, item) {
    if (i == cantidadObjetosJSON - 1) {
      if (item.totalPaginas != 0) {
        if (item.paginaAcual == 1) {
          pages += '<li class="disabled"><a><i class="material-icons">chevron_left</i></a></li>';
        } else {
          pages += '<li class="waves-effect"><a class="pages" value="' + (parseInt(item.paginaAcual) - 1) + '"><i class="material-icons">chevron_left</i></a></li>';
        }
        for (var cont = Math.max(1, (parseInt(item.paginaAcual) - 3)); cont <= Math.min(item.totalPaginas, parseInt(item.paginaAcual) + 3); cont++) {
          if (cont == item.paginaAcual) {
            pages += '<li class="active"><a>' + cont + '</a></li>';
          } else {
            pages += '<li class="waves-effect"><a class="pages" value="' + cont + '">' + cont + '</a></li>';
          }
        }
        if (item.paginaAcual == item.totalPaginas) {
          pages += '<li class="disabled"><a><i class="material-icons">chevron_right</i></a></li>';
        } else {
          var siguiente = parseInt(item.paginaAcual) + 1;
          pages += '<li class="waves-effect"><a class="pages" value="' + siguiente + '"><i class="material-icons">chevron_right</i></a></li>';
        }
      }
    }
  });
  return pages;
}

function fillReports(json) {
  var html = "";
  var cantidadObjetosJSON = Object.keys(json).length;
  if (cantidadObjetosJSON == 0) { //Verifica si existe algún reporte por la busqueda por asunto de reporte. Si existe hace el llenado, si no, muestra el mensaje que no existe.
    html += "<br><br><br><br><div class=\"center-align grey-text text-darken-1\">"
    html += "<img src=\"img/empty.png\">"
    html += "<h3 class=\"center-align\">No se encontró ningún reporte</h3>"
    html += "<p class=\"center-align\">Parece ser que la lista de reportes está vacía, lo que puede significar que aún no se ha creado ningún reporte (o tiene aplicado un filtro que ningún reporte cumplió).</p>"
    html += "</div>"
  }
  else {
    $.each(json, function (i, item) {
      if (item.totalPaginas == 0) {//Memsaje que no existen reportes.
        html += "<br><br><br><br><div class=\"center-align grey-text text-darken-1\">"
        html += "<img src=\"img/empty.png\">"
        html += "<h3 class=\"center-align\">No se encontró ningún reporte</h3>"
        html += "<p class=\"center-align\">Parece ser que la lista de reportes está vacía, lo que puede significar que aún no se ha creado ningún reporte (o tiene aplicado un filtro que ningún reporte cumplió).</p>"
        html += "</div>"
      }

      if (item.totalPaginas == null) {//Inicia llenado de reportes.
        html += "<div class=\"card horizontal\">";
        html += "<div class=\"card-stacked\"><div class=\"card-content\"><div class=\"row\"><div class=\"col s12 m6\">";
        html += "<p class=\"grey-text\">&nbsp;&nbsp;&nbsp;&nbsp;Reportado por: <img class=\"\" src=\"img/" + item.id_tipo_equipo + ".png\" ";
        html += "style=\"width: 10%;\" align=\"left\" style=\"margin-right: 6%\">" + item.nombre_solicitante + " " + item.apellido_solicitante + "</p>";
        html += "<p class=\"grey-text\"> &nbsp;&nbsp;&nbsp;&nbsp;" + item.fecha_solicitud + "</p></div>";
        html += "<div class=\"col s12 m6 right-align\">";
        html += "<img class=\"imgStatus\"src=\"img/st" + item.id_status + ".png\" style=\"padding-right: 5%;\">";
        html += "<span class=\"eraseDiv\"><button onclick=\"redirectReporte(this)\" value=\"" + item.id_reporte + "\" type=\"button\" class=\"waves-effect btn-floating btn-flat tooltipped ";
        html += "center-align\" data-position=\"bottom\" data-delay=\"50\" data-tooltip=\"Abrir reporte\" style=\" margin-bottom: 9%\"><i class=\"grey-text ";
        html += "material-icons middle\">view_headline</i></button></span>";
        html += "</div></div></div><div class=\"card-action\" style=\"margin-top: -8%\"><div class=\"row col s12 m12\">";
        html += "<!-- TÍTULO DEL PROBLEMA --><h5 class=\"grey-text text-darken-3\">" + item.asunto_reporte + "</h5>";
        html += "<!-- MARCA / MODELO DEL EQUIPO --><p class=\"grey-text\"><b>Equipo: </b>" + item.tipo_equipo + " " + item.marca_equipo + " " + item.modelo_equipo + "<br>";
        if (item.nombre_reparador == null) {
          html += "<b>Atendido por: </b>Sin Asignar</p></div><div class=\"row col s12 m12\" style=\"margin-top: -4%\">";
        } else {
          html += "<b>Atendido por: </b>" + item.nombre_reparador + " " + item.apellido_reparador + "</p></div><div class=\"row col s12 m12\" style=\"margin-top: -4%\">";
        }

        html += "<!--TRABAJO SOLICITADO--><p class=\"grey-text text-darken-2\"><b>Trabajo solicitado: </b>" + item.trabajos_solicitado + "</p>";
        if (item.trabajo_realizado == null) {
          html += "<!--TRABAJO REALIZADO --><p class=\"grey-text text-darken-2\"> <b>Trabajo realizado: </b>Aún se está trabajando en el reporte.</p>";
        } else {
          html += "<!--TRABAJO REALIZADO --><p class=\"grey-text text-darken-2\"> <b>Trabajo realizado: </b>" + item.trabajo_realizado + "</p>";
        }
        html += "</div></div>";

        html += "<ul class=\"collapsible popout\" data-collapsible=\"accordion\"><li>";
        html += "<div class=\"collapsible-header\"><i class=\"material-icons\">comment</i>Comentarios</div>"; //comentarios con collapsible
        html += "<div class=\"collapsible-body blue-grey lighten-5\"><div id=\"contComentarios" + item.id_reporte + "\"></div>";


        html += "<ul style=\"margin-top: 2%\" class=\"pagination center-align\" id=\"pagComments" + item.id_reporte + "\"></ul><div class=\"container\"><br><div id=\"reporte" + item.id_reporte + "\" class=\"row formMsgs\">";
        html += "<input name=\"txtComentario\" class=\"col m9\" type=\"text\" placeholder=\"Escriba un comentario y presione el botón\" autocomplete=\"off\"   ";
        html += " class=\"grey-text text-darken-2 rounded\"><div class=\"col m1\"></div>";
        html += "<button class=\"btn waves-effect waves-light col m2\" style=\"margin-top: 1%\" type=\"button\" name=\"action\" onclick=\"comentarTecnico(" + item.id_reporte + ")\">";
        html += "<i class=\"material-icons\">send</i></button></div></div>";
        html += "</div></li></ul>";
        html += "</div></div>";
        html += "</div></div>";
        getComentarios(item.id_reporte, 1);
      }
    });
  }
  return html;
}

// --- FIN Funciones generales. ---

// --- INICIO login.php ---

function login() {

  var $form = $('#formLogin');
  var email = $form.find("input[name='txtEmail']").val();
  var pass = $form.find("input[name='txtPass']").val();

  $.ajax({
    url: 'controllers/login.php',
    method: 'POST',
    data: {
      txtEmail: email, txtPass: pass
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "3" || data == "5") {
        window.location = "bibliotecaVer.php";
      } else if (data == "2") {
        window.location = "tecnicoOrders.php";
      } else if (data == "1") {
        window.location = "administradorBiblioteca.php";
      } else if (data == "4") {
        window.location = "directivoVer.php";
      } else {
        Materialize.toast(data, 4000, 'rounded');
      }
    }
  });
}

// --- FIN login.php ---

// --- INICIO signupDirectivo.php ---

function llenarBibliotecas() {
  /*
  ** Llena todos los campos donde se tenga que seleccionar una biblioteca.
  */

  return $.ajax({
    url: 'controllers/getBibliotecas.php',
    method: 'POST',
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var obj = jQuery.parseJSON(data);
      var groups = Object.create(null);

      for (var i = 0; i < obj.length; i++) { //Empieza agrupacion de delegaciones para agrupar las dependencias.
        var item = obj[i];
        if (!groups[item.delegacion]) {
          groups[item.delegacion] = [];
        }
        groups[item.delegacion].push({ //Se agregan a cada delegacion las dependencias y sus indicadores que le corresponda.
          dependencia: item.dependencia,
          idDependencia: item.idDependencia
        });
      }

      var result = [];

      for (var x in groups) {
        var obj2 = {};
        obj2[x] = groups[x];
        result.push(obj2);
      }

      html = "";
      for (var i = 0; i < result.length; i++) { // Procesar el array creado para agregarlo a la vista.

        $.each(result[i], function (index, item) {
          html += '<optgroup label="' + index + '">';
          $.each(item, function (index2, item2) {
            html += '<option value="' + item2.idDependencia + '">' + item2.idDependencia + ' - ' + item2.dependencia + '</option>';
          });
          html += '</optgroup>';
        });
      }
      $('#selectDependencia').html(html);
      $('select').material_select();
    }
  });

}

function signupDirectivo() {
  var $form = $('#formSignup');
  var fname = $form.find("input[name='txtFName']").val();
  var lname = $form.find("input[name='txtLName']").val();
  var phone = $form.find("input[name='txtPhone']").val();
  var email = $form.find("input[name='txtEmail']").val();
  var pass = $form.find("input[name='txtPass']").val();
  var checkPass = $form.find("input[name='txtCheckPass']").val();
  var numTrabajador = $form.find("input[name='txtNumTrabajador']").val();
  var dependencia = $form.find("select[name='dependencia']").val();

  $.ajax({
    url: 'controllers/registerDirectivo.php',
    method: 'POST',
    data: {
      fname: fname, lname: lname, phone: phone, email: email,
      pass: pass, checkPass: checkPass, dependencia: dependencia, numTrabajador: numTrabajador
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "ok") {
        Materialize.toast('Directivo registrado. Espere autorización del administrador.', 4000, 'rounded');
        setTimeout(function () {
          window.location = "login.php";
        }, 4500);

      } else {
        Materialize.toast(data, 4000, 'rounded');
      }
    }
  });
}

// --- FIN signupDirectivo.php ---

// --- INICIO signupTecnico.php ---

function signupTecnico() {
  var $form = $('#formSignup');
  var fname = $form.find("input[name='txtFName']").val();
  var lname = $form.find("input[name='txtLName']").val();
  var phone = $form.find("input[name='txtPhone']").val();
  var email = $form.find("input[name='txtEmail']").val();
  var pass = $form.find("input[name='txtPass']").val();
  var numTrabajador = $form.find("input[name='txtNumTrabajador']").val();
  var checkPass = $form.find("input[name='txtCheckPass']").val();

  $.ajax({
    url: 'controllers/registerTecnico.php',
    method: 'POST',
    data: {
      fname: fname, lname: lname, phone: phone, email: email,
      pass: pass, checkPass: checkPass, numTrabajador: numTrabajador
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "ok") {
        Materialize.toast('Técnico registrado. Espere autorización del administrador.', 4000, 'rounded');
        setTimeout(function () {
          window.location = "login.php";
        }, 4500);

      } else {
        Materialize.toast(data, 4000, 'rounded');
      }
    }
  });

  /*$.post('controllers/registerTecnico.php', {
    fname: fname, lname: lname, phone: phone, email: email,
    pass: pass, checkPass: checkPass, checkBox: checkBox
  },
    function (data) {
      
    });*/
}

// --- FIN signupTecnico.php ---

// --- INICIO administradorBiblioteca.php ---

function getResultBiblio(page) {
  /*
  **Obtiene todos los bibliotecarios registrados en el sistema. Los muestra en una tabla.
  */
  llenarBibliotecas();
  $.ajax({
    url: 'controllers/getUserBiblio.php',
    method: 'POST',
    data: {
      page: page
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var html = "";
      var pages = "";
      var obj = jQuery.parseJSON(data);
      var cantidadObjetosJSON = Object.keys(obj).length;
      $.each(obj, function (i, item) {
        if (i == cantidadObjetosJSON - 1) {
          if (item.totalPaginas != 0) {
            if (item.paginaAcual == 1) {
              pages += "<li class=\"disabled\"><a><i class=\"material-icons\">chevron_left</i></a></li>";

            } else {
              var anterior = parseInt(item.paginaAcual) - 1;
              pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + anterior + "\"><i class=\"material-icons\">chevron_left</i></a></li>";
            }
            for (var cont = Math.max(1, (parseInt(item.paginaAcual) - 3)); cont <= Math.min(item.totalPaginas, parseInt(item.paginaAcual) + 3); cont++) {
              if (cont == item.paginaAcual) {
                pages += "<li class=\"active\"><a>" + cont + "</a></li>";
              } else {
                pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + cont + "\">" + cont + "</a></li>";
              }
            }

            if (item.paginaAcual == item.totalPaginas) {
              pages += "<li class=\"disabled\"><a><i class=\"material-icons\">chevron_right</i></a></li>";
            } else {
              var siguiente = parseInt(item.paginaAcual) + 1;
              pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + siguiente + "\"><i class=\"material-icons\">chevron_right</i></a></li>";
            }
          }
        }
        else {
          html += "<tr>";
          html += "<td>" + item.rol + "</td>";
          html += "<td>" + item.nombre + "</td>";
          html += "<td>" + item.apellido + "</td>";
          html += "<td>" + item.email + "</td>";
          html += "<td>" + item.telefono + "</td>";
          html += "<td>" + item.idDependencia + " - " + item.dependencia + "</td>";
          html += "<td>";
          html += "<div class=\"switch\">";
          html += "<label>No<input type=\"checkbox\" id=\"" + item.id + "\" onclick=\"setStatusUser(this)\" ";
          if (item.Status == "1") {
            html += "checked";
          }
          html += "><span class=\"lever\"></span>Sí</label>";
          html += "</div>";
          html += "</td>";
          html += "<td><button type=\"button\" value=\"" + item.id + "\" onclick=\"redirectBibliotecarios(this)\" ";
          html += "class=\"btn-floating btn-small blue waves-effect waves-light tooltipped\" data-position=\"right\" ";
          html += "data-delay=\"900\" data-tooltip=\"Ver detalles\"><i class=\"material-icons\">mode_edit</i></button></td></tr>";
        }
      });
      $('#getResult').html(html);
      $('ul.pagination').html(pages);
    }
  });

}

function agregarBibliotecario() {
  /*
  ** Permite agregar nuevo bibliotecario al sistema
  */
  var $form = $('#agregarBibliotecario');
  var correo = $form.find("input[name='txtCorreo']").val();
  var telefono = $form.find("input[name='txtTelefono']").val();
  var pass = $form.find("input[name='txtPass']").val();
  var nombre = $form.find("input[name='txtNombre']").val();
  var apellido = $form.find("input[name='txtApellido']").val();
  var dependencia = $form.find("[name='dependencia']").val();
  var numTrabajador = $form.find("[name='txtNumTrabajador']").val();
  var rol = $form.find("select[name='rol']").val();

  $.ajax({
    url: 'controllers/agregarBibliotecario.php',
    method: 'POST',
    data: {
      correo: correo, telefono: telefono, pass: pass,
      nombre: nombre, apellido: apellido, dependencia: dependencia,
      rol: rol, numTrabajador: numTrabajador
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "ok") {
        $('#modalAgregar').modal('close');
        Materialize.toast("Bibliotecario agregado con éxito", 4000, 'rounded');
        clearAddBibliotecario();
        getResultBiblio(1);
      } else {
        Materialize.toast(data, 4000, 'rounded');
      }
    }
  });
}

function buscarBibliotecario() {
  /*
  **  Obtiene los usuarios con nombre o apellido.
  */

  $.ajax({
    url: 'controllers/getUserBiblio.php',
    method: 'POST',
    data: {
      busqueda: $('#buscarBibliotecario').val()
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var html = "";
      var pages = "";
      var obj = jQuery.parseJSON(data);
      var cantidadObjetosJSON = Object.keys(obj).length;
      $.each(obj, function (i, item) {

        html += "<tr>";
        html += "<td>" + item.rol + "</td>";
        html += "<td>" + item.nombre + "</td>";
        html += "<td>" + item.apellido + "</td>";
        html += "<td>" + item.email + "</td>";
        html += "<td>" + item.telefono + "</td>";
        html += "<td>" + item.idDependencia + " - " + item.dependencia + "</td>";
        html += "<td>";
        html += "<div class=\"switch\">";
        html += "<label>No<input type=\"checkbox\" id=\"" + item.id + "\" onclick=\"setStatusUser(this)\" ";
        if (item.Status == "1") {
          html += "checked";
        }
        html += "><span class=\"lever\"></span>Sí</label>";
        html += "</div>";
        html += "</td>";
        html += "<td><button type=\"button\" value=\"" + item.id + "\" onclick=\"redirectBibliotecarios(this)\" ";
        html += "class=\"btn-floating btn-small blue waves-effect waves-light tooltipped\" data-position=\"right\" ";
        html += "data-delay=\"900\" data-tooltip=\"Ver detalles\"><i class=\"material-icons\">mode_edit</i></button></td></tr>";

      });
      $('#getResult').html(html);
      $('ul.pagination').html(pages);
    }
  });
}

function clearAddBibliotecario() {
  /*
  ** Limpia los campos en el menu de agregar bibliotecario si se descartan los campos
  */
  $("#correo").val("");
  $("#telefono").val("");
  $("#pass").val("");
  $("#nombre").val("");
  $("#apellido").val("");
  $("#numTrabajador").val("");
  Materialize.updateTextFields();
}

function redirectBibliotecarios(par) {
  /*
  ** Redirige con el id del bibliotecario hacia la pagina de usuariosBiblioteca.php y despues permite editar los datos.
  */
  var id = $(par).attr('value');
  window.location = 'usuariosBiblioteca.php?id=' + id;
}

function setStatusUser(par) {
  /*
  ** Edita el status del usuario. se comparte con administradorTecnicos.php
  */
  $.ajax({
    url: 'controllers/setStatus.php',
    method: 'POST',
    data: {
      id: $(par).attr("id"),
      status: $(par).is(':checked')
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {

    }
  });
}

// --- FIN administradorBiblioteca.php ---

// --- INICIO usuariosBiblioteca.php ---

function getBibliotecario() {
  /*
  ** Obtiene Todos los datos de un bibliotecario en especifico y los muestra en sus respectivos campos.
  */


  var $form = $('#showBibliotecario');
  var id = $form.find("input[name='hdIdBibliotecario']").val();
  document.title = 'Editar usuario ' + id;
  var email = "";
  var rol = "";
  var nombre = "";

  var getData = function () {
    $.ajax({
      url: 'controllers/getbibliotecario.php',
      method: 'POST',
      data: {
        id: id
      },
      beforeSend: function () {

      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      },
      success: function (data) {

        var obj = jQuery.parseJSON(data);
        $.each(obj, function (i, item) {
          if (i == "dependencia") {
            $("select[name='dependencia']").find("option[value='" + item + "']").prop('selected', true);
          } else if (i == "rol") {
            $("select[name='rol']").find("option[value='" + item + "']").prop('selected', true);
            $('img.circle').attr('src', 'img/usr' + item + '.jpeg');
          } else if (i == "correo") {
            $('#h1Email').text(item);
            $("#" + i).val(item);
          } else if (i == "nombre") {
            nombre += item + " ";
            $("#" + i).val(item);
          } else if (i == "apellido") {
            nombre += item;
            $("#" + i).val(item);
          } else if (i == "textoRol") {
            $('#pTipoBibliotecario').text(item);
          } else {
            $("#" + i).val(item);
          }
        });

        $('#h5Nombre').text(nombre);
        $('select').material_select();
        Materialize.updateTextFields();
      }
    });
  };

  llenarBibliotecas().done(getData);

}

function updateBibliotecario() {
  /*
  ** Actualiza los datos del bibliotecario desde la sesion de algun admin.
  */
  var $form = $('#showBibliotecario');
  var email = $form.find("input[name='txtCorreo']").val();
  var telefono = $form.find("input[name='txtTelefono']").val();
  var pass = $form.find("input[name='txtPass']").val();
  var nombre = $form.find("input[name='txtNombre']").val();
  var apellido = $form.find("input[name='txtApellido']").val();
  var numTrabajador = $form.find("input[name='txtNumTrabajador']").val();
  var dependencia = $form.find("select[name='dependencia']").val();
  var rol = $form.find("select[name='rol']").val();
  var id = $form.find("input[type=hidden][name='hdIdBibliotecario']").val();

  $.ajax({
    url: 'controllers/updateBibliotecarios.php',
    method: 'POST',
    data: {
      email: email, telefono: telefono, pass: pass, nombre: nombre, apellido: apellido,
      dependencia: dependencia, rol: rol, id: id, numTrabajador: numTrabajador
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      Materialize.toast(data, 4000, 'rounded');
      $('#h5Nombre').text(nombre + " " + apellido);
      $('#h1Email').text(email);
      $('#pTipoBibliotecario').text($form.find("select[name='rol']").find("option[value='" + rol + "']").html());
    }
  });



}

function deleteBibliotecario() {
  /*
  ** Desactiva de la base de datos el usuario seleccionado.
  */
  var $form = $('#showBibliotecario');
  var id = $form.find("input[type=hidden][name='hdIdBibliotecario']").val();

  $.ajax({
    url: 'controllers/deleteTecnicoOrBibliotecario.php',
    method: 'POST',
    data: {
      id: id
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data = "ok") {
        window.location = 'AdministradorBiblioteca.php';
      } else {
        Materialize.toast("Ha ocurrido un error desconocido.", 4000, 'rounded');
      }
    }
  });

}

// --- FIN usuariosBiblioteca.php ---

// --- INICIO AdministradorTecnicos.php ---

function getResultTec(page) {
  /*
  ** Obtiene todos los técnicos registrados en el sistema. Los muestra en una tabla.
  */
  $.ajax({
    url: 'controllers/getUserTec.php',
    method: 'POST',
    data: {
      page: page
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var html = "";
      var pages = "";
      var obj = jQuery.parseJSON(data);
      var cantidadObjetosJSON = Object.keys(obj).length;
      $.each(obj, function (i, item) {
        if (i == cantidadObjetosJSON - 1) {
          if (item.totalPaginas != 0) {
            if (item.paginaAcual == 1) {
              pages += "<li class=\"disabled\"><a href=\"#!\"><i class=\"material-icons\">chevron_left</i></a></li>";

            } else {
              pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + (parseInt(item.paginaAcual) - 1) + "\"><i class=\"material-icons\">chevron_left</i></a></li>";
            }
            for (var cont = Math.max(1, (parseInt(item.paginaAcual) - 3)); cont <= Math.min(item.totalPaginas, parseInt(item.paginaAcual) + 3); cont++) {
              if (cont == item.paginaAcual) {
                pages += "<li class=\"active\"><a>" + cont + "</a></li>";
              } else {
                pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + cont + "\">" + cont + "</a></li>";
              }
            }

            if (item.paginaAcual == item.totalPaginas) {
              pages += "<li class=\"disabled\"><a><i class=\"material-icons\">chevron_right</i></a></li>";
            } else {
              var siguiente = parseInt(item.paginaAcual) + 1;
              pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + siguiente + "\"><i class=\"material-icons\">chevron_right</i></a></li>";
            }
          }
        }
        else {
          html += "<tr>";
          html += "<td>" + item.nombre + "</td>";
          html += "<td>" + item.apellido + "</td>";
          html += "<td>" + item.email + "</td>";
          html += "<td>" + item.telefono + "</td>";
          html += "<td>";
          html += "<div class=\"switch\">";
          html += "<label>No<input type=\"checkbox\" id=\"" + item.id + "\" onclick=\"setStatusUser(this)\" ";
          if (item.Status == "1") {
            html += "checked";
          }
          html += "><span class=\"lever\"></span>Sí</label>";
          html += "</div>";
          html += "</td>";
          html += "<td><button type=\"button\" value=\"" + item.id + "\" onclick=\"redirectTecnico(this)\" ";
          html += "class=\"btn-floating btn-small blue waves-effect waves-light tooltipped\" data-position=\"right\" ";
          html += "data-delay=\"900\" data-tooltip=\"Ver detalles\"><i class=\"material-icons\">mode_edit</i></button></td></tr>";
        }
      });
      $('#getResultTec').html(html);
      $('ul.pagination').html(pages);
    }
  });
}

function redirectTecnico(par) {
  /* 
  ** Redirige con el id del tecncio hacia la pagina de tecnico.php y despues permite editar los datos. 
  */
  var id = $(par).attr('value');
  window.location = 'tecnico.php?id=' + id;

}

function agregarTecnico() {
  /*
  ** Permite agregar nuevo tecnico al sistema.
  */
  var $form = $('#agregarTecnico');
  var correo = $form.find("input[name='txtCorreo']").val();
  var telefono = $form.find("input[name='txtTelefono']").val();
  var pass = $form.find("input[name='txtPass']").val();
  var nombre = $form.find("input[name='txtNombre']").val();
  var apellido = $form.find("input[name='txtApellido']").val();
  var numTrabajador = $form.find("input[name='txtNumTrabajador']").val();

  $.ajax({
    url: 'controllers/agregarTecnico.php',
    method: 'POST',
    data: {
      correo: correo, telefono: telefono, pass: pass, nombre: nombre,
      apellido: apellido, numTrabajador: numTrabajador
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "Usuario registrado.") {
        $('#modalAgregar').modal('close');
        Materialize.toast(data, 4000, 'rounded');
        clearAddTecnico();
        getResultTec(1);
        $('#modalCrearTec').modal('close');
      } else {
        Materialize.toast(data, 4000, 'rounded');
      }
    }
  });
}

function buscarTecnico() {
  /*
  **  Obtiene los usuarios con nombre o apellido.
  */

  $.ajax({
    url: 'controllers/getUserTec.php',
    method: 'POST',
    data: {
      busqueda: $('#buscarTecnico').val()
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var html = "";
      var pages = "";
      var obj = jQuery.parseJSON(data);
      var cantidadObjetosJSON = Object.keys(obj).length;
      $.each(obj, function (i, item) {

        html += "<tr>";
        html += "<td>" + item.nombre + "</td>";
        html += "<td>" + item.apellido + "</td>";
        html += "<td>" + item.email + "</td>";
        html += "<td>" + item.telefono + "</td>";
        html += "<td>";
        html += "<div class=\"switch\">";
        html += "<label>No<input type=\"checkbox\" id=\"" + item.id + "\" onclick=\"setStatusUser(this)\" ";
        if (item.Status == "1") {
          html += "checked";
        }
        html += "><span class=\"lever\"></span>Sí</label>";
        html += "</div>";
        html += "</td>";
        html += "<td><button type=\"button\" value=\"" + item.id + "\" onclick=\"redirectTecnico(this)\" ";
        html += "class=\"btn-floating btn-small blue waves-effect waves-light tooltipped\" data-position=\"right\" ";
        html += "data-delay=\"900\" data-tooltip=\"Ver detalles\"><i class=\"material-icons\">mode_edit</i></button></td></tr>";

      });
      $('#getResultTec').html(html);
      $('ul.pagination').html(pages);
    }
  });
}

function clearAddTecnico() {
  /*
  ** Limpia los campos en el menu de agregar tecnico si se descartan los campos.
  */
  $("#correo").val("");
  $("#telefono").val("");
  $("#pass").val("");
  $("#nombre").val("");
  $("#apellido").val("");
  $("#apellido").val("");
  $("#numTrabajador").val("");
  Materialize.updateTextFields();
}

// --- FIN AdministradorTecnicos.php ---

// --- INICIO tecnico.php ---

function getTecnico() {
  /*
  ** Obtiene Todos los datos de un tecnico en especifico y los muestra en sus respectivos campos.
  */
  var $form = $('#showTecnico');
  var id = $form.find("input[name='hdIdTecnico']").val();
  document.title = 'Editar usuario ' + id;
  var nombre = "";

  $.ajax({
    url: 'controllers/getTecnico.php',
    method: 'POST',
    data: {
      id: id
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var obj = jQuery.parseJSON(data);
      $.each(obj, function (i, item) {
        if (i == "correo") {
          $('#h1Email').text(item);
          $("#" + i).val(item);
        } else if (i == "nombre") {
          nombre += item + " ";
          $("#" + i).val(item);
        } else if (i == "apellido") {
          nombre += item;
          $("#" + i).val(item);
        } else if (i == "rol") {
          $('img.circle').attr('src', 'img/usr' + item + '.jpeg');
        } else {
          $("#" + i).val(item);
        }
      });
      $('#h5Nombre').text(nombre);
      Materialize.updateTextFields();

    }
  });

}

function updateTecnico() {
  /*
  ** Actualiza los datos del tecnico desde la sesion de algun admin.
  */
  var $form = $('#showTecnico');
  var email = $form.find("input[name='txtCorreo']").val();
  var telefono = $form.find("input[name='txtTelefono']").val();
  var pass = $form.find("input[name='txtPass']").val();
  var nombre = $form.find("input[name='txtNombre']").val();
  var apellido = $form.find("input[name='txtApellido']").val();
  var numTrabajador = $form.find("input[name='txtNumTrabajador']").val();
  var id = $form.find("input[type=hidden][name='hdIdTecnico']").val();

  $.ajax({
    url: 'controllers/updateTecnico.php',
    method: 'POST',
    data: {
      email: email, telefono: telefono, pass: pass, nombre: nombre,
      apellido: apellido, id: id, numTrabajador: numTrabajador
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      Materialize.toast(data, 4000, 'rounded');
      $('#h1Email').text(email);
      $('#h5Nombre').text(nombre + " " + apellido);
    }
  });
}

function deleteTecnico() {
  /*
  ** Desactiva de la base de datos el usuario seleccionado.
  */
  var $form = $('#showTecnico');
  var id = $form.find("input[type=hidden][name='hdIdTecnico']").val();

  $.ajax({
    url: 'controllers/deleteTecnicoOrBibliotecario.php',
    method: 'POST',
    data: {
      id: id
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "ok") {
        window.location = 'AdministradorTecnicos.php';
      } else {
        Materialize.toast('Ha ocurrido un error desconocido.', 4000, 'rounded');
      }

    }
  });
}

// --- FIN tecnico.php ---

// --- INICIO tecnicoOrders.php ---

function comentarTecnico(idReporte) {
  /*
  **  Agrega un comentario al reporte seleccionado.
  */
  var $form = $('#reporte' + idReporte);
  var comentario = $form.find("input[type=text][name='txtComentario']").val();

  $.ajax({
    url: 'controllers/agregarComentario.php',
    method: 'POST',
    data: {
      comentario: comentario, idReporte: idReporte
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "ok") {
        $form.find("input[type=text][name='txtComentario']").val("");
        getComentarios(idReporte, 1);
      } else if (data == "dataNull") {
        Materialize.toast('Complete el campo "Comentario"', 4000, 'rounded');
      }
    }
  });

}

function getComentarios(idReporte, page) {
  /*
  ** Muestra los comentarios segun el reporte
  */
  var html = "";
  var pages = "";
  var ret;

  $.ajax({
    url: 'controllers/getComentarios.php',
    method: 'POST',
    data: {
      idReporte: idReporte, page: page
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var obj = jQuery.parseJSON(data);
      var cantidadObjetosJSON = Object.keys(obj).length;
      $.each(obj, function (i, item) {

        if (i == cantidadObjetosJSON - 1) {
          if (item.totalPaginas != 0) {
            if (item.paginaAcual == 1) {
              pages += "<li class=\"disabled\"><a><i class=\"material-icons\">chevron_left</i></a></li>";

            } else {
              pages += "<li class=\"waves-effect\"><a onclick=\"getComentarios(" + idReporte + "," + (parseInt(item.paginaAcual) - 1) + ")\"><i class=\"material-icons\">chevron_left</i></a></li>";
            }
            for (var cont = Math.max(1, (parseInt(item.paginaAcual) - 3)); cont <= Math.min(item.totalPaginas, parseInt(item.paginaAcual) + 3); cont++) {
              if (cont == item.paginaAcual) {
                pages += "<li class=\"active\"><a>" + cont + "</a></li>";
              } else {
                pages += "<li class=\"waves-effect\"><a onclick=\"getComentarios(" + idReporte + "," + cont + ")\">" + cont + "</a></li>";
              }
            }

            if (item.paginaAcual == item.totalPaginas) {
              pages += "<li class=\"disabled\"><a><i class=\"material-icons\">chevron_right</i></a></li>";
            } else {
              var siguiente = parseInt(item.paginaAcual) + 1;
              pages += "<li class=\"waves-effect\"><a onclick=\"getComentarios(" + idReporte + "," + siguiente + ")\"><i class=\"material-icons\">chevron_right</i></a></li>";
            }
          }
        } else {


          html += "<div class=\"row col s12 m12 right-align extended\">";
          html += "<p class=\"blue-grey-text text-darken-3 right-align right-spaced\">" + item.comentario + "</p>";
          html += "<div class=\"chip\"><img src=\"img/usr" + item.rol + ".jpeg\" alt=\"Contact Person\"><b>" + item.nombre + " " + item.apellido + "</b> " + item.fecha + "</div>";
          html += "</div>";

        }
      });
      $('#contComentarios' + idReporte).html(html);
      $('#pagComments' + idReporte).html(pages);
    }
  });
}

function getAllReports(page) {
  /*
  ** Obtiene todos los reportes en la base de datos y los muestra en la pagina tecnicoOrders.php
  */
  $('ul.pagination').show();
  $.ajax({
    url: 'controllers/getAllReports.php',
    method: 'POST',
    data: {
      page: page
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var obj = jQuery.parseJSON(data);
      $('#showRepots').html(fillReports(obj));
      $('#paginationReports').html(pagination(obj));
      $('.collapsible').collapsible();
    }
  });
}

function buscarReporte() {
  /*
  ** Busca por el asunto del reporte. Se obtiene del campo de texto y se analiza en la base de datos.
  */
  $('ul.pagination').hide();
  var $form = $('#busquedaReporte');
  var busqueda = $form.find("input[type=search]").val();
  if (busqueda != "") {
    $.ajax({
      url: 'controllers/getAllReports.php',
      method: 'POST',
      data: {
        asunto: busqueda
      },
      beforeSend: function () {

      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      },
      success: function (data) {
        var obj = jQuery.parseJSON(data);
        $('#showRepots').html(fillReports(obj));
        $('.collapsible').collapsible();
      }
    });
  } else {
    getAllReports(1);
  }
}

function limpiarBusquedaReporte() {
  /*
  ** Limpia el campo de busqueda al clickear la "X"
  */
  var $form = $('#busquedaReporte');
  var input = $form.find("input[type=search]").val("");
  getAllReports(1);
}

function filtrarReportes(page) {
  /*
  ** Filtra los reportes conforme al status del reporte. Se obtiene de los radiobutton y se analiza en la base de datos.
  */
  $('ul.pagination').show();
  var $form = $('#filtrarReportes');
  var radio = $form.find("input[type=radio][name='filtro']:checked").val();
  if (radio == 0) {
    getAllReports(1);
  } else {
    $.ajax({
      url: 'controllers/getAllReports.php',
      method: 'POST',
      data: {
        page: page, status: radio
      },
      beforeSend: function () {

      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      },
      success: function (data) {
        var obj = jQuery.parseJSON(data);
        $('#showRepots').html(fillReports(obj));
        $('ul.pagination').html(pagination(obj));
        $('.collapsible').collapsible();

      }
    });
  }
}

function redirectReporte(par) {
  /*
  ** Este es para ver los datos del reporte desde la sesion del tecnico. 
  ** Asigna al tecnico como reparador de ese reporte. 
  ** Se utiliza tambien en equipo.php
  */
  var id = $(par).attr('value');
  $.post('controllers/setTecnicoReport.php', { idReporte: id }, );
  window.location = 'orders.php?id=' + id;
}

function clickTecnico(page) {
  /*
  ** Llamado cuando se pulsa el boton de una pagina en especifico.
  */
  getAllReports(page);
  $("html, body").delay(50).animate({
    scrollTop: $('#buscarReportes').offset().top
  }, 200);
}

function clickTecnicoFiltro(page) {
  /*
  ** Llamado cuando se pulsa la pagina y esta el fitro de status activado.
  */
  filtrarReportes(page);
  $("html, body").delay(50).animate({
    scrollTop: $('#buscarReportes').offset().top
  }, 200);
}

function clickTecnicoFechas(page) {
  /*
  ** Llamado cuando se pulsa la pagina y esta el fitro de status activado.
  */
  filtroPorFechas(page);
  $("html body").delay(50).animate({
    scrollTop: $('#buscarReportes').offset().top
  }, 200);
}

function filtroCompleto(page) {
  /*
  ** Llamado cuando los dos campos de rango de fechas y el de status estan seleccionados.
  */
  var $form = $('#filtrarReportes');
  var fechaInicio = $form.find("input[id='fechaInicio']").val();
  var fechaFinal = $form.find("input[id='fechaFinal']").val();
  var status = $form.find("input[type=radio][name='filtro']:checked").val();
  if (status == "0") {
    filtroPorFechas(1);
  } else {
    $.ajax({
      url: 'controllers/getAllReports.php',
      method: 'POST',
      data: {
        page: page, fechaInicio: fechaInicio, fechaFinal: fechaFinal, status: status
      },
      beforeSend: function () {

      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      },
      success: function (data) {
        var obj = jQuery.parseJSON(data);
        $('#showRepots').html(fillReports(obj));
        $('ul.pagination').html(pagination(obj));
        $('.collapsible').collapsible();
      }
    });
  }
}

function filtroPorFechas(page) {
  /*
  ** Llamado cuando los dos campos de rango de fechas estan seleccionados.
  */
  var $form = $('#filtrarReportes');
  var fechaInicio = $form.find("input[id='fechaInicio']").val();
  var fechaFinal = $form.find("input[id='fechaFinal']").val();

  $.ajax({
    url: 'controllers/getAllReports.php',
    method: 'POST',
    data: {
      page: page, fechaInicio: fechaInicio, fechaFinal: fechaFinal
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var obj = jQuery.parseJSON(data);
      $('#showRepots').html(fillReports(obj));
      $('ul.pagination').html(pagination(obj));
      $('.collapsible').collapsible();
    }
  });
}

// --- FIN tecnicoOrders.php ---

// --- INICIO orders.php ---

function getOrder() {
  /*
  ** Obtiene el reporte especifico ademas de seleccionar el usuario reparador en un combobox
  ** conforme al id de reparador sacado del reporte. Esto permite modificar el usuario reparador.
  */

  var $form = $('#showOrder');
  var id = $form.find("input[name='hdIdOrder']").val();
  document.title = 'Orden #' + id;
  $('#h1IdReporte').text("ORDEN #" + id);

  $.ajax({
    url: 'controllers/getOrder.php',
    method: 'POST',
    data: {
      id: id
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var obj = jQuery.parseJSON(data);
      $.each(obj, function (i, item) {
        if (i == "fechaReporte") {
          $(document).ready(function () {
            $('#fechaReporte').text(item);
          });
        } else if (i == "asuntoReporte") {
          $(document).ready(function () {
            $('#asuntoReporte').text(item);
          });
        } else if (i == "estado") {
          $('#' + item + 's').prop('checked', true);

        } else if (i == "servicio") {
          $('#' + item + 't').prop('checked', true);

        } else if (i == "imgTipoEquipo") {
          $("#imgTipoEquipo").attr("src", "img/" + item + ".png");

        } else if (i == "reparador") {
          id = item;

        } else {
          $("#" + i).val(item);
        }
      });

      Materialize.updateTextFields();
      $.ajax({
        url: 'controllers/getTecnicoCombo.php',
        method: 'POST',
        data: {
          id: id
        },
        beforeSend: function () {

        },
        error: function (error) {
          Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
        },
        success: function (data) {
          $('#reparador').html(data);
          $('select').material_select();
        }
      });
    }
  });
  getComentarios(id, 1);

}

function updateReporte() {
  /*
  ** Obtiene los nuevos valores del reporte y los modifica.
  */
  var $form = $('#showOrder');
  var status = $form.find("input[type=radio][name='estado']:checked").val();
  var tipoServicio = $form.find("input[type=radio][name='servicio']:checked").val();
  var trabajoRealizado = $form.find("textarea[name='txtTrabajoRealizado']").val();
  var idTecnicoReparador = $form.find("select[name='txtReparador']").val();
  var idOrder = $form.find("input[name='hdIdOrder']").val();

  $.ajax({
    url: 'controllers/updateReporte.php',
    method: 'POST',
    data: {
      status: status, tipoServicio: tipoServicio, trabajoRealizado: trabajoRealizado,
      idOrder: idOrder, idTecnicoReparador: idTecnicoReparador
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      Materialize.toast(data, 4000, 'rounded');
    }
  });
}

// --- FIN orders.php ---

// --- INICIO TecnicoInventario.php ---

function getInventario(page) {
  /*
  ** Obtiene todos los equipos registrados en el sistema.
  */

  var form = $('#agregarEquipo');
  getProveedores(form);
  getEncargados(form);
  llenarBibliotecas();

  $.ajax({
    url: 'controllers/getInventario.php',
    method: 'POST',
    data: {
      page: page
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var html = "";
      var pages = "";
      var obj = jQuery.parseJSON(data);
      var cantidadObjetosJSON = Object.keys(obj).length;
      $.each(obj, function (i, item) {
        if (i == cantidadObjetosJSON - 1) {
          if (item.totalPaginas != 0) {
            if (item.paginaAcual == 1) {
              pages += "<li class=\"disabled\"><a><i class=\"material-icons\">chevron_left</i></a></li>";
            } else {
              pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + (parseInt(item.paginaAcual) - 1) + "\"><i class=\"material-icons\">chevron_left</i></a></li>";
            }

            for (var cont = Math.max(1, (parseInt(item.paginaAcual) - 3)); cont <= Math.min(item.totalPaginas, parseInt(item.paginaAcual) + 3); cont++) {
              if (cont == item.paginaAcual) {
                pages += "<li class=\"active\"><a>" + cont + "</a></li>";
              } else {
                pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + cont + "\">" + cont + "</a></li>";
              }
            }

            if (item.paginaAcual == item.totalPaginas) {
              pages += "<li class=\"disabled\"><a><i class=\"material-icons\">chevron_right</i></a></li>";
            } else {
              var siguiente = parseInt(item.paginaAcual) + 1;
              pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + siguiente + "\"><i class=\"material-icons\">chevron_right</i></a></li>";
            }
          }
        }
        else {
          html += "<tr>";
          html += "<td>" + item.nameTipoEquipo + "</td>";
          html += "<td>" + item.marca + "</td>";
          html += "<td>" + item.modelo + "</td>";
          html += "<td>" + item.serie + "</td>";
          html += "<td class=\"patrimonio\">" + item.patrimonio + "</td>";
          html += "<td>" + item.dependencia + "</td>";
          html += "<td>" + item.so + "</td>";
          html += "<td>" + item.office + "</td>";
          html += "<td>" + item.encargado + "</td>";
          html += "<td><button type=\"button\" onclick=\"redirectEquipo(this)\" ";
          html += "value=\"" + item.patrimonio + "\"class=\"btn-floating btn-small blue waves-effect waves-light tooltipped\" data-position=\"right\" ";
          html += "data-delay=\"900\" data-tooltip=\"Ver detalles\"><i class=\"material-icons\">assignment</i></button></td>";
          html += "</tr>";
        }
      });
      $('#getInventario').html(html);
      $('ul.pagination').html(pages);
      $('ul.pagination').show();
    }
  });
}

function buscarEquipo() {
  /*
  ** Busca por el numero de patrimonio del equipo. Se obtiene del campo de texto y se analiza en la base de datos.
  */

  $('ul.pagination').hide();
  var $form = $('#busquedaEquipo');
  var busqueda = $form.find("input[type=search]").val();
  if (busqueda != "") {
    $.ajax({
      url: 'controllers/getInventario.php',
      method: 'POST',
      data: {
        busqueda: busqueda
      },
      beforeSend: function () {

      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      },
      success: function (data) {
        var html = "";
        var pages = "";
        var obj = jQuery.parseJSON(data);
        var count = Object.keys(obj).length;
        $.each(obj, function (i, item) {
          html += "<tr>";
          html += "<td>" + item.nameTipoEquipo + "</td>";
          html += "<td>" + item.marca + "</td>";
          html += "<td>" + item.modelo + "</td>";
          html += "<td>" + item.serie + "</td>";
          html += "<td class=\"patrimonio\">" + item.patrimonio + "</td>";
          html += "<td>" + item.so + "</td>";
          html += "<td>" + item.office + "</td>";
          html += "<td>" + item.encargado + "</td>";
          html += "<td><button type=\"button\" onclick=\"redirectEquipo(this)\" ";
          html += "value=\"" + item.patrimonio + "\"class=\"btn-floating btn-small blue waves-effect waves-light tooltipped\" data-position=\"right\" ";
          html += "data-delay=\"900\" data-tooltip=\"Ver detalles\"><i class=\"material-icons\">assignment</i></button></td>";
          html += "</tr>";
        });
        $('#getInventario').html(html);
        $('#paginar').hide();
      }
    });
  } else {
    getInventario(1);
    $('#paginar').show();
  }
}

function limpiarBusquedaEquipo() {
  /*
  ** Limpia el campo de busqueda al clickear la "X"
  */
  var $form = $('#busquedaEquipo');
  var input = $form.find("input[type=search]").val("");
  getInventario(1);
  $('#paginar').show();
}

function redirectEquipo(par) {
  /*
  ** Este es para ver los datos del equipo desde la sesion del tecnico.
  */
  var patrimonio = $(par).attr('value');
  window.location = 'equipo.php?patrimonio=' + patrimonio;
}

function clearAddEquipo() {
  /*
  ** Limpia los campos en el menu de agregar bibliotecario si se descartan los campos
  */
  $("#1").prop('checked', 'checked');
  $("#marca").val("").removeClass("valid");
  $("#modelo").val("").removeClass("valid");
  $("#serie").val("").removeClass("valid");
  $("#patrimonio").val("").removeClass("valid");
  $("#so").val("").removeClass("valid");
  $("#office").val("").removeClass('valid');
  $("#nombreUsuario").val("").removeClass('valid');
  $("#folio").val("").removeClass('valid');
  $("#factura").val("").removeClass('valid');
  $("#programaFederal").val("").removeClass('valid');
  $("#proveedor").val("").removeClass('valid');
  $("#numTrabajador").val("").removeClass('valid');
  $("#description").val("");
  Materialize.updateTextFields();
}

function agregarEquipo() {
  /*
  ** Permite agregar nuevo equipo al sistema.
  */
  var $form = $('#agregarEquipo');
  var tipoEquipo = $form.find("input[type=radio][name='equipo']:checked").val();
  var marca = $form.find("input[name='txtMarca']").val();
  var modelo = $form.find("input[name='txtModelo']").val();
  var serie = $form.find("input[name='txtSerie']").val();
  var patrimonio = $form.find("input[name='txtPatrimonio']").val();
  var so = $form.find("input[name='txtSO']").val();
  var office = $form.find("input[name='txtOffice']").val();
  var usuario = $form.find("input[name='nombreUsuario']").val();
  var folio = $form.find("input[name='txtFolio']").val();
  var factura = $form.find("input[name='txtFactura']").val();
  var programaFederal = $form.find("input[name='txtProgramaFederal']").val();
  var proveedor = $form.find("input[name='txtProveedor']").val();
  var numTrabajador = $form.find("input[name='txtNumTrabajador']").val();
  var idProveedor = $form.find("input[name='hdIdProveedor']").val();
  var dependencia = $form.find("select[name='dependencia']").val();
  var description = $('#description').val();

  if (tipoEquipo != 4) {
    so = 'NA';
    office = 'NA';
  }

  $.ajax({
    url: 'controllers/agregarEquipo.php',
    method: 'POST',
    data: {
      tipoEquipo: tipoEquipo, marca: marca, modelo: modelo, serie: serie, patrimonio: patrimonio, so: so, office: office, usuario: usuario,
      description: description, folio: folio, factura: factura, programaFederal: programaFederal, proveedor: proveedor, numTrabajador: numTrabajador, idProveedor: idProveedor,
      dependencia: dependencia
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "ok") {
        $('#modalCrear').modal('close');
        clearAddEquipo();
        Materialize.toast('Equipo agregado con éxito.', 4000, 'rounded');
        getInventario(1);
      } else {
        Materialize.toast(data, 4000, 'rounded');
      }
    }
  });
}

function uploadFile() {

  $("#subirExcel").on('submit', (function (e) {
    $('#imgLoading').prop('hidden', false);
    e.preventDefault();
    $("#showErrores").empty();
    $.ajax({
      url: "controllers/uploaderExcel.php", // Url to which the request is send
      type: "POST",             // Type of request to be send, called as method
      data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
      contentType: false,       // The content type used when sending data to the server.
      cache: false,             // To unable request pages to be cached
      processData: false,        // To send DOMDocument or non processed data file it is set to false
      success: function (data)   // A function to be called if request succeeds
      {
        $('#imgLoading').prop('hidden', true)
        $("#showErrores").html(data);
        getInventario(1);
      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      }
    });
  }));
}

function getProveedores(formulario) {
  /*
  **  Permite llenar el autocomplete de los proveedores en agregarEquipo y detallesEquipo
  */
  var datosProveedores = {};
  var idsProveedores = {};
  var counter = 0;

  $.ajax({
    url: 'controllers/getProveedores.php',
    method: 'POST',
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (json) {
      var cantidadObjetosJSON = Object.keys(json).length;
      $.each(json, function (i, item) {

        datosProveedores[item.pr_proveedor] = null;
        idsProveedores[item.pr_proveedor] = item.pr_id;
      });
      $('#proveedor').autocomplete({
        data: datosProveedores,
        limit: 3,
        onAutocomplete: function (val) {
          //$('#idProveedor').val(idsProveedores[val]);
          /*$('<input>').attr({
            type: 'hidden',
            id: 'hdIdProveedor',
            name: 'hdIdProveedor',
            value: idsProveedores[val]
        }).appendTo(formulario);*/


        },
        minLength: 3,
      });
    }
  });
}

function getEncargado(idEncargado) {
  $.ajax({
    url: 'controllers/getEncargados.php',
    method: 'POST',
    data: {
      idEncargado: idEncargado
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (json) {
      $.each(json, function (i, item) {
        if (i != "error") {
          $('#nombreUsuario').val(item.en_nombre);
          Materialize.updateTextFields();
        }

      });
    }
  });
}

function getEncargados(formulario) {
  /*
  **  Permite llenar el autocomplete del nombre de encargado y numero de trabajador en agregarEquipo y detallesEquipo
  **  para despues llenar el campo nombre y/o numTrabajor
  */
  var datosNombresEncargados = {};
  var idsEncargados = {};
  var counter = 0;

  $.ajax({
    url: 'controllers/getEncargados.php',
    method: 'POST',
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (json) {
      $.each(json, function (i, item) {

        datosNombresEncargados[item.en_nombre] = null;
        idsEncargados[item.en_nombre] = item.en_numTrabajador;

      });
      $('#nombreUsuario').autocomplete({
        data: datosNombresEncargados,
        limit: 3,
        onAutocomplete: function (val) {
          if (formulario.selector == "#agregarEquipo") {
            $('#numTrabajador').val(idsEncargados[val]).addClass('valid');
          } else if (formulario.selector == "#showEquipo") {
            $('#numTrabajador').val(idsEncargados[val]);
          }
          Materialize.updateTextFields();
        },
        minLength: 3,
      });

    }
  });
}

function exportarInventario() {
  /*
  **  Permite llamar al controlador para exportar el inventario existente en equipos.
  */
  Materialize.toast('Generando archivo .xlsx', 4000, 'rounded');
  window.location = "controllers/exportarInventario.php"
}

// --- FIN TecnicoInventario.php ---

// --- INICIO equipo.php ---

function getEquipo() {
  /*
  ** Obtiene todos los datos de un equipo en especifico y los muestra en su campo correspondiente.
  */

  var $form = $('#showEquipo');
  var patrimonio = $form.find("input[name='oldPatrimonio']").val();
  document.title = "Patrimonio # " + patrimonio;

  getReportsByEquipo(1, patrimonio);
  $.ajax({
    url: 'controllers/getEquipo.php',
    method: 'POST',
    data: {
      idPatrimonio: patrimonio
    },
    beforeSend: function () {
      getProveedores($form);
      getEncargados($form);
      llenarBibliotecas();
    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var usuario = "";
      var obj = jQuery.parseJSON(data);
      $.each(obj, function (i, item) {
        if (i == "tipoEquipo") {
          if (item != 4) {
            $('#so').attr('disabled', true).val("").removeClass('valid');
            $('#office').attr('disabled', true).val("").removeClass('valid');
            Materialize.updateTextFields();
          }
          $("#imgTipoEquipo").attr("src", "img/" + item + ".png");
        } else if (i == "contador") {
          $('#contador').text(item);
        } else if (i == "usuario") {
          $('#nombreUsuario').val(item);
          usuario = item;
        } else if (i == "idDependencia") {
          $("select[name='dependencia']").find("option[value='" + item + "']").prop('selected', true);
          $('select').material_select();
        } else {
          $("#" + i).val(item);
        }
      });
      Materialize.updateTextFields();
    }
  });
}

function getReportsByEquipo(page, patrimonio) {
  /*
  ** Usuado para obtener los reportes segun un numero de patrimonio especifico.
  */
  $.ajax({
    url: 'controllers/getAllReports.php',
    method: 'POST',
    data: {
      page: page, patrimonio: patrimonio
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var obj = jQuery.parseJSON(data);
      $('#showRepots').html(fillReports(obj));
      $('ul.pagination').html(pagination(obj));
      $('.collapsible').collapsible();
    }
  });
}

function clickPatrimonio(param, patrimonio) {
  /*
  ** Llamado cuando se pulsa el boton de una pagina en especifico.
  */
  var page = $(param).attr("id");
  getReportsByEquipo(page, patrimonio);
  $("html, body").delay(50).animate({
    scrollTop: $('body').offset().top
  }, 200);
}

function updateEquipo() {
  /*
  **  Obtiene los datos modificados de un equipo y los actualiza en base de datos.
  */
  var $form = $('#showEquipo');
  var marca = $form.find("input[name='txtMarca']").val();
  var modelo = $form.find("input[name='txtModelo']").val();
  var serie = $form.find("input[name='txtSerie']").val();
  var patrimonio = $form.find("input[name='txtPatrimonio']").val();
  var so = $form.find("input[name='txtSO']").val();
  var office = $form.find("input[name='txtOffice']").val();
  var usuario = $form.find("input[name='nombreUsuario']").val();
  var folio = $form.find("input[name='txtFolio']").val();
  var factura = $form.find("input[name='txtFactura']").val();
  var programaFederal = $form.find("input[name='txtProgramaFederal']").val();
  var proveedor = $form.find("input[name='txtProveedor']").val();
  var numTrabajador = $form.find("input[name='txtNumTrabajador']").val();
  var dependencia = $form.find("select[name='dependencia']").val();
  var description = $('#description').val();
  var oldPatrimonio = $form.find("input[type=hidden][name='oldPatrimonio']").val();

  $.ajax({
    url: 'controllers/updateEquipo.php',
    method: 'POST',
    data: {
      marca: marca, modelo: modelo, serie: serie, patrimonio: patrimonio, so: so, office: office, usuario: usuario, oldPatrimonio: oldPatrimonio,
      folio: folio, factura: factura, programaFederal: programaFederal, description: description, proveedor: proveedor, numTrabajador: numTrabajador,
      dependencia: dependencia
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var obj = jQuery.parseJSON(data);
      $.each(obj, function (i, item) {
        if (item.success) {
          Materialize.toast(item.success, 4000, 'rounded');
          window.history.pushState('Object', 'Title', '/srs/proyecto/equipo.php?patrimonio=' + patrimonio);
          $('input[name="oldPatrimonio"]').attr('value', patrimonio);
          getProveedores($form);
          getEncargados($form);
        }
        if (item.error) {
          if (item.error == "fills") {
            Materialize.toast('Rellene todos los campos.', 4000, 'rounded');
          } else {
            Materialize.toast(item.error, 4000, 'rounded');
            $('#patrimonio').val(oldPatrimonio);
          }
        }
      });
    }
  });
}

function deleteEquipo() {
  /*
  ** Cambia el status del equipo a eliminado
  */
  var razon = $('#razon').val();
  var idPatrimonio = $('#showEquipo').find("input[name='oldPatrimonio']").val();
  $.ajax({
    url: 'controllers/deleteEquipo.php',
    method: 'POST',
    data: {
      razon: razon, idPatrimonio: idPatrimonio
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "ok") {
        $('#modalBorrarEQU').modal('close');
        Materialize.toast('Equipo dado de baja correctamente.', 4000, 'rounded');
        setTimeout(function () {
          window.location = "TecnicoInventario.php";
        }, 4500);
      } else if (data == "noReazon") {
        Materialize.toast('No ha señalado el motivo.', 4000, 'rounded');
      }
    }
  });
}

// --- FIN equipo.php ---

// --- INICIO bibliotecaVer.php ---

//function getReportsBibliotecario(page, idBibliotecario){ 
/*
** Obtiene todos los reportes en la base de datos del bibliotecario logueado
*/
/*$.post('controllers/getAllReports.php', {page:page, idBibliotecario:idBibliotecario},
 function(data) {
   var obj = jQuery.parseJSON(data);  
   $('#showRepots').html(fillReports(obj));
   $('ul.pagination').html(pagination(obj));
   $('.collapsible').collapsible();
 });
}*/

function clickBibliotecario(page, id) {
  /*
  ** Llamado cuando se pulsa el boton de una pagina en especifico.
  */
  getReportsBibliotecario(page, id);
  $("html, body").delay(50).animate({
    scrollTop: $('body').offset().top
  }, 200);
}

function buscarReporteBibliotecario() {
  /*
  ** Busca por el asunto del reporte. Se obtiene del campo de texto y se analiza en la base de datos.
  */
  $('ul.pagination').hide();
  var $form = $('#busquedaReporte');
  var busqueda = $form.find("input[type=search]").val();
  if (busqueda != "") {
    $.ajax({
      url: 'controllers/getAllReports.php',
      method: 'POST',
      data: {
        asunto: busqueda
      },
      beforeSend: function () {

      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      },
      success: function (data) {
        var obj = jQuery.parseJSON(data);
        $('#showRepots').html(fillReports(obj));
        $('.collapsible').collapsible();
      }
    });
  } else {
    getAllReports(1);
  }
}

// --- FIN bibliotecaVer.php ---

// --- FIN BibliotecaHacer.php ---

function report() {
  /*
  ** Permite realizar un reporte en el sistema.
  */
  var $form = $('#formReport');
  var asunto = $form.find("input[name='txtAsunto']").val();
  var noSerie = $form.find("input[name='txtNoSerie']").val();
  var marca = $form.find("input[name='txtMarca']").val();
  var modelo = $form.find("input[name='txtModelo']").val();
  var patrimonio = $form.find("input[name='txtPatrimonio']").val();
  var trabajoSolicitado = $form.find("textarea[name='txtTrabajoSolicitado']").val();
  var tipoEquipo = $form.find("input[type=radio][name='equipo']:checked").val();
  var tipoServicio = $form.find("input[type=radio][name='servicio']:checked").val();

  $.ajax({
    url: 'controllers/report.php',
    method: 'POST',
    data: {
      asunto: asunto, noSerie: noSerie, marca: marca, modelo: modelo, patrimonio: patrimonio, trabajoSolicitado: trabajoSolicitado,
      tipoEquipo: tipoEquipo, tipoServicio: tipoServicio
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data != "error") {
        Materialize.toast("Reporte generado.", 4000, 'rounded');
        setTimeout(function () {
          window.location = data;
        }, 4500);
      } else {
        Materialize.toast("Favor de introducir todos los datos.", 4000, 'rounded');
      }
    }
  });
}

// --- FIN BibliotecaHacer.php ---

// --- INICIO directivoBiblioteca.php ---

function getBibliotecariosByDependencia(page, idDependencia) {
  /**
  ** Obtiene la lista de bibliotecarios según su dependencia.
  **/
  $.ajax({
    url: 'controllers/getUserBiblio.php',
    method: 'POST',
    data: {
      page: page, idDependencia: idDependencia
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var html = "";
      var pages = "";
      var obj = jQuery.parseJSON(data);
      var cantidadObjetosJSON = Object.keys(obj).length;
      $.each(obj, function (i, item) {
        if (i == cantidadObjetosJSON - 1) {
          if (item.totalPaginas != 0) {
            if (item.paginaAcual == 1) {
              pages += "<li class=\"disabled\"><a><i class=\"material-icons\">chevron_left</i></a></li>";

            } else {
              var anterior = parseInt(item.paginaAcual) - 1;
              pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + anterior + "\"><i class=\"material-icons\">chevron_left</i></a></li>";
            }
            for (var cont = Math.max(1, (parseInt(item.paginaAcual) - 3)); cont <= Math.min(item.totalPaginas, parseInt(item.paginaAcual) + 3); cont++) {
              if (cont == item.paginaAcual) {
                pages += "<li class=\"active\"><a>" + cont + "</a></li>";
              } else {
                pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + cont + "\">" + cont + "</a></li>";
              }
            }

            if (item.paginaAcual == item.totalPaginas) {
              pages += "<li class=\"disabled\"><a><i class=\"material-icons\">chevron_right</i></a></li>";
            } else {
              var siguiente = parseInt(item.paginaAcual) + 1;
              pages += "<li class=\"waves-effect\"><a class=\"pages\" value=\"" + siguiente + "\"><i class=\"material-icons\">chevron_right</i></a></li>";
            }
          }
        }
        else {
          html += "<tr>";
          html += "<td>" + item.rol + "</td>";
          html += "<td>" + item.nombre + "</td>";
          html += "<td>" + item.apellido + "</td>";
          html += "<td>" + item.email + "</td>";
          html += "<td>" + item.telefono + "</td>";
          html += "<td>" + item.idDependencia + " - " + item.dependencia + "</td>";
          html += "<td>";
          html += "<div class=\"switch\">";
          html += "<label>No<input type=\"checkbox\" id=\"" + item.id + "\" onclick=\"setStatusUser(this)\" ";
          if (item.Status == "1") {
            html += "checked";
          }
          html += "><span class=\"lever\"></span>Sí</label>";
          html += "</div>";
          html += "</td>";
          html += "<td><button type=\"button\" value=\"" + item.id + "\" onclick=\"redirectBibliotecarios(this)\" ";
          html += "class=\"btn-floating btn-small blue waves-effect waves-light tooltipped\" data-position=\"right\" ";
          html += "data-delay=\"900\" data-tooltip=\"Ver detalles\"><i class=\"material-icons\">mode_edit</i></button></td></tr>";
        }
      });
      $('#getResult').html(html);
      $('ul.pagination').html(pages);
      $('ul.pagination').show();
    }
  });
}

function agregarBibliotecarioByDirectivo() {
  /*
  ** Permite agregar nuevo bibliotecario al sistema
  */
  var $form = $('#agregarBibliotecario');
  var correo = $form.find("input[name='txtCorreo']").val();
  var telefono = $form.find("input[name='txtTelefono']").val();
  var pass = $form.find("input[name='txtPass']").val();
  var nombre = $form.find("input[name='txtNombre']").val();
  var apellido = $form.find("input[name='txtApellido']").val();
  var dependencia = $form.find("[name='dependencia']").val();
  var numTrabajador = $form.find("[name='txtNumTrabajador']").val();
  var rol = $form.find("select[name='rol']").val();

  $.ajax({
    url: 'controllers/agregarBibliotecario.php',
    method: 'POST',
    data: {
      correo: correo, telefono: telefono, pass: pass, nombre: nombre, apellido: apellido,
      dependencia: dependencia, rol: rol, numTrabajador: numTrabajador
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      if (data == "ok") {
        $('#modalCrearBib').modal('close');
        Materialize.toast("Bibliotecario agregado con éxito", 4000, 'rounded');
        getBibliotecariosByDependencia(1, dependencia);
        clearAddBibliotecario();

      } else {
        Materialize.toast(data, 4000, 'rounded');
      }
    }
  });
}

function buscarBibliotecarioByDirectivo(idDependencia) {
  /*
  **  Obtiene los usuarios con nombre o apellido.
  */

  var busqueda = $('#buscarBibliotecario').val();
  $.ajax({
    url: 'controllers/getUserBiblio.php',
    method: 'POST',
    data: {
      busqueda: busqueda, idDependencia, idDependencia
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var html = "";
      var obj = jQuery.parseJSON(data);
      var cantidadObjetosJSON = Object.keys(obj).length;
      $.each(obj, function (i, item) {

        html += "<tr>";
        html += "<td>" + item.rol + "</td>";
        html += "<td>" + item.nombre + "</td>";
        html += "<td>" + item.apellido + "</td>";
        html += "<td>" + item.email + "</td>";
        html += "<td>" + item.telefono + "</td>";
        html += "<td>" + item.dependencia + "</td>";
        html += "<td>";
        html += "<div class=\"switch\">";
        html += "<label>No<input type=\"checkbox\" id=\"" + item.id + "\" onclick=\"setStatusUser(this)\" ";
        if (item.Status == "1") {
          html += "checked";
        }
        html += "><span class=\"lever\"></span>Sí</label>";
        html += "</div>";
        html += "</td>";
        html += "<td><button type=\"button\" value=\"" + item.id + "\" onclick=\"redirectBibliotecarios(this)\" ";
        html += "class=\"btn-floating btn-small blue waves-effect waves-light tooltipped\" data-position=\"right\" ";
        html += "data-delay=\"900\" data-tooltip=\"Ver detalles\"><i class=\"material-icons\">mode_edit</i></button></td></tr>";

      });
      $('#getResult').html(html);
      $('ul.pagination').hide();
    }
  });
}

// --- FIN directivoBiblioteca.php ---

// --- INICIO directivoVer.php ---

function getReportesByDependencia(page, idDependencia) {
  /*
  **  Obtiene los reportes de una dependencia en especifico
  */

  $('ul.pagination').show();
  $.ajax({
    url: 'controllers/getAllReports.php',
    method: 'POST',
    data: {
      page: page, idDependencia: idDependencia
    },
    beforeSend: function () {

    },
    error: function (error) {
      Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
    },
    success: function (data) {
      var obj = jQuery.parseJSON(data);
      $('#showRepots').html(fillReports(obj));
      $('#paginationReports').html(pagination(obj));
      $('.collapsible').collapsible();
      $('.eraseDiv').remove();
      $('.imgStatus').attr('style', 'padding-right: 5%; margin-bottom: 9%');
    }
  });
}

function buscarReporteDirectivo(idDependencia) {
  /*
  **  Busca por aunto segun la dependencia.
  */
  $('ul.pagination').hide();
  var $form = $('#busquedaReporte');
  var busqueda = $form.find("input[type=search]").val();
  if (busqueda != "") {
    $.ajax({
      url: 'controllers/getAllReports.php',
      method: 'POST',
      data: {
        asunto: busqueda, idDependencia: idDependencia
      },
      beforeSend: function () {

      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      },
      success: function (data) {
        var obj = jQuery.parseJSON(data);
        $('#showRepots').html(fillReports(obj));
        $('.collapsible').collapsible();
      }
    });
  } else {
    getReportesByDependencia(1, idDependencia);
  }
}

function filtrarReportesDirectivo(page, idDependencia) {
  /*
  ** Filtra los reportes conforme al status del reporte. Se obtiene de los radiobutton y se analiza en la base de datos.
  */
  $('ul.pagination').show();
  var $form = $('#filtrarReportes');
  var radio = $form.find("input[type=radio][name='filtro']:checked").val();
  if (radio == 0) {
    getReportesByDependencia(1, idDependencia);
  } else {
    $.ajax({
      url: 'controllers/getAllReports.php',
      method: 'POST',
      data: {
        page: page, status: radio, idDependencia: idDependencia
      },
      beforeSend: function () {

      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      },
      success: function (data) {
        var obj = jQuery.parseJSON(data);
        $('#showRepots').html(fillReports(obj));
        $('ul.pagination').html(pagination(obj));
        $('.collapsible').collapsible();
        $('.eraseDiv').remove();
        $('.imgStatus').attr('style', 'padding-right: 5%; margin-bottom: 9%');
      }
    });
  }
}

function filtroCompletoDirectivo(page, idDependencia) {
  /*
  ** Llamado cuando los dos campos de rango de fechas y el de status estan seleccionados.
  */
  var $form = $('#filtrarReportes');
  var fechaInicio = $form.find("input[id='fechaInicio']").val();
  var fechaFinal = $form.find("input[id='fechaFinal']").val();
  var status = $form.find("input[type=radio][name='filtro']:checked").val();
  if (status == "0") {
    filtroPorFechas(1);
  } else {
    $.ajax({
      url: 'controllers/getAllReports.php',
      method: 'POST',
      data: {
        page: page, fechaInicio: fechaInicio, fechaFinal: fechaFinal, status: status, idDependencia
      },
      beforeSend: function () {

      },
      error: function (error) {
        Materialize.toast('Error al conectarse con el servidor. Error: ' + error.statusText, 4000, 'rounded');
      },
      success: function (data) {
        var obj = jQuery.parseJSON(data);
        $('#showRepots').html(fillReports(obj));
        $('ul.pagination').html(pagination(obj));
        $('.collapsible').collapsible();
      }
    });
  }
}

// --- FIN directivoVer.php ---

/*
// --- ESTRUCTURA BASICA AJAX ---
$.ajax({
    url:'controllers/getEquipo.php',
    method: 'POST',
    data:{
      idPatrimonio:patrimonio
    },
    beforeSend: function(){
    
    },
    error: function(error){
      Materialize.toast('Error al conectarse con el servidor. Error: '+ error.statusText, 4000, 'rounded');
    },
    success: function(data){
     
    }
  });
  */