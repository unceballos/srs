<?php 
		session_start();
		if (empty($_SESSION['txtEmail'])) {
    		header('Location: login.php');
    		die();
    	}
    	if($_SESSION['txtRol']!=1){
    		if($_SESSION['txtRol']==2){
    			header('Location: tecnicoOrders.php');
    		}else if($_SESSION['txtRol']==3){
    			header('Location: bibliotecaVer.php');
    		}
		}
	?>
<html>
<head>
	<title>Estadísticas</title>
	<link rel="shortcut icon" href="img/icon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/navbar.css">
	<link rel="stylesheet" type="text/css" href="css/biblioteca.css">
	<link rel="stylesheet" type="text/css" href="css/radios.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>
</head>

<body class="blue-grey lighten-5" onload="cargando()">

	<!-- Navbar and Header -->
	<nav class="nav-extended cyan darken-3" style="margin-bottom: 4%">
		<div class="nav-background nabground">
			<div class="ea k"></div>
		</div>
		<div class="nav-wrapper db">
			<!-- LOGO -->
			<a href="" data-activates="mobile" class="button-collapse"><i class="white-text material-icons">menu</i></a>
			<ul class="bt hide-on-med-and-down">
				<li><a class="dropdown-button white-text" href="#!" data-activates="dropdown1"><?php echo $_SESSION['txtEmail'];?><i class="material-icons right">arrow_drop_down</i></a></li>
			</ul>

			<div class="nav-header de">
				<!-- WHAT THE USER IT-S SEARCHING -->
				<div class="row">
					<div class="col s4 offset-s4 center-align">
						<img src="img/helmet.png" style="width: 20%; margin-bottom: -6%">
					</div>
				</div>
				<h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">Administrador</h3>
				<h1>ESTADÍSTICAS</h1>
			</div>
		</div>

		<!-- Dropdown Structure -->
		<ul id='dropdown1' class='dropdown-content ddd'>
			<li><a href="controllers/logout.php">Cerrar sesión</a></li>
		</ul>

		<div class="categories-wrapper row cyan darken-4">
			<div class="center-align">
				<ul>
					<li class="col s4 m2 offset-m3"><a href="AdministradorTecnicos.php" class="white-text">TÉCNICOS</a></li>
					<li class="col s4 m2"><a href="AdministradorBiblioteca.php" class="white-text">BIBLIOTECAS</a></li>
					<li class="col s4 m2 k"><a href="AdministradorEstadisticas.php" class="white-text">ESTADÍSTICAS</a></li>
				</ul>
			</div>
		</div>
	</nav>

	<!-- SIDE NAV -->
	<ul id="mobile" class="side-nav">
		<li>
			<div class="userView">
				<div class="background">
					<img src="img/library.jpeg">
				</div>
				<a href="#!user"><img style="width:25%;" src="img/school.png"></a>
				<a href="#!name"><span class="white-text name">Técnico</span></a>
				
				<!-- Email of the user in here -->
				<a href="#!email"><span class="white-text email"><?php echo $_SESSION['txtEmail'];?></span></a>
			</div>
		</li>

		<li><a class="subheader">Actividades</a></li>
		<li><a class="waves-effect" href="bibliotecaVer.php" ><i class="material-icons">assignment</i>Ver mis reportes</a></li>
		<li><a class="waves-effect" href="bibliotecaHacer.php" ><i class="material-icons">mode_edit</i>Hacer un reporte</a></li>
		<li><a class="waves-effect" href="controllers/logout.php"> <i class="material-icons">perm_identity</i>Salir de mi cuenta</a></li>
	</ul>
		
	<!-- CONTENIDO DE LA PÁGINA -->
	<div class="row container">
		
		<div class="col s12 m12">
			<div class="row">
				<div class="col s12 m12 center-align">
				
				<!-- FECHAS -->
				<div class="container">
					<form id="rangoFechas" class="row">
						<h5 class="grey-text text-darken-3 center-align">Filtrado por fechas</h5>
						<p class="grey-text text-darken-1 center-align">Introduzca un rango de fechas para que las gráficas le muestren la información deseada desde una fecha inicial hasta una fecha límite</p><br>
						<div class="col m6 left-align">
							<label for="fechaInicio" class="active ">Rango de inicio</label>
							<input id="fechaInicio" type="text" class="datepicker" aria-haspopup="true" aria-expanded="true" aria-readonly="false">
						</div>
						<div class="col m6 right-align">		
							<label for="fechaFin" class="active right-align">Rango final</label>
							<input id="fechaFin" type="text" class="datepicker right-align" aria-haspopup="true" aria-expanded="true" aria-readonly="false">
						</div>
						<button type="button" onclick="obtenerDatos()"class="waves-effect waves-light btn wide center-align">Filtrar por las fechas definidas</button>
					</form>
				</div>

				<br><div class="divider"></div><br>
				
				<!-- TOTAL Y ATENDIDOS -->
				<h2 class="grey-text text-darken-3">0</h2>
				<small id="total_reportes" class="grey-text text-darken-3">(0 en total)</small>
				<h5 class="grey-text text-darken-3">Reportes atendidos este mes</h5>
					
			</div>
		</div>		
	</div>
	<div id="grafTotalContainer">
		<canvas id="grafTotal" width="400" height="150"></canvas>
		<br><div class="divider"></div><br>
	</div>
	

<div class="row">
	<div class="col m6 s12 center-align" id="grafStatusContainer">
		<h5 class="grey-text text-darken-3">Estado de los reportes</h5>
		<canvas id="grafStatus" width="200" height="150"></canvas>
	</div>
	<div class="col m6 s12 center-align" id="grafEquiposContainer">
		<h5 class="grey-text text-darken-3">Equipos reportados</h5>
		<canvas id="grafEquipos" width="200" height="150"></canvas>
	</div>
	
</div>

	<br><div class="divider"></div><br>
	<div class=" center-align" id="grafBibliotecasContainer">
		<h5 class="grey-text text-darken-3">Reportes generados por biblioteca</h5>
		<canvas id="grafBibliotecas" width="400" height="500"></canvas>
	</div>

<!-- FIN DEL CONTENIDO DE LA PAGINA -->

	</body>
	<script src="js/jquery-2.1.4.min.js" />"></script>
	<script src="js/materialize.min.js" />"></script>
	<script src="js/scripts.js" />"></script>
	<script src="js/graficas.js"></script>
	<script>
		$(document).ready(function() {
			$('.modal').modal();
			$('#fechaInicio').pickadate({
			    selectMonths: true, // Creates a dropdown to control month
			    selectYears: 5, // Creates a dropdown of 15 years to control year,
			    today: 'Hoy',
			    clear: 'Limpiar',
			    close: 'Ok',
			    max: new Date(),
			    firstDay: true,
			    closeOnSelect: false,
			    format: 'yyyy-mm-dd' // Close upon selecting a date,
			});
			$('#fechaFin').prop('disabled', true);

		});
		$('.dropdown-button').dropdown({belowOrigin: true});
		$(document).ready(function() {
			$('select').material_select();
		});
		$(".button-collapse").sideNav();
		

		$('#fechaInicio').change(function() {
			$('#fechaFin').prop('disabled', false);
			var date = $('#fechaInicio').val(),
			date=date.replace('-',',')
			$('#fechaFin').pickadate({
		    	selectMonths: true, // Creates a dropdown to control month
		    	selectYears: 5, // Creates a dropdown of 15 years to control year,
		    	today: 'Hoy',
		    	clear: 'Limpiar',
		    	close: 'Ok',
		    	min: new Date(date),
		    	max: new Date(),
		    	firstDay: true,
		    	closeOnSelect: false,
		    	format: 'yyyy-mm-dd' // Close upon selecting a date,
		  	});
    
		});

		
	</script>
	</html>
