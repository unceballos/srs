<?php
		session_start();
		if (empty($_SESSION['txtEmail'])) {
    		header('Location: login.php');
    		die();
    	}
    	if($_SESSION['txtRol']!=3||$_SESSION['txtRol']!=4||$_SESSION['txtRol']!=2){
    		if($_SESSION['txtRol']==1){
    			header('Location: administradorBiblioteca.php');
    		}
		}

	?>
<html>
<head>
	<title>Hacer un reporte</title>
	<link rel="shortcut icon" href="img/icon.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/navbar.css">
	<link rel="stylesheet" type="text/css" href="css/biblioteca.css">
	<link rel="stylesheet" type="text/css" href="css/radios.css">
	<style>
    .backdrop{
       background-color: teal;
     }
  </style>
</head>

<body class="indigo-grey lighten-5">

	<!-- Navbar and Header -->
	<nav class="nav-extended cyan darken-3" style="margin-bottom: 4%">
		<div class="nav-background nabground">
			<div class="ea k"></div>
		</div>

		<div class="nav-wrapper db">
			<!-- LOGO -->
			<a href="#" data-activates="mobile" class="button-collapse"><i
			class="material-icons">menu</i></a>
			<ul class="bt hide-on-med-and-down">
				<li>
					<a class="dropdown-button" href="#!" data-activates="dropdown1"><?php echo $_SESSION['txtEmail'];?><i class="material-icons right">arrow_drop_down</i></a>
				</li>
			</ul>

			<div class="nav-header de">
				<div class="row">
					<div class="col s4 offset-s4 center-align">
						<?php
						if($_SESSION['txtRol']==2){
							?><img src="img/screwdriver.png" style="width: 20%; margin-bottom: -6%"><?php
						}else{
							?><img src="img/school.png" style="width: 20%; margin-bottom: -6%"><?php
						}
						?>
						
					</div>
				</div>
				<?php
				if($_SESSION['txtRol']==4){
					?><h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">ENCARGADO</h3><?php
				}else if($_SESSION['txtRol']==2){
					?><h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">Técnico</h3><?php
				}else{
					?><h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">BIBLIOTECA</h3><?php
				}
				?>
				<h1>HACER UN REPORTE</h1>
			</div>
		</div>

		<!-- Dropdown Structure -->
		<ul id='dropdown1' class='dropdown-content ddd'>
			<li><a href="controllers/logout.php">Cerrar sesión</a></li>
		</ul>

		<div class="categories-wrapper row cyan darken-4">
				<div class="center-align">
				<?php
				if($_SESSION['txtRol']==4){
					?>
					<ul>
						<li class="col s6 m2 offset-m3"><a href="directivoVer.php" class="white-text">VER MIS REPORTES</a></li>
						<li class="col s6 m2 k"><a href="bibliotecaHacer.php" class="white-text">HACER REPORTE</a></li>
						<li class="col s6 m2"><a href="directivoBiblioteca.php" class="white-text">BIBLIOTECARIOS</a></li>
					</ul>
					<?php
				}else if($_SESSION['txtRol']==2){
					?>
					<ul>
						<li class="col s4 m2 offset-m3"><a href="TecnicoOrders.php" class="white-text">REPORTES</a></li>
						<li class="col s4 m2 k"><a href="bibliotecaHacer.php" class="white-text">HACER REPORTE</a></li>
						<li class="col s4 m2"><a href="TecnicoInventario.php" class="white-text">INVENTARIO</a></li>
					</ul>
					<?php
				}else{
					?>
					<ul>
						<li class="col s6 m3 offset-m3"><a href="bibliotecaVer.php" class="white-text">VER MIS REPORTES</a></li>
						<li class="col s6 m3 k"><a href="bibliotecaHacer.php" class="white-text">HACER REPORTE</a></li>
					</ul>
					<?php
				}
				?>
					
				</div>
			</div>
	</nav>

	<!-- SIDE NAV -->
	<ul id="mobile" class="side-nav">
		<li>
			<div class="userView">
				<div class="background">
					<img src="img/library.jpeg">
				</div>
				<a href="#!user"><img style="width:25%;"
				src="img/school.png"></a>
				<a href="#!name"><span class="white-text name">Biblioteca</span></a>

				<!-- Email of the user in here -->
				<a href="#!email"><span class="white-text email"><?php echo $_SESSION['txtEmail'];?></span></a>
			</div>
		</li>
		
		<li><a class="subheader">Actividades</a></li>		
		<li><a  class="waves-effect"href="bibliotecaVer.php" ><i class="material-icons">assignment</i>Ver mis reportes</a></li>
		<li><a class="waves-effect" href="bibliotecaHacer.php" ><i class="material-icons">mode_edit</i>Hacer un reporte</a></li>
		<li><a class="waves-effect" href="controllers/logout.php"> <i class="material-icons">perm_identity</i>Salir de mi cuenta</a></li>
	</ul>

	<!-- CONTENT OF THE PAGE -->
	<div class="container col s12 m8 offset-m2">

		<!-- Comienzo del formulario -->
		<!-- START OF A CARD (BOOK) -->
		<div class="card horizontal">
			<div class="card-stacked">
				<div class="card-content">
					<form id="formReport">
						<div class="row">
							<p class="grey-text text-darken-3" style="margin-left: 20px"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Asunto</b></p>
								<div class="col s12">
									
									
									<!-- ASUNTO -->
									<div class="row">
										<div class="input-field col s10" style="margin-left: 35px">
											<input id="asunto" name="txtAsunto" type="text" class="validate">
											<label for="asunto">Asunto del reporte</label>
										</div>
									</div>

									<!-- DATOS DEL EQUIPO -->
							<div class="row">
								<div class="col s12"><p class="grey-text  text-darken-3" style="margin-left: 20px"><b>&nbsp;&nbsp;Datos del equipo</b> <i class="material-icons tooltipped" data-delay="50" style="font-size: 20px;">info_outline</i></p></div>
								<div>
									
								</div>
									<div class="col s10">
										<div class="row" style="margin: 20px">
											<div class="input-field col s6 m4">
												<input id="patrimonio" name="txtPatrimonio" type="text" class="validate" onkeyup="buscarEquipoReporte()">
												<label for="patrimonio">Patrimonio</label>
											</div>

											<div class="input-field col s6 m3">
												<input disabled id="serie" name="txtNoSerie" type="text" class="validate">
												<label for="serie">Número de serie</label>
											</div>
											<div class="input-field col s6 m2">
												<input disabled id="marca" name="txtMarca" type="text" class="validate">
												<label for="marca">Marca</label>
											</div>

											<div disabled class="input-field col s6 m3">
												<input disabled id="modelo" name="txtModelo" type="text" class="validate">
												<label for="modelo">Modelo</label>
											</div>
										</div>
									</div>
								</div>

									<!-- DISPOSITIVOS VAN AQUI -->
									
									<div class="row" id="radioEquipo">

										<p class="grey-text  text-darken-3" style="margin-left: 20px"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Equipo</b></p>
										
										<!-- IMPRESORA -->
										<div>
											<div class="col s6 m2 offset-m1">
												<label>
												<input id="1" type="radio" name="equipo" value="1" disabled="disabled"/>
												<img src="img/1.png">
												<p class="center-align">Impresora</p>
												</label>
											</div>
										</div>
									

										<!-- MINIPRINTER -->
										<div class="col s6 m2">
											<label>
												<input id="2" type="radio" name="equipo" value="2" disabled="disabled"/>
												<img src="img/2.png">
												<p class="center-align">Miniprinter</p>
											</label>
										</div>

										<!-- LECTOR DE CODIGO DE BARRAS -->
										<div class="col s6 m2">
											<label>
												<input id="3" type="radio" name="equipo" value="3" disabled="disabled"/>
												<img src="img/3.png">
												<p class="center-align">Lector de CB</p>
											</label>
										</div>

										<!-- PC -->
										<div class="col s6 m2">
											<label>
												<input id="4" type="radio" name="equipo" value="4" disabled="disabled"/>
												<img src="img/4.png">
												<p class="center-align">Computadora</p>
											</label>
										</div>

										<!-- PANTALLA -->
										<div class="col s6 m2">
											<label>
												<input type="radio" name="equipo" value="5" id="5" disabled="disabled"/>
												<img src="img/5.png">
												<p class="center-align">Pantalla</p>
											</label>
										</div>

										<!-- ESCANER -->
										<div class="col s6 m2 offset-m1">
											<label>
												<input type="radio" name="equipo" value="6" id="6" disabled="disabled"/>
												<img src="img/6.png">
												<p class="center-align">Escáner</p>
											</label>
										</div>

										<!-- PROYECTOR -->
										<div class="col s6 m2">
											<label>
												<input type="radio" name="equipo" value="7" id="7" disabled="disabled"/>
												<img src="img/7.png">
												<p class="center-align">Proyector</p>
											</label>
										</div>

										<!-- VIDEO CASETERA -->
										<div class="col s6 m2">
											<label>
												<input type="radio" name="equipo" value="8" id="8" disabled="disabled" />
												<img src="img/8.png">
												<p class="center-align">Videocasetera</p>
											</label>
										</div>
										<!-- Mobiliario -->
										<div class="col s6 m2">
											<label>
												<input type="radio" name="equipo" value="10" id="10" disabled="disabled" />
												<img src="img/10.png">
												<p class="center-align">Mobiliario</p>
											</label>
										</div>

										<!-- Alumbrado -->
										<div class="col s6 m2">
											<label>
												<input type="radio" name="equipo" value="11" id="11" disabled="disabled" />
												<img src="img/11.png">
												<p class="center-align">Alumbrado</p>
											</label>
										</div>

										<!-- OTRO -->
										<div class="col s6 m2 offset-m5">
											<label>
												<input type="radio" name="equipo" value="9" id="9" disabled="disabled" />
												<img src="img/9.png">
												<p class="center-align">Otro</p>
											</label>
										</div>
									</div>
							</div>

							

								<!-- TIPO DE SERVICIO -->
								<div class="row left-align">
									<div class="col s12">
										<p class="grey-text text-darken-3 left-align" style="margin-left: 20px"><b>&nbsp;&nbsp;&nbsp;&nbsp;Tipo de servicio</b></p>
										<div class="row" style="margin : 40px">

											<input class="with-gap col s2" name="servicio" type="radio" id="instalacion" value="1" checked="checked" />
											<label for="instalacion">Instalación</label>

											<input class="with-gap col s2" name="servicio" type="radio" id="mantenimiento" value="2" />
											<label for="mantenimiento">Mantenimiento</label>

											<input class="with-gap col s2" name="servicio" type="radio" id="reparacion" value="3" />
											<label for="reparacion">Reparación</label>

											<input class="with-gap col s2" name="servicio" type="radio" id="garantia" value="4" />
											<label for="garantia">Garantía</label>

											<input class="with-gap col s2" name="servicio" type="radio" id="asesoria" value="5" />
											<label for="asesoria">Asesoría</label>

											<div class="row"><br>
												<div class="input-field col s12">
													<i class="material-icons prefix left-align valign-wrapper">mode_edit</i>
													<textarea id="textarea1" name="txtTrabajoSolicitado" class="materialize-textarea validate"></textarea>
													<label for="textarea1">Describa el trabajo que está solicitando</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card-action center-align"><br>
								<button class="blue darken-1 waves-effect waves-light btn" type="button" onclick="report()"><i class="material-icons right">send</i>Enviar reporte</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
		
		<!-- END OF THE CARD (BOOK) -->
		</body>
		<script src="js/jquery-2.1.4.min.js" />"></script>
		<script src="js/materialize.min.js" />"></script>
		<script src="js/buscarEquipoReporte.js" />"></script>
		<script src="js/scripts.js" />"></script>

		<script>
			var test='<video id="exampleVideo" width="586" height="72">';
			test+='<source src="img/example.mp4" type="video/mp4"></video>';
																			
			$(document).ready(function() {
				$('.tooltipped').tooltip({delay: 50,html: true,tooltip: test,position: 'right' });
				$('.modal').modal();
				$('i.tooltipped').mouseover(function(){
					$('#exampleVideo').get(0).play();
				});
				$('i.tooltipped').mouseout(function(){
					$('#exampleVideo').get(0).currentTime = 0;
					$('#exampleVideo')[0].pause();
				});
			});
			$('.dropdown-button').dropdown({belowOrigin: true});
			$(document).ready(function() {$('select').material_select();});
			$(".button-collapse").sideNav();
		</script>
	</html>