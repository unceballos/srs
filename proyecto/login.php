<html>
<head>
<title>Iniciar Sesión</title>
<link rel="shortcut icon" href="img/icon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"	rel="stylesheet">
<link href="materialize/css/materialize.min.css" rel="stylesheet">
<link href="css/signup.css" rel="stylesheet">
<link href="css/navbar.css" rel="stylesheet">

</head>

<body class="indigo-grey lighten-5 imgbglogin">

	<nav class="transparent z-depth-0">
		<div class="nav-wrapper db">
			<a href="./" class="brand-logo teal-text"><i class="material-icons">camera</i>SRS</a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li><a href="tipoDeRegistro.php" class="waves-effect waves-light teal-text ">Crear una cuenta</a></li>
			</ul>
		</div>
	</nav>

<div class="row white-text">
	<div class="col s12 m8 offset-m2 center-align ">
		<h3 class="grey-text text-darken-1">Inicie sesión</h3>
		<p class="grey-text text-darken-1">Por favor ingrese los datos de su cuenta. Si aún no tiene una cuenta, <a href="tipoDeRegistro.php" class="teal-text">regístrese</a> para acceder al sistema.</p>
	</div>
</div>

	<div class="container row " style="margin-top: 4%;">

		<div class="col s12 m8 offset-m2 white z-depth-2"><br>
			<form class="col s12" id="formLogin">
				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="email" class="validate" name="txtEmail" > <label
							for="email">Correo</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<input id="password" type="password" class="validate" name="txtPass" > <label
							for="password">Contraseña</label>
					</div>
				</div>

				<div class="row">
					<div class="col s12 center-align">
						<button class="btn waves-effect waves-light" type="button" name="action" onclick="login()" >Iniciar sesión</button>
					</div>
				</div>
			</form>
	</div> 
	</div>
	<script src="js/jquery-2.1.4.min.js"></script>
	<script src="js/materialize.min.js"></script>
	<script src="js/scripts.js"></script>
	<script>
		$('#formLogin input').keydown(function(e) {
			
			if (e.keyCode == 13) {
				event.preventDefault();
				login();
			}
		});
	</script>
</body>
</html>
