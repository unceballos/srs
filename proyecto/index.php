<html>

<head>
    <title>Inicio del SRS</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="shortcut icon" href="img/icon.ico">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='materialize/css/materialize.min.css' rel="stylesheet">
    <link href="css/navbar.css" rel="stylesheet" media="screen,projection">

</head>

<body class="white">
    <!-- NAV -->
    <nav class="white">
        <div class="nav-wrapper db   ">
            <a href="./" class="brand-logo teal-text ">
                <i class="material-icons">camera</i>SRS</a>

            <a href="/" data-activates="mobile" class="button-collapse grey-text">
                <i class="material-icons">menu</i>
            </a>
            <ul class="right hide-on-med-and-down">
              
   
                        <a href="login.php" class="transparent waves-effect teal-text hovery">Iniciar sesión</a>
                   
                  
                        <a href="tipoDeRegistro.php" class="transparent waves-effect teal-text hovery">Registrarse</a>
                   

           
            </ul>
        </div>
    </nav>

    <!-- SIDE NAV -->
    <ul id="mobile" class="side-nav">
        <li>
            <div class="userView">
                <div class="background">
                    <img src="img/bck.jpg" alt="">
                </div>

                <br>
                <br>
                <br>

            </div>
        </li>

        <li>
            <a class="subheader">Actividades</a>
        </li>
        <li>
            <a class="waves-effect" href="login">
                <i class="material-icons">contacts</i>Iniciar sesión</a>
        </li>
    </ul>


    <!--SLIDER-->
    <div class="row">
        <div class="slider col s12">
            <ul class="slides">
                <li>
                    <img src="img/simple.jpeg">
                    <!-- random image -->
                    <div class="caption center-align">
                        <div class="">
                            <h3>Versátil, simple y robusto</h3>
                            <h5 class="light grey-text text-lighten-3">Desde levantar reportes hasta ver el estado de sus equipos, </h5>
                            <h5 class="light grey-text text-lighten-3">el SRS lo tiene todo y aún más.</h5>



                        </div>
                    </div>
                </li>
                <li>
                    <img src="img/sl-bridge.jpeg">
                    <div class="caption left-align">
                        <h3>SRS</h3>
                        <h5 class="light grey-text text-lighten-3">El sistema de reparación de SIABUC, </h5>
                        <h5 class="light grey-text text-lighten-3">ahora en cualquier momento y lugar</h5>
                    </div>
                </li>
                <li>
                    <img src="img/sl-fix.jpeg">
                    <div class="caption right-align">
                        <h3>Todo lo que ya hacía</h3>
                        <h3>pero mejor</h3>
                        <a href="tipoDeRegistro.php" class="waves-effect waves-light btn-large">Regístrese ahora</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="container">


        <!--ICONS SECTION-->
        <br>
        <h4 class="center-align title grey-text">Un registro seguro
        </h4>
                <p class="center-align title grey-text">El proceso para registrarse y comenzar a usar el SRS no sólo es simple, sino también seguro y eficaz</p>

        <div class="row iconz">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center teal-text text-lighten-1">
                        <i class="material-icons large">person_pin</i>
                    </h2>
                    <h5 class="center">Regístrese</h5>
                    <p class="center-align light grey-text text-darken-1 comp">Empiece por crearse una cuenta, si usted es un bibliotecario pídale al encargado que le agregue a la
                        plataforma
                    </p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center teal-text text-lighten-1">
                        <i class="material-icons large">verified_user</i>
                    </h2>
                    <h5 class="center">Verifíquese</h5>

                    <p class="center-align light grey-text text-darken-1 comp">Al registrarse se enviará una petición al administrador para verificar su cuenta (es posible que le contacten
                        para verificar su identidad)</p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center teal-text text-lighten-1">
                        <i class="material-icons large">terrain</i>
                    </h2>
                    <h5 class="center">Inicie sesión</h5>


                    <p class="center-align light grey-text text-darken-1 comp">Una vez verificado, tan solo bastará con ingresar su correo y su contraseña para acceder al sistema</p>
                </div>
            </div>
        </div>
        <br>






        <!-- SECOND SECTION -->

        <h4 class="center-align title grey-text">Todo es posible
        </h4>
        <p class="center-align title grey-text">En esta sección presentamos algunas
           de las tareas más relevantes que se pueden realizar mediante esta plataforma</p>

        <div class="row">
            <div class="col s6 m6">
                <div class="hoverable card-panel grey lighten-5 z-depth-1">
                    <div class="row">
                        <div class="col s2">
                            <i class="material-icons small teal-text">report</i>

                        </div>
                        <div class="col s10">
                            <span class="grey-text text-darken-1">
                                Levantar reportes sobre un equipo de manera automatizada, en cualquier momento y lugar.
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s6 m6">
                <div class="hoverable card-panel grey lighten-5 z-depth-1">
                    <div class="row">
                        <div class="col s2">
                            <i class="material-icons small teal-text">format_list_bulleted</i>

                        </div>
                        <div class="col s10">
                            <span class="grey-text text-darken-1">
                                Ver el estado en tiempo real de todos los reportes realizados por su biblioteca.
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s6 m6">
                <div class="hoverable card-panel grey lighten-5 z-depth-1">
                    <div class="row">
                        <div class="col s2">
                            <i class="material-icons small teal-text">insert_invitation</i>
                        </div>
                        <div class="col s10">
                            <span class="grey-text text-darken-1">
                                Llevar un inventario más simple y versátil, con buscador e historial de incidencias incorporado.
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s6 m6">
                <div class="hoverable card-panel grey lighten-5 z-depth-1">
                    <div class="row">
                        <div class="col s2">
                            <i class="material-icons small teal-text">message</i>
                        </div>
                        <div class="col s10">
                            <span class="grey-text text-darken-1">
                                Lograr una comunicación entre las bibliotecas y el departamento de reparación sobre un reporte en específico.
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col s6 m6">
                <div class="hoverable card-panel hover grey lighten-5 z-depth-1">
                    <div class="row">
                        <div class="col s2">
                            <i class="material-icons small teal-text">filter_list</i>
                        </div>
                        <div class="col s10">
                            <span class="grey-text text-darken-1">
                                Filtrar los reportes que se han realizado por fechas y estado, siempre encontrarás lo que buscas.
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col s6 m6">
                <div class="hoverable card-panel grey lighten-5 z-depth-1">
                    <div class="row">
                        <div class="col s2">
                            <i class="material-icons small teal-text">pie_chart</i>

                        </div>
                        <div class="col s10">
                            <span class="grey-text text-darken-1">
                                Sistematización de las estádisticas de la plataforma. Toda la información relevante mostrada de manera gráfica.
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <br>

        <br>



    </div>


</body>
<footer class="page-footer  teal darken-1" style="margin-top: 0%">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Contacto</h5>
                <p class="white-text">Si desea ponerse en contacto con el departamento de soporte técnico (dónde a la vez, se encuentra el actual
                    administrador de esta plataforma) puede usar el siguiente correo:</p>
                <a class="green-text text-lighten-2" href="mailto:soportedgti@ucol.mx">
                    soportedgti@ucol.mx
                </a>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Enlaces</h5>
                <ul>
                    <li>
                        <a class="grey-text text-lighten-3" href="login.php">Iniciar sesión
                        </a>
                    </li>
                    <li>
                        <a class="grey-text text-lighten-3" href="tipoDeRegistro.php">Registrarse
                        </a>
                    </li>
                    <li>
                        <a class="grey-text text-lighten-3" href="./">Inicio
                        </a>
                    </li>
                    <li>
                        <a class="grey-text text-lighten-3" href="#!">
                            Sobre nosotros
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            Made with love by
            <div class="green-text text-lighten-2">
                <a class="white-text" href="https://www.linkedin.com/in/fernanceballos" target="_blank">C</a>, 
                <a class="white-text" href="https://www.linkedin.com/in/ubaldo-torres-ju%C3%A1rez-792827149/" target="_blank">U</a> & 
                <a class="white-text" href="https://www.linkedin.com/in/gustavo-urbina/" target="_blank">G</a>
            </div>

        </div>
    </div>
</footer>

<script src="js/jquery-2.1.4.min.js" />"></script>
<script src="js/materialize.min.js" />"></script>
<script src="js/script.js" />"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.slider').slider({
            indicators: false
        });
    });
    $(document).ready(function () {
        $('.parallax').parallax();
    });
    $(".button-collapse").sideNav();
    $('.carousel.carousel-slider').carousel({ fullWidth: true });
</script>

</html>