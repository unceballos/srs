	<?php
		session_start();
		if (empty($_SESSION['txtEmail'])) {
    		header('Location: login.php');
    		die();
    	}
    	if($_SESSION['txtRol']!=1){
    		if($_SESSION['txtRol']==2){
    			header('Location: tecnicoOrders.php');
    		}else if($_SESSION['txtRol']==3){
    			header('Location: bibliotecaVer.php');
    		}
		}

	?>
	<html>
		<head>
		<title>Inventario</title>
		<link rel="shortcut icon" href="img/icon.ico">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
		rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="materialize/css/materialize.min.css">
		<link rel="stylesheet" type="text/css" href="css/navbar.css">
		<link rel="stylesheet" type="text/css" href="css/biblioteca.css">
		<link rel="stylesheet" type="text/css" href="css/radios.css">
	</head>
	<body class="blue-grey lighten-5">

		<!-- Navbar and Header -->
		<nav class="nav-extended cyan darken-3" style="margin-bottom: 4%">
			<div class="nav-background nabground">
				<div class="ea k"></div>
			</div>
			<div class="nav-wrapper db">
			    <!-- LOGO -->
				<a
					href="#" data-activates="mobile" class="button-collapse"><i
					class="white-text material-icons">menu</i></a>
				<ul class="bt hide-on-med-and-down">
					      <li><a class="dropdown-button white-text" href="#!" data-activates="dropdown1"><?php echo $_SESSION['txtEmail'];?><i class="material-icons right">arrow_drop_down</i></a></li>
				</ul>

				<div class="nav-header de">

					<!-- WHAT THE USER IT-S SEARCHING -->
					<div class="row">
					<div class="col s4 offset-s4 center-align">
					<img src="img/helmet.png" style="width: 20%; margin-bottom: -6%">
				</div>
					</div>
						<h3 class="cyan-text text-lighten-5" style="margin-bottom: -3%">Administrador</h3>
						<h1>BIBLIOTECAS</h1>
				</div>
			</div>

			<!-- Dropdown Structure -->
			<ul id='dropdown1' class='dropdown-content ddd'>
				<li><a href="controllers/logout.php">Cerrar sesión</a></li>
			</ul>

			<!-- Pestañitas (hide-on-med-and-down)-->
			<div class=" categories-wrapper  cyan darken-4">

				<!--categories-db col s4 offset-s4center-align">-->
					<div class="center-align" style="margin-left: 41%;">
						<ul class="">
							<li><a href="AdministradorTecnicos.php" class="white-text">TÉCNICOS</a></li>
							<li class="k"><a href="AdministradorBiblioteca.php" class="white-text">BIBLIOTECAS</a></li>
						</ul>
					</div>
			</div>
		</nav>

		<!-- SIDE NAV -->
		<ul id="mobile" class="side-nav">
			<li><div class="userView">
					<div class="background">
						<img src="img/library.jpeg">
					</div>

					<a href="#!user"><img style="width:25%;"
						src="img/school.png"></a>

					<a href="#!name"><span class="white-text name">Técnico</span></a>

					<!-- Email of the user in here -->
					<a href="#!email"><span class="white-text email">mgonzales@ucol.mx</span></a>
				</div></li>

			<li><a class="subheader">Actividades</a></li>
			<li><a  class="waves-effect"href="bibliotecaVer.php" ><i
					class="material-icons">assignment</i>Ver mis reportes</a></li>
			<li><a class="waves-effect" href="bibliotecaHacer.php" ><i
					class="material-icons">mode_edit</i>Hacer un reporte</a></li>
			<li><a class="waves-effect" href="logout.php"> <i
					class="material-icons">perm_identity</i>Salir de mi cuenta</a></li>
		</ul>

		<!-- CONTENIDO DE LA PÁGINA -->
		<div class="row container">
			<div class="col s12 m12">
				<div class="row"><div class="col s12 m12 center-align">
											<img src="img/school.png" width="100px"><br>
											<p class="grey-text text-darken-3 ">Aquí se mostrarán los usuarios que se hayan registrado para representar a una biblioteca.</p>
										</div></div>
				

			          

	   

				<!-- COMIENZO DEL REPORTE 1-->
				<div class="row"<div class="card horizontal col s9">
						<div class="card-content" style="margin:auto;">
							<div class="row ">
								<div class="col s12 m12 ">
									<table class="bordered highlight centered">
        								<thead>
								        	<tr>
								            	<th>Nombre</th>
								            	<th>Apellido</th>
								            	<th>Correo</th>
								            	<th>Teléfono</th>
								            	<th>Aprovado</th>
								            	<th>Detalles</th>
								          	</tr>
								        </thead>

								        <tbody>
								        	<tr>
								        		<td>Marco</td>
								        		<td>Antonio</td>
								        		<td>solis@ucol.mx</td>
								        		<td>3121054240</td>
								        		<td><div class="switch">
    <label>
      No
      <input type="checkbox">
      <span class="lever"></span>
      Sí
    </label>
  </div></td>
								        		<td><a href="Equipo.php" class="btn-floating btn-small blue waves-effect waves-light tooltipped" data-position="right" data-delay="900" data-tooltip="Ver detalles"><i class="material-icons">assignment</i></a></td>
								          	</tr>
								          	<tr>
								        		<td>Marco</td>
								        		<td>Antonio</td>
								        		<td>solis@ucol.mx</td>
								        		<td>3121054240</td>
								        		<td><div class="switch">
    <label>
      No
      <input type="checkbox">
      <span class="lever"></span>
      Sí
    </label>
  </div></td>
								        		<td><a href="Equipo.php" class="btn-floating btn-small blue waves-effect waves-light tooltipped" data-position="right" data-delay="900" data-tooltip="Ver detalles"><i class="material-icons">assignment</i></a></td>
								          	</tr><tr>
								        		<td>Marco</td>
								        		<td>Antonio</td>
								        		<td>solis@ucol.mx</td>
								        		<td>3121054240</td>
								        		<td><div class="switch">
    <label>
      No
      <input type="checkbox">
      <span class="lever"></span>
      Sí
    </label>
  </div></td>
								        		<td><a href="Equipo.php" class="btn-floating btn-small blue waves-effect waves-light tooltipped" data-position="right" data-delay="900" data-tooltip="Ver detalles"><i class="material-icons">assignment</i></a></td>
								          	</tr>
								        </tbody>
								    </table>
								</div>
							</div>
						</div>
				</div></div>
				</div></div>
				<!-- FIN DEL CONTENEDOR -->

			<!-- DISPARADOR DEL MODAL -->
			<div class="fixed-action-btn">
			   <a href="#modalCrear" class="btn-floating btn-large cyan darken-2 tooltipped" data-position="left" data-delay="1200" data-tooltip="Agregar equipo"><i class="material-icons">add</i></a>
			  </div>

  <!-- Modal Structure -->
  <div id="modalCrear" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Agregar nuevo equipo</h4>
      <p>Ingrese los datos del nuevo equipo y haga clic en "Aceptar" para agregarlo al inventario</p>

		<!-- DISPOSITIVOS VAN AQUI -->
												<!-- IMPRESORA -->
												<div class="row">
													<p class="grey-text  text-darken-3" style="margin-left: 20px"><b>Equipo</b></p>
													<div>
														<div class="col s6 m2 offset-m1">
															<label>
																<input type="radio" name="equipo" value="impresora" />
																<img src="img/print.png">
																<p class="center-align">Impresora</p>
															</label>
														</div>

														<!-- MINIPRINTER -->
														<div class="col s6 m2">
															<label>
																<input type="radio" name="equipo" value="miniprinter" />
																<img src="img/miniprint.png">
																<p class="center-align">Miniprinter</p>
															</label>
														</div>

														<!-- LECTOR DE CODIGO DE BARRAS -->
														<div class="col s6 m2">
															<label>
																<input type="radio" name="equipo" value="lector" />
																<img src="img/laser.png">
																<p class="center-align">Lector de CB</p>
															</label>
														</div>

														<!-- PC -->
														<div class="col s6 m2">
															<label>
																<input type="radio" name="equipo" value="pc" />
																<img src="img/pc.png">
																<p class="center-align">Computadora</p>
															</label>
														</div>

														<!-- PANTALLA -->
														<div class="col s6 m2">
															<label>
																<input type="radio" name="equipo" value="pantalla" id="pantalla" />
																<img src="img/monitor.png">
																<p class="center-align">Pantalla</p>
															</label>
														</div>

														<!-- ESCANER -->
														<div class="col s6 m2 offset-m2">
															<label>
																<input type="radio" name="equipo" value="escaner" id="escaner" />
																<img src="img/scan.png">
																<p class="center-align">Escáner</p>
															</label>
														</div>

														<!-- PROYECTOR -->
														<div class="col s6 m2">
															<label>
																<input type="radio" name="equipo" value="proyector" id="proyector" />
																<img src="img/proyector.png">
																<p class="center-align">Proyector</p>
															</label>
														</div>

														<!-- VIDEO CASETERA -->
														<div class="col s6 m2">
															<label>
																<input type="radio" name="equipo" value="videocas" id="videocas" />
																<img src="img/vhs.png">
																<p class="center-align">Videocasetera</p>
															</label>
														</div>

														<!-- OTRO -->
														<div class="col s6 m2">
															<label>
																<input type="radio" name="equipo" value="otro" id="otro" />
																<img src="img/otro.png">
																<p class="center-align">Otro</p>
															</label>
														</div>

													</div></div>

																											<div class="row">

														<br>
<p class="grey-text  text-darken-3" style="margin-left: 20px"><b>Datos del equipo</b></p>

														<div class="row"><div class="col s12 col m10 offset-m1 left-align" style="margin-top: 20px; ">
															<div class="row">
																<div class="input-field col s2"><input value="Brother" id="solicitante" type="text" class="validate">
																	<label for="first_name">Marca</label></div>
																	<div class="input-field col s3"><input value="DCP-L2540DW" id="solicitante" type="text" class="validate">
																		<label for="first_name">Modelo</label></div>
																		<div class="input-field col s3"><input value="84763903484" id="solicitante" type="text" class="validate">
																			<label for="first_name">Serie</label></div>
																			<div class="input-field col s4"><input value="MX-231432SG" id="solicitante" type="text" class="validate">
																				<label for="first_name">Patrimonio</label></div>
																			</div>

																		</div>
																	</div>

																	<div class="row"><div class="col s12 col m10 offset-m1 left-align" style="margin-top: 20px; ">
																		<div class="row">
																			<div class="input-field col s3"><input value="Windows 10" id="solicitante" type="text" class="validate">
																				<label for="first_name">S.O.</label></div>
																				<div class="input-field col s3"><input value="2013" id="solicitante" type="text" class="validate">
																					<label for="first_name">Office</label></div>
																					<div class="input-field col s3"><input value="193.123.111.3" id="solicitante" type="text" class="validate">
																						<label for="first_name">IP</label></div>
																						<div class="input-field col s3"><input value="Administrador" id="solicitante" type="text" class="validate">
																							<label for="first_name">Usuario</label></div>
																						</div>

																					</div>
																				</div>


																			</div>

    </div>
    <div class="modal-footer">
    	<a href="#!" style="margin-right:15px; margin-right:15px;" class="modal-action blue modal-close waves-effect waves-green btn-flat white-text">Aceptar</a>
    	<a href="#!" style="margin-right:15px; margin-right:15px;" class="modal-action modal-close waves-effect waves-red btn-flat ">Cancelar</a>
      
    </div>
  </div>

			</body>
	<script src="js/jquery-2.1.4.min.js" />"></script>
	<script src="js/materialize.min.js" />"></script>
	<script>
		$(document).ready(function() {
		$('.modal').modal();});
		$('.dropdown-button').dropdown({belowOrigin: true});
		$(document).ready(function() {$('select').material_select();});
		$(".button-collapse").sideNav();
	</script>
	</html>
